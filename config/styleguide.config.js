const path = require('path')
const __root = path.resolve(__dirname, '..')

module.exports = {
  webpackConfig: Object.assign({}, require(`${__root}/config/webpack.config.js`)),
  styleguideDir: `${__root}/docs/components`,
  components: `${__root}/src/components/**/*.js`,
  ignore: ['**/index.js']
}

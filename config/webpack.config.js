const path = require('path')
const { execSync } = require('child_process')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const FaviconWebpackPlugin = require('favicons-webpack-plugin')
const { EnvironmentPlugin } = require('webpack')

const LIFECYCLE_EVENT = process.env.npm_lifecycle_event

const __root = path.resolve(__dirname, '..')
const __dist = path.resolve(__root, 'dist')
const __js = path.resolve(__root, 'src/main.js')
const __html = path.resolve(__root, 'src/main.html')
const __favicon = path.resolve(__root, 'assets/Scenic.svg')

// Add code source aliases
// For CSS, all aliases are forced by adding a tilde in the path
const aliases = {
  '~': `${__root}`,

  '@ui-components': path.resolve(__root, 'node_modules/@sat-mtl/ui-components'),

  '@assets': `${__root}/assets`,
  '@models': `${__root}/src/models`,
  '@components': `${__root}/src/components`,
  '@stores': `${__root}/src/stores`,
  '@api': `${__root}/src/api`,
  '@utils': `${__root}/src/utils`,
  '@styles': `${__root}/src/styles`
}

// Add favicon auto-generation from the SVG logo
// It increses a lot the build times of webpack
// See https://github.com/jantimon/favicons-webpack-plugin
const favicon = {
  logo: __favicon,
  mode: 'webapp',
  inject: true,
  favicons: {
    appName: 'Scenic',
    appDescription: 'A stage teleconference system for real-time transmission of artistic performances.'
  }
}

const common = {
  entry: __js,
  output: {
    filename: '[name].bundle.js',
    sourceMapFilename: '[name].js.map',
    path: __dist
  },
  resolve: {
    alias: aliases
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    }, {
      test: /\.(sa|sc|c)ss$/,
      use: ['style-loader', 'css-loader', 'sass-loader']
    }, {
      test: /\.(woff|woff2|eot|ttf|otf)$/,
      type: 'asset/inline'
    }, {
      test: /\.(jpe?g|png)$/i,
      type: 'asset/resource'
    }, {
      test: /\.md$/,
      use: ['babel-loader', 'markdown-to-react-loader']
    }]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({ template: __html }),
    new FaviconWebpackPlugin(favicon),
    new CopyWebpackPlugin({
      patterns: [{
        from: 'assets',
        to: 'assets',
        context: 'node_modules/@sat-mtl/ui-components'
      }]
    }),
    new EnvironmentPlugin({
      GIT_SHORT_HASH: execSync('git rev-parse --short HEAD', { encoding: 'utf8' }).trim(),
      NODE_ENV: LIFECYCLE_EVENT === 'start' ? 'development' : 'production'
    })
  ]
}

const development = {
  mode: 'development',
  devtool: 'source-map',
  optimization: {
    minimize: false
  },
  devServer: {
    contentBase: __dist
  }
}

const production = {
  mode: 'production',
  devtool: 'source-map',
  optimization: {
    minimize: true
  }
}

if (LIFECYCLE_EVENT === 'start') {
  module.exports = { ...common, ...development }
} else if (LIFECYCLE_EVENT === 'build') {
  module.exports = { ...common, ...production }
} else {
  module.exports = common
}

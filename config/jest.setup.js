/* global HTMLCanvasElement */

import React from 'react'
import { useTranslation } from 'react-i18next'

React.useLayoutEffect = React.useEffect

useTranslation.t = key => key

window.setImmediate = window.setTimeout
HTMLCanvasElement.prototype.getContext = Function.prototype

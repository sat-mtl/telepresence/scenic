#!/bin/bash

# Sorts **all** the keys of a JSON locale file
#
# $1 - The locale JSON file
#
# Usage:
#  ./sort_locale_keys.sh assets/locales/fr/client.json
#
# Dependencies:
# * The script uses the [`jq command`](https://stedolan.github.io/jq) to parse the JSON file.
HAS_JQ=`command -v jq`

if ! [ -x "${HAS_JQ}" ]; then
  echo "jq could not be found" >&2
  exit 1
elif ! [ "$#" -eq "1" ]; then
  echo "a locale file must be supplied" >&2
  exit 1
else
  cat "$1" | jq --sort-keys > "$1.tmp" && mv "$1.tmp" "$1"
fi

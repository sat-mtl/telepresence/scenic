#!/bin/bash

# Check arguments
if [ "$#" -ne 1 ]; then
  echo "VERSION argument is required"
  exit 0
fi

# Check
if [[ -z "${GITLAB_API_TOKEN}" || -z "${CI_PROJECT_ID}" ]]; then
  echo "Gitlab variables GITLAB_API_TOKEN or CI_PROJECT_ID are missing !"
  exit 0
fi

# See https://gitlab.com/gitlab-org/gitlab-foss/-/tree/master/doc/api/releases
BUNDLE_VERSION="${1:-0.0.0}"
BUNDLE_CI_JOB="publish-bundle"
BUNDLE_NAME="scenic-${BUNDLE_VERSION}.tar.gz"
BUNDLE_URL_PRE="https://gitlab.com/sat-mtl/tools/scenic/scenic/-/jobs/artifacts/"
BUNDLE_URL="${BUNDLE_URL_PRE}${BUNDLE_VERSION}/raw/${BUNDLE_NAME}?job=${BUNDLE_CI_JOB}"

echo -e "\nDelete release ${BUNDLE_VERSION}"
curl \
  --request DELETE \
  --header "Private-Token: ${GITLAB_API_TOKEN}" \
  "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases/${BUNDLE_VERSION}"

RELEASE_DATA="$(cat <<EOVAR
{
  "name": "Scenic ${BUNDLE_VERSION}",
  "tag_name": "${BUNDLE_VERSION}",
  "description": "Version ${BUNDLE_VERSION} of Scenic App is now available!",
  "assets": {
    "links": [{
      "name": "Download Scenic ${BUNDLE_VERSION} bundle",
      "url": "${BUNDLE_URL}"
    }]
  }
}
EOVAR
)"

echo -e "\nPublish new release ${BUNDLE_VERSION}"
curl \
  --request POST \
  --header 'Content-Type: application/json' \
  --header "Private-Token: ${GITLAB_API_TOKEN}" \
  --data "${RELEASE_DATA}" \
  "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/releases"

#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo "BUCKET_NAME argument is required"
  exit 0
fi

if [[  -z "${AWS_ACCESS_KEY_ID}" || -z "${AWS_SECRET_ACCESS_KEY}" ]]; then
  echo "AWS credentials (AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY) must be set as environment variables !"
  exit 0
fi

echo "Installing AWS tooling"

apt update -qq && apt install -y -qq curl jq
pip3 install awscli

echo "Deploying 'dist' directory on s3://$1 bucket"

aws s3 sync dist/ s3://$1/ --delete


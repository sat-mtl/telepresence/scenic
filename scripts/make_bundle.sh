#!/bin/bash

if [[ -z "${GITLAB_API_TOKEN}" || -z "${CI_PROJECT_ID}" ]]; then
  echo "Gitlab variables missing !"
  exit 0
fi

echo "Configure bundled version"

VERSION=$(node -p 'require("./package.json").version')
GITLAB_UPLOAD_API="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/uploads"
BUNDLE_NAME="scenic-${VERSION}"

apt update -qq
apt install -y -qq jq curl

if [ "${CI_COMMIT_REF_NAME}" == "develop" ]; then
  BUNDLE_NAME="${BUNDLE_NAME}-develop";
fi

echo "Create bundled version"

make bundle

echo "Upload new bundle"

UPLOAD_URL=$(curl -X POST \
  -H "Private-Token: ${GITLAB_API_TOKEN}" \
  --form "file=@${BUNDLE_NAME}.tgz" \
  ${GITLAB_UPLOAD_API} | jq '.url')

SCENIC_URL="https://gitlab.com/sat-mtl/tools/scenic/scenic"
BUNDLE_URL="${SCENIC_URL}${UPLOAD_URL}"

echo "Bundle is ready to download from ${BUNDLE_URL}"

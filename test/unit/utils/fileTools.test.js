/* global beforeEach jest describe it expect File */

import FileBridge from '@models/common/FileBridge'
import { uploadFile, downloadFile } from '@utils/fileTools'

describe('fileTools', () => {
  const TEST_VALUE = 'test'
  const NOW_VALUE = Date.now()

  beforeEach(() => {
    jest.spyOn(global.Date, 'now').mockReturnValue(NOW_VALUE)
  })

  describe('uploadFile', () => {
    it('should upload a file bridge from a blob and return its content', async () => {
      const blob = new File([TEST_VALUE], 'test.txt', { type: 'text/plain' })
      const model = FileBridge.fromFileBlob(blob)
      model.content = TEST_VALUE

      expect(await uploadFile(blob)).toEqual(model)
    })

    it('should throw an error if the upload failed', async () => {
      await expect(uploadFile('lol')).rejects.toThrow()
    })
  })

  describe('downloadFile', () => {
    it('should create a temporary link on the content of the file', () => {
      const $ref = { setAttribute: jest.fn(), click: jest.fn() }
      downloadFile($ref, 'test')
      expect($ref.setAttribute).toHaveBeenCalled()
      expect($ref.click).toHaveBeenCalled()
    })
  })
})

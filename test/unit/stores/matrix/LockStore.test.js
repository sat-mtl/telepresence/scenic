/* global describe it expect jest beforeEach */

import { configure } from 'mobx'
import populateStores from '@stores/populateStores.js'
import LockStore from '@stores/matrix/LockStore'

import MatrixEntry from '@models/matrix/MatrixEntry'
import Contact from '@models/sip/Contact'

import { test, sip, rtmpOutput } from '@fixture/allQuiddities'
import { SIP_ID } from '@models/quiddity/specialQuiddities'
import { getKind } from '@fixture/allQuiddityKinds'

configure({ safeDescriptors: false })

describe('LockStore', () => {
  const testEntry = new MatrixEntry(test)
  const sipEntry = new MatrixEntry(sip)
  const rtmpEntry = new MatrixEntry(rtmpOutput)

  const contact = Contact.fromJSON({
    id: '0',
    uri: 'contact0',
    connections: ['some_shmpath']
  })

  const contactEntry = new MatrixEntry(sip, contact)

  let quiddityStore, lockStore, kindStore, sipStore

  beforeEach(() => {
    ({ quiddityStore, lockStore, kindStore, sipStore } = populateStores())
  })

  describe('constructor', () => {
    it('should fail to build a lock store without a quiddity store', () => {
      expect(() => new LockStore()).toThrow()
    })

    it('should fail to build a lock store without a sip store', () => {
      expect(() => new LockStore(quiddityStore)).toThrow()
    })
  })

  describe('lockedQuiddities', () => {
    it('should return a set with the ids of all locked quiddities if they are added to the locked entries', () => {
      lockStore.addLock(testEntry)
      expect(lockStore.lockedQuiddities).toEqual(new Set().add(test.id))
    })

    it('should not include the id of a sip quiddity in the returned set', () => {
      jest.spyOn(sipStore, 'sipId', 'get').mockReturnValue(SIP_ID)
      lockStore.addLock(sipEntry)
      expect(lockStore.lockedQuiddities).toEqual(new Set())
      lockStore.addLock(testEntry)
      expect(lockStore.lockedQuiddities).toEqual(new Set().add(test.id))
    })
  })

  describe('lockedContacts', () => {
    it('should not return the id of a locked entry if it is not a contact', () => {
      lockStore.addLock(testEntry)
      expect(lockStore.lockedContacts).toEqual(new Set())
    })

    it('should return a set of all contact names for locked entries that are contacts', () => {
      lockStore.addLock(contactEntry)
      expect(lockStore.lockedContacts).toEqual(new Set().add(contact.userName))
      lockStore.addLock(testEntry)
      expect(lockStore.lockedContacts).toEqual(new Set().add(contact.userName))
    })
  })

  describe('isLockableQuiddity', () => {
    beforeEach(() => {
      kindStore.addKind(getKind(test))
      quiddityStore.addQuiddity(test)
    })

    it('should flag a lockable quiddity', () => {
      expect(lockStore.isLockableQuiddity(test.id)).toEqual(false)
      lockStore.addLockableKind(test.kindId)
      expect(lockStore.isLockableQuiddity(test.id)).toEqual(true)
      lockStore.deleteLockableKind(test.kindId)
      expect(lockStore.isLockableQuiddity(test.id)).toEqual(false)
    })
  })

  describe('setLock', () => {
    beforeEach(() => {
      quiddityStore.addQuiddity(rtmpOutput)
    })

    it('should add a lock if it was not registered', () => {
      expect(lockStore.lockedEntries.has(contactEntry.id)).toEqual(false)
      lockStore.setLock(contactEntry, true)
      expect(lockStore.lockedEntries.has(contactEntry.id)).toEqual(true)
    })

    it('should not add a lockable kind twice', () => {
      lockStore.addLock(rtmpEntry)
      const addSpy = jest.spyOn(lockStore, 'addLock')
      lockStore.setLock(rtmpEntry, true)
      expect(addSpy).not.toHaveBeenCalled()
    })

    it('should delete a lock if it was registered', () => {
      lockStore.addLock(rtmpEntry)
      expect(lockStore.lockedEntries.has(rtmpEntry.id)).toEqual(true)
      lockStore.setLock(rtmpEntry, false)
      expect(lockStore.lockedEntries.has(rtmpEntry.id)).toEqual(false)
    })

    it('should not delete an unregistered quiddity', () => {
      const deleteSpy = jest.spyOn(lockStore, 'deleteLock')
      lockStore.setLock(contactEntry, false)
      expect(deleteSpy).not.toHaveBeenCalled()
    })
  })
})

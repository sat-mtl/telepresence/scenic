/* global describe it expect jest beforeEach */
import populateStores from '@stores/populateStores.js'

describe('ModalStore', () => {
  let modalStore

  const MODAL_TEST_ID = 'test'
  const MODAL_ALT_ID = 'alt'
  const MODAL_PROPS = { test: 'test' }

  beforeEach(() => {
    ({ modalStore } = populateStores())
  })

  describe('constructor', () => {
    it('should create a modal store with default values', () => {
      expect(modalStore.modals.size).toEqual(0)
      expect(modalStore.modalProps.size).toEqual(0)
      expect(modalStore.activeModal).toEqual(null)
    })
  })

  describe('cleanActiveModal', () => {
    beforeEach(() => {
      modalStore.clearActiveModal = jest.fn()
      modalStore.activeModal = MODAL_TEST_ID
    })

    it('should clean the active modal', () => {
      modalStore.cleanActiveModal(MODAL_TEST_ID)
      expect(modalStore.clearActiveModal).toHaveBeenCalled()
    })

    it('shouldn\'t clean a modal that is not active', () => {
      modalStore.cleanActiveModal(MODAL_ALT_ID)
      expect(modalStore.clearActiveModal).not.toHaveBeenCalled()
    })
  })

  describe('setActiveModal', () => {
    beforeEach(() => {
      modalStore.addModal(MODAL_TEST_ID)
    })

    it('should set a modal as active', () => {
      modalStore.setActiveModal(MODAL_TEST_ID)
      expect(modalStore.activeModal).toEqual(MODAL_TEST_ID)
    })

    it('should not set a modal as active if it wasn\'t registered', () => {
      modalStore.setActiveModal(MODAL_ALT_ID)
      expect(modalStore.activeModal).not.toEqual(MODAL_ALT_ID)
    })
  })

  describe('clearActiveModal', () => {
    it('should clear the active modal', () => {
      modalStore.addModal(MODAL_TEST_ID)
      modalStore.setActiveModal(MODAL_TEST_ID)
      expect(modalStore.activeModal).toEqual(MODAL_TEST_ID)
      modalStore.clearActiveModal()
      expect(modalStore.activeModal).toEqual(null)
    })
  })

  describe('addModal', () => {
    it('should add a modal', () => {
      expect(modalStore.modals.size).toEqual(0)
      modalStore.addModal(MODAL_TEST_ID)
      expect(modalStore.modals.size).toEqual(1)
      expect(modalStore.modals.has(MODAL_TEST_ID)).toEqual(true)
    })
  })

  describe('setModalProps', () => {
    beforeEach(() => {
      modalStore.addModal(MODAL_TEST_ID)
    })

    it('should set the modal props', () => {
      modalStore.setModalProps(MODAL_TEST_ID, MODAL_PROPS)
      expect(modalStore.modalProps.has(MODAL_TEST_ID)).toEqual(true)
    })

    it('should not set the modal props if it wasn\'t registered', () => {
      modalStore.setModalProps(MODAL_ALT_ID, MODAL_PROPS)
      expect(modalStore.modalProps.has(MODAL_ALT_ID)).toEqual(false)
    })
  })
})

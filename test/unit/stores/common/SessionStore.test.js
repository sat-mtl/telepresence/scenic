/* global describe it expect jest beforeEach File */

import { Socket } from '@fixture/socket'

import populateStores from '@stores/populateStores.js'
import SessionStore, { LOG } from '@stores/common/SessionStore'
import FileBridge from '@models/common/FileBridge'
import { uploadFile } from '@utils/fileTools'
import { RequirementError } from '@utils/errors'

describe('SessionStore', () => {
  let socket, socketStore, sessionStore, userTreeStore, quiddityStore, lockStore
  let sessionAPI

  const FILE_TEST = new FileBridge('test.json')
  const FILE_SAMPLE = new FileBridge('sample.json')
  const FILE_LIST = [FILE_TEST.id, FILE_SAMPLE.id]
  const FILE_JSON = FILE_TEST.toJSON()

  /**
   * @todo: update when modification date is provided from the backend
   * @See {@link https://gitlab.com/sat-mtl/tools/switcher/-/issues/123}
   */
  // const FILE_MAP = new Map(FILES.map(f => [f.id, f]))

  global.TextDecoder = function () {
    this.decode = () => 'DECODED'
  }

  beforeEach(() => {
    ({ sessionStore, socketStore, userTreeStore, quiddityStore, lockStore } = populateStores())

    LOG.error = jest.fn()
    LOG.warn = jest.fn()

    socket = new Socket()
    socketStore.setActiveSocket(socket)

    sessionAPI = socketStore.APIs.sessionAPI
  })

  describe('constructor', () => {
    beforeEach(() => {
      sessionStore.handleSocketChange = jest.fn()
    })

    it('should be correctly instantiated', () => {
      expect(sessionStore).toBeDefined()
      expect(sessionStore.sessions).toBeDefined()
      expect(sessionStore.currentSessionId).toEqual(null)
    })

    it('should react to the SocketStore\'s active socket change', () => {
      socketStore.activeSocket = new Socket()
      expect(sessionStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })

    it('should throw an error if the quiddityStore is not provided', () => {
      expect(() => new SessionStore(socketStore, userTreeStore)).toThrow(RequirementError)
    })

    it('should throw an error if the UserTreeStore is not provided', () => {
      expect(() => new SessionStore(socketStore, quiddityStore)).toThrow(RequirementError)
    })

    it('should not throw an error if all required stores are provided', () => {
      expect(() => new SessionStore(socketStore, quiddityStore, userTreeStore)).not.toThrow()
    })
  })

  describe('handleSocketChange', () => {
    beforeEach(() => {
      sessionAPI.onSessionReset = jest.fn().mockResolvedValue([])
    })

    it('should define the APIs if there is a new socket', () => {
      const newSocket = new Socket()

      sessionStore.handleSocketChange(newSocket)
      expect(sessionAPI.onSessionReset).toHaveBeenCalled()
    })
  })

  describe('handleSessionImported', () => {
    beforeEach(() => {
      sessionStore.updateSessionList = jest.fn()
    })

    it('should be called when the API is triggered', () => {
      sessionStore.handleSessionImported('test')
      expect(sessionStore.updateSessionList).toHaveBeenCalled()
    })
  })

  describe('handleSessionReset', () => {
    beforeEach(() => {
      sessionStore.fetchSessionList = jest.fn().mockResolvedValue([])
      sessionStore.clearCurrentSession = jest.fn()
      quiddityStore.handleSessionChange = jest.fn()
      userTreeStore.clear = jest.fn()
      lockStore.clear = jest.fn()
    })

    it('should clear current file', () => {
      sessionStore.handleSessionReset()
      expect(sessionStore.clearCurrentSession).toHaveBeenCalled()
    })

    it('should call handle session change from the quiddity store', () => {
      sessionStore.handleSessionReset()
      expect(quiddityStore.handleSessionChange).toHaveBeenCalled()
    })

    it('should clear the user tree store', () => {
      sessionStore.handleSessionReset()
      expect(userTreeStore.clear).toHaveBeenCalled()
    })

    it('should clear the lock store', () => {
      sessionStore.handleSessionReset()
      expect(lockStore.clear).toHaveBeenCalled()
    })
  })

  describe('handleSessionLoaded', () => {
    beforeEach(() => {
      sessionStore.handleSocketChange(socket)
      sessionStore.setCurrentSession = jest.fn()
      sessionStore.addSession(FILE_TEST)
    })

    it('should set the loaded file as the current file', () => {
      sessionStore.handleSessionLoaded(FILE_TEST.id)
      expect(sessionStore.setCurrentSession).toHaveBeenCalledWith(FILE_TEST.id)
    })

    it('should log an error if the imported file name is undefined', () => {
      sessionStore.handleSessionLoaded('')
      expect(sessionStore.setCurrentSession).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('handleSessionSaved', () => {
    beforeEach(() => {
      sessionStore.setCurrentSession = jest.fn()
      sessionStore.updateSessionList = jest.fn()
    })

    it('should set the saved file as the current file', () => {
      sessionStore.handleSessionSaved(FILE_JSON)
      expect(sessionStore.setCurrentSession).toHaveBeenCalledWith(FILE_JSON.id)
      expect(sessionStore.updateSessionList).toHaveBeenCalled()
    })

    it('should log an error if the saved file is not parsable', () => {
      sessionStore.handleSessionSaved('I AM A FAILURE')
      expect(sessionStore.setCurrentSession).not.toHaveBeenCalledWith()
      expect(sessionStore.updateSessionList).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('handleSessionDeleted', () => {
    it('should update the session list when it is called', () => {
      sessionStore.updateSessionList = jest.fn()
      sessionStore.handleSessionDeleted(FILE_JSON.name)
      expect(sessionStore.updateSessionList).toHaveBeenCalledWith()
    })
  })

  describe('updateSessionList', () => {
    const FILE_TO_DELETE = FileBridge.fromJSON({
      ...FILE_TEST.toJSON(),
      id: 'to delete'
    })

    beforeEach(() => {
      sessionStore.fetchSessionList = jest.fn().mockResolvedValue([
        FILE_TEST,
        FILE_SAMPLE
      ])
    })

    it('should delete the unsynchronized file', async () => {
      sessionStore.addSession(FILE_TO_DELETE)
      sessionStore.removeSession = jest.fn()
      await sessionStore.updateSessionList()
      expect(sessionStore.removeSession).toHaveBeenCalledWith(FILE_TO_DELETE.id)
    })

    it('should add all the unsynchronized file', async () => {
      sessionStore.addSession = jest.fn()
      await sessionStore.updateSessionList()
      expect(sessionStore.addSession).toHaveBeenNthCalledWith(1, FILE_TEST)
      expect(sessionStore.addSession).toHaveBeenNthCalledWith(2, FILE_SAMPLE)
    })
  })

  describe('fetchSessionList', () => {
    it('should fetch all parsed files', async () => {
      sessionAPI.list = jest.fn().mockResolvedValue(FILE_LIST)
      // @todo: update to line below when modification date is provided from the backend
      // expect(await sessionStore.fetchSessionList()).toEqual(FILE_LIST.map(f => new FileBridge(f)))
      expect(await sessionStore.fetchSessionList().id).toEqual(FILE_LIST.map(f => new FileBridge(f)).id)
      expect(await sessionStore.fetchSessionList().name).toEqual(FILE_LIST.map(f => new FileBridge(f)).name)
      expect(await sessionStore.fetchSessionList().extension).toEqual(FILE_LIST.map(f => new FileBridge(f)).extension)
    })

    it('should log an error if the API failed', async () => {
      sessionAPI.list = jest.fn().mockRejectedValue(false)
      await sessionStore.fetchSessionList()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('fetchSessionContent', () => {
    const TEXT = 'Test'

    beforeEach(() => {
      sessionAPI.read = jest.fn().mockResolvedValue(TEXT)
    })

    it('should return the file content', async () => {
      expect(await sessionStore.fetchSessionContent(FILE_TEST)).toEqual(TEXT)
    })

    it('should log an error if the API failed', async () => {
      sessionAPI.read = jest.fn().mockRejectedValue(false)
      await sessionStore.fetchSessionContent(FILE_TEST)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applySessionReset', () => {
    beforeEach(() => {
      sessionAPI.reset = jest.fn().mockResolvedValue(true)
    })

    it('should reset the current session', async () => {
      await sessionStore.applySessionReset()
      expect(sessionAPI.reset).toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if it failed to reset the current session', async () => {
      sessionAPI.reset = jest.fn().mockRejectedValue(false)
      await sessionStore.applySessionReset()
      expect(sessionAPI.reset).toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applySessionImport', () => {
    beforeEach(() => {
      sessionAPI.write = jest.fn().mockResolvedValue(true)
    })

    it('should import a new session into the server', async () => {
      await sessionStore.applySessionImport(FILE_TEST, 'test')
      expect(sessionAPI.write).toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if it failed to import a new session', async () => {
      sessionAPI.write = jest.fn().mockRejectedValue(false)
      await sessionStore.applySessionImport(FILE_TEST, 'test')
      expect(sessionAPI.write).toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applySessionUpload', () => {
    const blob = new File([JSON.stringify({ id: 'test', content: 'something' })], 'test.json', {
      type: 'application/json'
    })

    beforeEach(() => {
      sessionStore.applySessionImport = jest.fn()
    })

    it('should call applySessionImport to import a new session into the server', async () => {
      await sessionStore.applySessionUpload(blob)
      await uploadFile(blob)
      expect(sessionStore.applySessionImport).toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if the bridge could not be created', async () => {
      await sessionStore.applySessionUpload('I am not a blob')
      expect(sessionStore.applySessionImport).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applySessionLoading', () => {
    beforeEach(() => {
      sessionAPI.load = jest.fn().mockResolvedValue(true)
      sessionStore.handleSessionLoaded = jest.fn()
    })

    it('should load and use the loaded session', async () => {
      expect(sessionStore.currentSessionId).toBeFalsy()
      await sessionStore.applySessionLoading(FILE_SAMPLE)
      expect(sessionAPI.load).toHaveBeenCalledWith(FILE_SAMPLE.id)
      expect(sessionStore.currentSessionId).toEqual(FILE_SAMPLE.id)
      expect(sessionStore.handleSessionLoaded).toHaveBeenCalled()
    })

    it('should log an error if it failed to reset the current session', async () => {
      sessionAPI.load = jest.fn().mockRejectedValue(false)
      await sessionStore.applySessionLoading(FILE_SAMPLE)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applySessionSaving', () => {
    beforeEach(() => {
      sessionAPI.saveAs = jest.fn().mockResolvedValue(true)
      sessionStore.addSession(FILE_TEST)
      sessionStore.setCurrentSession(FILE_TEST.id)
    })

    it('should fail and log an error if no file is used', async () => {
      sessionStore.setCurrentSession(null)
      await sessionStore.applySessionSaving()
      expect(sessionAPI.saveAs).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should save the used file', async () => {
      await sessionStore.applySessionSaving()
      expect(sessionAPI.saveAs).toHaveBeenCalledWith(FILE_TEST.name)
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if it failed to save the current file', async () => {
      sessionAPI.saveAs = jest.fn().mockRejectedValue(false)
      await sessionStore.applySessionSaving()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applySessionSavingAs', () => {
    beforeEach(() => {
      sessionAPI.saveAs = jest.fn().mockResolvedValue(true)
      sessionStore.handleSessionSaved = jest.fn()
    })

    it('should save the current session as a new name', async () => {
      await sessionStore.applySessionSavingAs(FILE_SAMPLE.name)
      expect(sessionAPI.saveAs).toHaveBeenCalledWith(FILE_SAMPLE.name)
      expect(sessionStore.handleSessionSaved).toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if it failed to reset the current session', async () => {
      sessionAPI.saveAs = jest.fn().mockRejectedValue(false)
      await sessionStore.applySessionSavingAs(FILE_SAMPLE.name)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applySessionDeletion', () => {
    beforeEach(() => {
      sessionAPI.remove = jest.fn().mockResolvedValue(true)
      sessionStore.addSession(FILE_TEST)
      sessionStore.handleSessionDeleted = jest.fn()
    })

    it('should delete the requested file', async () => {
      await sessionStore.applySessionDeletion(FILE_TEST.id)
      expect(sessionAPI.remove).toHaveBeenCalledWith(FILE_TEST.id)
      expect(sessionStore.handleSessionDeleted).toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
      expect(LOG.warn).not.toHaveBeenCalled()
    })

    it('should log an error if it failed to reset the current session', async () => {
      await sessionStore.applySessionDeletion(FILE_SAMPLE.id)
      expect(LOG.warn).toHaveBeenCalled()
    })

    it('should log an error if it failed to reset the current session', async () => {
      sessionAPI.remove = jest.fn().mockRejectedValue(false)
      await sessionStore.applySessionDeletion(FILE_TEST.id)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyClearTrash', () => {
    let clearSpy

    beforeEach(() => {
      sessionStore.applySessionDeletion = jest.fn().mockResolvedValue(true)
      clearSpy = jest.spyOn(sessionStore, 'clearSessionTrash')
      FILE_LIST.map(f => sessionStore.addSessionToTrash(f.id))
    })

    it('should delete all the file', async () => {
      await sessionStore.applyClearTrash()
      expect(sessionStore.applySessionDeletion).toHaveBeenCalled()
      expect(clearSpy).toHaveBeenCalled()
      expect(sessionStore.sessionTrash.size).toEqual(0)
    })
  })
})

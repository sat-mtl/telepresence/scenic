/* global jest describe it expect beforeEach */
import SettingStore, { LANGUAGE_SETTING_ID, DISPLAY_THUMBNAILS_ID, DISPLAY_ENCODERS_ID } from '@stores/common/SettingStore'
import populateStores from '@stores/populateStores.js'

import { DEFAULT_LANGUAGE } from '@utils/i18n'

const FR_LANGUAGE = 'fr'

describe('SettingStore', () => {
  let socketStore, configStore, settingStore

  beforeEach(() => {
    ({ socketStore, configStore, settingStore } = populateStores())
  })

  describe('constructor', () => {
    it('should not instantiate store without config stores', () => {
      expect(() => new SettingStore(socketStore)).toThrow()
    })

    it('should create a store with default values', () => {
      expect(settingStore.i18n).toBeDefined()
    })
  })

  describe('userLanguage', () => {
    it('should use the default language by default', () => {
      expect(settingStore.userLanguage).toEqual(DEFAULT_LANGUAGE)
    })

    it('should use the configured language if no language is set by user', () => {
      configStore.setUserScenic({ defaultLang: FR_LANGUAGE })
      expect(settingStore.userLanguage).toEqual(FR_LANGUAGE)
    })

    it('should use the set language', () => {
      settingStore.setSetting(LANGUAGE_SETTING_ID, FR_LANGUAGE)
      expect(settingStore.userLanguage).toEqual(FR_LANGUAGE)
    })
  })

  describe('displayThumbnails', () => {
    it('should use the configured setting by default', () => {
      configStore.setUserScenic({ displayThumbnails: false })
      expect(settingStore.displayThumbnails).toEqual(false)
    })
    it('should use the changed settings', () => {
      settingStore.setSetting(DISPLAY_THUMBNAILS_ID, false)
      expect(settingStore.displayThumbnails).toEqual(false)
    })
  })

  describe('displayEncoders', () => {
    it('should use the configured setting by default', () => {
      configStore.setUserScenic({ displayEncoders: false })
      expect(settingStore.displayEncoders).toEqual(false)
    })
    it('should use the changed settings', () => {
      settingStore.setSetting(DISPLAY_ENCODERS_ID, false)
      expect(settingStore.displayEncoders).toEqual(false)
    })
  })

  describe('handleConfigUpdate', () => {
    beforeEach(() => {
      settingStore.handleConfigUpdate = jest.fn()
    })

    it('should handle all new config updates', () => {
      const config = { defaultLang: FR_LANGUAGE }
      configStore.setUserScenic(config)
      expect(settingStore.handleConfigUpdate).toHaveBeenCalledWith(configStore.scenicConfiguration)
    })
  })

  describe('handleSettingUpdate', () => {
    beforeEach(() => {
      settingStore.handleSettingUpdate = jest.fn()
    })

    it('should handle all setting updates', () => {
      settingStore.setSetting(LANGUAGE_SETTING_ID, FR_LANGUAGE)
      expect(settingStore.handleSettingUpdate).toHaveBeenCalledWith(LANGUAGE_SETTING_ID, FR_LANGUAGE)
    })
  })
})

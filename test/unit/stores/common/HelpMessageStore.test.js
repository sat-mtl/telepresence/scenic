/* global describe it expect beforeEach */
import populateStores from '@stores/populateStores.js'

describe('HelpMessageStore', () => {
  const HELP_MESSAGE = 'test'
  let helpMessageStore

  beforeEach(() => {
    ({ helpMessageStore } = populateStores())
  })

  describe('constructor', () => {
    it('should be well instantiated', () => {
      expect(helpMessageStore).toBeDefined()
      expect(helpMessageStore.helpMessage).toBeDefined()
    })
  })

  describe('setHelpMessage', () => {
    it('should set a new help message', () => {
      expect(helpMessageStore.helpMessage).toBeFalsy()
      helpMessageStore.setHelpMessage(HELP_MESSAGE)
      expect(helpMessageStore.helpMessage).toEqual(HELP_MESSAGE)
    })
  })

  describe('cleanHelpMessage', () => {
    beforeEach(() => {
      helpMessageStore.setHelpMessage(HELP_MESSAGE)
    })

    it('should reset the help message', () => {
      expect(helpMessageStore.helpMessage).toEqual(HELP_MESSAGE)
      helpMessageStore.cleanHelpMessage()
      expect(helpMessageStore.helpMessage).toBeFalsy()
    })
  })
})

/* global describe it expect jest beforeEach */

import populateStores from '@stores/populateStores.js'

import Quiddity from '@models/quiddity/Quiddity'
import Kind from '@models/quiddity/Kind'
import Shmdata from '@models/shmdata/Shmdata'
import StatusEnum from '@models/common/StatusEnum'
import QuiddityTagEnum from '@models/quiddity/QuiddityTagEnum'

import QUIDDITY_KINDS from '@fixture/description.classes.json'
import SDI_INPUT_QUIDDITY from '@fixture/models/quiddity.sdiInput1.json'
import VIDEO_OUTPUT_QUIDDITY from '@fixture/models/quiddity.videoOutput.json'

describe('QuiddityStatusStore', () => {
  const sdiInput = Quiddity.fromJSON(SDI_INPUT_QUIDDITY)
  const videoOutput = Quiddity.fromJSON(VIDEO_OUTPUT_QUIDDITY)

  const SHMDATA = new Shmdata('test', 'test', 'test')
  const SHMDATA_ALT = new Shmdata('alt', 'alt', 'alt')

  let quiddityStore, quiddityStatusStore, shmdataStore, statStore, kindStore

  function initQuiddityKinds () {
    Object.values(QUIDDITY_KINDS.kinds)
      .map(json => Kind.fromJSON(json))
      .forEach(cl => kindStore.addKind(cl))
  }

  beforeEach(() => {
    ({ quiddityStore, quiddityStatusStore, shmdataStore, statStore, kindStore } = populateStores())
  })

  describe('handleQuiddityStatusesUpdate', () => {
    beforeEach(() => {
      quiddityStatusStore.populateQuiddityStatuses = jest.fn()
      quiddityStatusStore.cleanQuiddityStatuses = jest.fn()
      initQuiddityKinds()
    })

    it('should handle each quiddity updates by populating and cleaning all statuses', () => {
      quiddityStore.addQuiddity(sdiInput)
      expect(quiddityStatusStore.populateQuiddityStatuses).toHaveBeenCalled()
      expect(quiddityStatusStore.cleanQuiddityStatuses).toHaveBeenCalled()
    })
  })

  describe('populateQuiddityStatuses', () => {
    beforeEach(() => {
      quiddityStatusStore.setQuiddityStatus = jest.fn()
      quiddityStatusStore.handleQuiddityStatusesUpdate = jest.fn()
      initQuiddityKinds()
    })

    it('should populate statuses for each source and destination', () => {
      quiddityStore.addQuiddity(sdiInput)
      quiddityStore.addQuiddity(videoOutput)
      quiddityStatusStore.populateQuiddityStatuses()
      expect(quiddityStatusStore.setQuiddityStatus).toHaveBeenCalledTimes(2)
    })
  })

  describe('cleanQuiddityStatuses', () => {
    beforeEach(() => {
      quiddityStatusStore.removeQuiddityStatus = jest.fn()
    })

    it('should clean statuses for each source and destination', () => {
      quiddityStatusStore.setQuiddityStatus(QuiddityTagEnum.SOURCE, sdiInput.id, StatusEnum.INACTIVE)
      quiddityStatusStore.setQuiddityStatus(QuiddityTagEnum.DESTINATION, videoOutput.id, StatusEnum.INACTIVE)
      quiddityStatusStore.cleanQuiddityStatuses()
      expect(quiddityStatusStore.removeQuiddityStatus).toHaveBeenCalledTimes(2)
    })
  })

  describe('makeGlobalQuiddityStatus', () => {
    it('should return an error status if one of the quiddity\'s status is an error status', () => {
      expect(quiddityStatusStore.makeGlobalQuiddityStatus(StatusEnum.DANGER)).toEqual(StatusEnum.DANGER)
      expect(quiddityStatusStore.makeGlobalQuiddityStatus(undefined, StatusEnum.DANGER)).toEqual(StatusEnum.DANGER)
    })

    it('should return a warning status if one of the quiddity\'s status is a warning status', () => {
      expect(quiddityStatusStore.makeGlobalQuiddityStatus(StatusEnum.BUSY)).toEqual(StatusEnum.BUSY)
      expect(quiddityStatusStore.makeGlobalQuiddityStatus(undefined, StatusEnum.BUSY)).toEqual(StatusEnum.BUSY)
    })

    it('should return an inactive status if one of the quiddity\'s status is an inactive status', () => {
      expect(quiddityStatusStore.makeGlobalQuiddityStatus(StatusEnum.INACTIVE)).toEqual(StatusEnum.INACTIVE)
      expect(quiddityStatusStore.makeGlobalQuiddityStatus(undefined, StatusEnum.INACTIVE)).toEqual(StatusEnum.INACTIVE)
    })

    it('should return an active status if one of the quiddity\'s status is an active status', () => {
      expect(quiddityStatusStore.makeGlobalQuiddityStatus(StatusEnum.ACTIVE)).toEqual(StatusEnum.ACTIVE)
      expect(quiddityStatusStore.makeGlobalQuiddityStatus(undefined, StatusEnum.ACTIVE)).toEqual(StatusEnum.ACTIVE)
    })
  })

  describe('makeQuiddityStatus', () => {
    it('should return inactive status if the quiddity has no shmdata', () => {
      expect(quiddityStatusStore.makeQuiddityStatus(sdiInput.id)).toEqual(StatusEnum.INACTIVE)
    })

    it('should return error status if all the quiddity\'s shmdatas are inactive', () => {
      shmdataStore.getShmdataSet = jest.fn().mockReturnValue(new Set([SHMDATA]))
      statStore.getInactiveShmdatas = jest.fn().mockReturnValue([SHMDATA])
      expect(quiddityStatusStore.makeQuiddityStatus(sdiInput.id)).toEqual(StatusEnum.DANGER)
    })

    it('should return warning status if some of the quiddity\'s shmdatas are inactive', () => {
      shmdataStore.getShmdataSet = jest.fn().mockReturnValue(new Set([SHMDATA, SHMDATA_ALT]))
      statStore.getInactiveShmdatas = jest.fn().mockReturnValue([SHMDATA])
      expect(quiddityStatusStore.makeQuiddityStatus(sdiInput.id)).toEqual(StatusEnum.BUSY)
    })

    it('should return active status if all the quiddity\'s shmdatas are active', () => {
      shmdataStore.getShmdataSet = jest.fn().mockReturnValue(new Set([SHMDATA, SHMDATA_ALT]))
      statStore.getInactiveShmdatas = jest.fn().mockReturnValue([])
      expect(quiddityStatusStore.makeQuiddityStatus(sdiInput.id)).toEqual(StatusEnum.ACTIVE)
    })
  })
})

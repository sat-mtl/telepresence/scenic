/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'

import QUID_KINDS from '@fixture/description.classes.json'
import SDI_INPUT_QUID from '@fixture/models/quiddity.sdiInput1.json'
import VIDEO_OUTPUT_QUID from '@fixture/models/quiddity.videoOutput.json'
import SYSTEM_USAGE_QUID from '@fixture/models/quiddity.systemUsage.json'

import populateStores from '@stores/populateStores.js'
import QuiddityStore, { LOG } from '@stores/quiddity/QuiddityStore'
import InitStateEnum from '@models/common/InitStateEnum'

import Kind from '@models/quiddity/Kind'
import Quiddity from '@models/quiddity/Quiddity'
import QuiddityTagEnum from '@models/quiddity/QuiddityTagEnum'

describe('QuiddityStore', () => {
  const DUMB_ERROR = new Error('error')
  const AVAILABLE_KINDS = Object.values(QUID_KINDS.kinds)
    .map(json => Kind.fromJSON(json))
  const QUID_ID = 'quiddityTest'
  const QUID = new Quiddity(QUID_ID, 'quiddityTest0', QUID_ID)

  let socket, socketStore, quiddityStore, kindStore
  let quiddityAPI

  beforeEach(() => {
    ({ socketStore, kindStore, quiddityStore } = populateStores())

    LOG.error = jest.fn()
    socket = new Socket()
    socketStore.setActiveSocket(socket)
  })

  describe('constructor', () => {
    beforeEach(() => {
      quiddityStore.handleSocketChange = jest.fn()
    })

    it('should fail if the configStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new QuiddityStore(socketStore) }).toThrow()
      /* eslint-enable no-new */
    })

    it('should be correctly instantiated', () => {
      expect(quiddityStore).toBeDefined()
      expect(quiddityStore.initState).toEqual(InitStateEnum.NOT_INITIALIZED)
    })

    it('should react to the SocketStore\'s active socket change', () => {
      socketStore.activeSocket = new Socket()
      expect(quiddityStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })
  })

  describe('initialize', () => {
    beforeEach(() => {
      quiddityStore.initializeCurrentQuiddities = jest.fn().mockResolvedValue(true)
      quiddityStore.initializeInitQuiddities = jest.fn().mockResolvedValue(true)
      quiddityStore.clear = jest.fn().mockResolvedValue(true)
    })

    it('should not trigger initialization if it was already initialized', async () => {
      quiddityStore.isNotInitialized = jest.fn().mockReturnValue(false)

      await quiddityStore.initialize()

      expect(quiddityStore.initializeCurrentQuiddities).not.toHaveBeenCalled()
      expect(quiddityStore.initializeInitQuiddities).not.toHaveBeenCalled()
    })

    it('should initialize all quiddities and all quiddity kinds', async () => {
      await quiddityStore.initialize()

      expect(quiddityStore.initializeCurrentQuiddities).toHaveBeenCalled()
      expect(quiddityStore.initializeInitQuiddities).toHaveBeenCalled()
    })

    it('should clear quiddities, log an error and re-throw an error when current quiddity initialization failed', async () => {
      quiddityStore.initializeCurrentQuiddities = jest.fn().mockRejectedValue(new Error())

      await expect(quiddityStore.initialize()).rejects.toThrow()

      expect(quiddityStore.initializeCurrentQuiddities).toHaveBeenCalled()
      expect(quiddityStore.initializeInitQuiddities).not.toHaveBeenCalled()
      expect(quiddityStore.clear).toHaveBeenCalled()

      expect(LOG.error).toHaveBeenCalledWith(
        expect.objectContaining({
          msg: expect.stringMatching('current quiddities')
        })
      )
    })

    it('should clear quiddities, log an error and re-throw an error when configured quiddity initialization failed', async () => {
      quiddityStore.initializeInitQuiddities = jest.fn().mockRejectedValue(new Error())

      await expect(quiddityStore.initialize()).rejects.toThrow()

      expect(quiddityStore.initializeCurrentQuiddities).toHaveBeenCalled()
      expect(quiddityStore.initializeInitQuiddities).toHaveBeenCalled()
      expect(quiddityStore.clear).toHaveBeenCalled()

      expect(LOG.error).toHaveBeenCalledWith(
        expect.objectContaining({
          msg: expect.stringMatching('initQuiddities')
        })
      )
    })
  })

  describe('clear', () => {
    it('should de-initialized the store', () => {
      quiddityStore.clear()
      expect(quiddityStore.initState).toEqual(InitStateEnum.NOT_INITIALIZED)
    })

    it('should clear all store quiddities', () => {
      quiddityStore.quiddities.set(SDI_INPUT_QUID.id, Quiddity.fromJSON(SDI_INPUT_QUID))
      expect(quiddityStore.quiddities.size).toEqual(1)
      quiddityStore.clear()
      expect(quiddityStore.quiddities.size).toEqual(0)
    })
  })

  describe('quiddityTags', () => {
    beforeEach(() => {
      for (const kind of AVAILABLE_KINDS) {
        kindStore.addKind(kind)
      }

      quiddityStore.handleQuiddityCreation(SDI_INPUT_QUID)
      quiddityStore.handleQuiddityCreation(VIDEO_OUTPUT_QUID)
      quiddityStore.handleQuiddityCreation(SYSTEM_USAGE_QUID)
    })

    it('should find sdiInput quiddity type as a source', () => {
      expect(quiddityStore.quiddityTags.get(SDI_INPUT_QUID.id)).toEqual([QuiddityTagEnum.SOURCE])
    })

    it('should find videoOutput quiddity type as a destination', () => {
      expect(quiddityStore.quiddityTags.get(VIDEO_OUTPUT_QUID.id)).toEqual([QuiddityTagEnum.DESTINATION])
    })

    it('should find systemUsage quiddity type as a no type', () => {
      expect(quiddityStore.quiddityTags.get(SYSTEM_USAGE_QUID.id)).toEqual([])
    })
  })

  describe('fetchQuiddities', () => {
    let quiddityAPI

    const createdQuiddities = [
      SDI_INPUT_QUID,
      VIDEO_OUTPUT_QUID,
      SYSTEM_USAGE_QUID
    ]

    beforeEach(() => {
      ({ quiddityAPI } = socketStore.APIs)
    })

    beforeEach(() => {
      quiddityAPI.listCreated = jest.fn().mockResolvedValue(createdQuiddities)
      LOG.error = jest.fn()
    })

    it('should fetch all quiddity models', async () => {
      expect(await quiddityStore.fetchQuiddities()).toEqual(createdQuiddities)
    })

    it('should log an error if the request failed', async () => {
      quiddityAPI.listCreated = jest.fn().mockResolvedValue({})
      expect(await quiddityStore.fetchQuiddities()).toEqual([])

      expect(LOG.error).toHaveBeenCalledWith(
        expect.objectContaining({ msg: expect.stringMatching('created resources') })
      )
    })
  })

  describe('fetchAllQuiddityIds', () => {
    const errorMsg = 'A server error occurred while fetching all created resources.'
    const allQuids = [{ id: 'quid1' }, { id: 'quid2' }]

    const execFetchAllQuiddityIds = async () => {
      return quiddityStore.fetchAllQuiddityIds()
    }

    beforeEach(() => {
      ({ quiddityAPI } = socketStore.APIs)
    })

    it('should return all quiddity ids as an array', async () => {
      const result = allQuids.map(quid => quid.id)
      quiddityAPI.listCreated = jest.fn().mockResolvedValue(allQuids)
      expect(await execFetchAllQuiddityIds()).toEqual(result)
    })

    it('should fail if all quiddities are not fetched as an array', async () => {
      const typeError = new TypeError('Failed to list all quiddities as an array')
      quiddityAPI.listCreated = jest.fn().mockResolvedValue({ id: 'quid1' })
      await execFetchAllQuiddityIds()
      expect(LOG.error.mock.calls[0]).toMatchObject([{ msg: errorMsg, error: typeError.message }])
    })

    it('should fail if the fetch is rejected', async () => {
      quiddityAPI.listCreated = jest.fn().mockRejectedValue(DUMB_ERROR)
      await execFetchAllQuiddityIds()
      expect(LOG.error.mock.calls[0]).toMatchObject([{ msg: errorMsg, error: DUMB_ERROR.message }])
    })
  })

  describe('handleSocketChange', () => {
    beforeEach(() => {
      ({ quiddityAPI } = socketStore.APIs)

      quiddityStore.handleQuiddityCreation = jest.fn()
      quiddityStore.handleQuiddityRemoval = jest.fn()
      quiddityStore.handleSessionChange = jest.fn()
      quiddityStore.clear = jest.fn()
    })

    it('should define the APIs if there is a new socket', () => {
      const onCreationSpy = jest.spyOn(quiddityAPI, 'onCreated')
      const onRemovalSpy = jest.spyOn(quiddityAPI, 'onDeleted')

      quiddityStore.handleSocketChange(socket)

      expect(quiddityStore.clear).toHaveBeenCalled()

      expect(onCreationSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function)
      )
      expect(onRemovalSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function)
      )
    })

    it('should clear the store if the new socket is null', () => {
      quiddityStore.handleSocketChange(null)
      expect(quiddityStore.clear).toHaveBeenCalled()
    })

    it('should handle created quiddity', () => {
      socket.onEmit('quiddity_created', QUID.toJSON())
      expect(quiddityStore.handleQuiddityCreation).toHaveBeenCalledWith(QUID.toJSON())
    })

    it('should handle removed quiddity', () => {
      socket.onEmit('quiddity_deleted', QUID_ID)
      expect(quiddityStore.handleQuiddityRemoval).toHaveBeenCalledWith(QUID_ID)
    })
  })

  describe('applyQuiddityCreation', () => {
    beforeEach(() => {
      ({ quiddityAPI } = socketStore.APIs)
      quiddityAPI.create = jest.fn().mockResolvedValue(true)
      LOG.error = jest.fn()
    })

    it('should request a new quiddity', async () => {
      await quiddityStore.applyQuiddityCreation('test')
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log a failure when the request fails', async () => {
      quiddityAPI.create = jest.fn().mockRejectedValue(false)
      await quiddityStore.applyQuiddityCreation('test')
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyQuiddityRemoval', () => {
    beforeEach(() => {
      ({ quiddityAPI } = socketStore.APIs)
      quiddityAPI.delete = jest.fn().mockResolvedValue(true)
      LOG.info = jest.fn()
      LOG.error = jest.fn()
    })

    it('should request a quiddity removal', async () => {
      await quiddityStore.applyQuiddityRemoval('test')
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log a failure when the request fails', async () => {
      quiddityAPI.delete = jest.fn().mockRejectedValue(false)
      await quiddityStore.applyQuiddityRemoval('test')
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('makeQuiddityKindIndex', () => {
    let videoTestSource

    beforeEach(() => {
      videoTestSource = new Kind('test', 'test', 'test')
      kindStore.addKind(videoTestSource)
    })

    it('should provide the index 0 by default', () => {
      expect(quiddityStore.makeQuiddityKindIndex(videoTestSource.id)).toEqual(0)
    })

    it('should provide an index according to the number of existing quiddities with the requested kind', () => {
      for (const index of [...Array(10).keys()]) {
        const quidId = `${videoTestSource.id}${index}`
        expect(quiddityStore.makeQuiddityKindIndex('test')).toEqual(index)
        kindStore.addKind(new Kind('test', 'test', 'test'))

        quiddityStore.addQuiddity(
          Quiddity.fromJSON({
            id: quidId,
            kindId: videoTestSource.id,
            userTree: { index }
          })
        )
      }
    })
  })
})

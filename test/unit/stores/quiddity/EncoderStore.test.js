/* global it describe beforeEach expect jest */

import populateStores from '@stores/populateStores'
import EncoderStore from '@stores/quiddity/EncoderStore'

import MatrixEntry from '@models/matrix/MatrixEntry'
import Quiddity from '@models/quiddity/Quiddity'
import Contact from '@models/sip/Contact'
import Shmdata from '@models/shmdata/Shmdata'

import { sdiInput, videoOutput, sipMedia, ndiOutput, rtmpOutput, test, encoderH264 } from '@fixture/allQuiddities'

import { DISPLAY_ENCODERS_ID } from '@stores/common/SettingStore'
import { NDI_INPUT_KIND_ID } from '@models/quiddity/specialQuiddities'

describe('EncoderStore', () => {
  let socketStore
  let settingStore
  let encoderStore
  let capsStore

  beforeEach(() => {
    ({ socketStore, settingStore, encoderStore, capsStore } = populateStores())
  })

  const QUIDDITY = new Quiddity('test', 'test', 'test')
  const NDI_INPUT_QUIDDITY = new Quiddity('test', NDI_INPUT_KIND_ID, NDI_INPUT_KIND_ID)
  const sipContact = new Contact('contact', 'contact@dev.sip.test')
  const sdiShmdata = new Shmdata(`/tmp/${sdiInput.id}/0`, 'test', 'test')
  const sipShmdata = new Shmdata('/tmp/switcher_scenic8000_3_video-0', 'video', 'test')

  describe('constructor', () => {
    it('should be correctly instantiated', () => {
      expect(encoderStore).toBeDefined()
    })

    it('should throw an error if the setting store is not provided', () => {
      expect(() => new EncoderStore(socketStore)).toThrow()
    })

    it('should throw an error if the setting store is not provided', () => {
      expect(() => new EncoderStore(settingStore)).toThrow()
    })

    it('should throw an error if the caps store is not provided', () => {
      expect(() => new EncoderStore(socketStore, settingStore)).toThrow()
    })

    it('should not throw an error if all required stores are provided', () => {
      expect(() => new EncoderStore(socketStore, settingStore, capsStore)).not.toThrow()
    })
  })

  describe('isEncoderDisplayed', () => {
    it('should return true if the displayEncoder setting is set to true', () => {
      settingStore.setSetting(DISPLAY_ENCODERS_ID, true)
      expect(encoderStore.isEncoderDisplayed(QUIDDITY)).toBe(true)
    })

    it('should return true if the quiddity is an ndi input', () => {
      settingStore.setSetting(DISPLAY_ENCODERS_ID, false)
      expect(encoderStore.isEncoderDisplayed(NDI_INPUT_QUIDDITY)).toBe(true)
    })

    it('should return false if the displayEncoder setting is set to false', () => {
      settingStore.setSetting(DISPLAY_ENCODERS_ID, false)
      expect(encoderStore.isEncoderDisplayed(QUIDDITY)).toBe(false)
    })
  })

  describe('areEntriesConnectable', () => {
    let sdiEntry, videoEntry, sipEntry, ndiEntry, rtmpEntry, testEntry, encoderEntry
    beforeEach(() => {
      sdiEntry = new MatrixEntry(sdiInput, sipContact, [sdiShmdata])
      videoEntry = new MatrixEntry(videoOutput)
      sipEntry = new MatrixEntry(sipMedia, '', [sipShmdata])
      ndiEntry = new MatrixEntry(ndiOutput)
      rtmpEntry = new MatrixEntry(rtmpOutput)
      encoderEntry = new MatrixEntry(encoderH264)
      testEntry = new MatrixEntry(test)
      capsStore.areConnectable = jest.fn()
    })

    it('should return true if the caps are compatible', () => {
      capsStore.areConnectable.mockReturnValue(true)
      expect(encoderStore.areEntriesConnectable(sdiEntry, videoEntry)).toEqual(true)
    })

    it('should return false if the caps are not compatible', () => {
      capsStore.areConnectable.mockReturnValue(false)
      expect(encoderStore.areEntriesConnectable(sdiEntry, videoEntry)).toEqual(false)
    })

    it('should return false if the sdiEntry has a dead shmdata', () => {
      sdiEntry = new MatrixEntry(sdiInput, sipContact, [])
      capsStore.areConnectable.mockReturnValue(true)
      expect(encoderStore.areEntriesConnectable(sdiEntry, videoEntry)).toEqual(false)
    })

    it('should return true if the source entry is a sip entry and the destination entry does not require an encoded video', () => {
      jest.spyOn(MatrixEntry.prototype, 'isEncodableVideoSource', 'get').mockReturnValue(true)
      jest.spyOn(MatrixEntry.prototype, 'isSipSource', 'get').mockReturnValue(true)

      expect(encoderStore.areEntriesConnectable(sipEntry, videoEntry)).toEqual(true)
      expect(encoderStore.areEntriesConnectable(sipEntry, encoderEntry)).toEqual(true)
      expect(encoderStore.areEntriesConnectable(sipEntry, ndiEntry)).toEqual(true)
    })

    it('should return false if the source entry is a sip entry and the destination entry does require an encoded video', () => {
      jest.spyOn(MatrixEntry.prototype, 'isEncodableVideoSource', 'get').mockReturnValue(true)
      jest.spyOn(MatrixEntry.prototype, 'isSipSource', 'get').mockReturnValue(true)

      expect(encoderStore.areEntriesConnectable(sipEntry, rtmpEntry)).toEqual(false)
    })

    it('should return false if the source entry is a sip entry and the destination entry is not of type video', () => {
      jest.spyOn(MatrixEntry.prototype, 'isEncodableVideoSource', 'get').mockReturnValue(true)
      jest.spyOn(MatrixEntry.prototype, 'isSipSource', 'get').mockReturnValue(true)

      expect(encoderStore.areEntriesConnectable(sipEntry, testEntry)).toEqual(false)
    })
  })
})

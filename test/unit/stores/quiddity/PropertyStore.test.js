/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'
import { configure } from 'mobx'

import Quiddity from '@models/quiddity/Quiddity'
import Kind from '@models/quiddity/Kind'
import ShmdataRoleEnum from '@models/shmdata/ShmdataRoleEnum'
import Property from '@models/quiddity/Property'

import populateStores from '@stores/populateStores.js'
import PropertyStore, { LOG, PROPERTY_UNIT_MAPPING } from '@stores/quiddity/PropertyStore'

import SDI_INPUT_QUIDDITY from '@fixture/models/quiddity.sdiInput1.json'
import VIDEO_OUTPUT_QUIDDITY from '@fixture/models/quiddity.videoOutput.json'
import TEST_PATTERN_QUIDDITY from '@fixture/models/quiddity.testPattern0.json'
import WEBCAM_QUIDDITY from '@fixture/models/quiddity.webcam0.json'

configure({ safeDescriptors: false })

describe('PropertyStore', () => {
  const PROPERTY = new Property('test', 'test')

  const PROPERTY_VALUE = 1
  const PROPERTY_VALUE_ALT = 2
  const NEW_PROPERTY = Property.fromJSON({ ...PROPERTY.toJSON(), value: PROPERTY_VALUE })
  const GROUP_PROPERTY = Property.fromJSON({ ...PROPERTY.toJSON(), value: PROPERTY_VALUE, type: 'group' })

  const PROPERTY_JSON = { ...PROPERTY.toJSON(), id: `${PROPERTY.id}1` }
  const PROPERTY_JSON_ALT = { ...PROPERTY.toJSON(), id: `${PROPERTY.id}2` }

  const QUIDDITY_KIND = new Kind('test', 'test', 'test', [ShmdataRoleEnum.READER])
  const QUIDDITY = new Quiddity('test', 'test0', 'test', {
    property: [PROPERTY_JSON, PROPERTY_JSON_ALT]
  })

  let socket, socketStore, configStore, quiddityStore, propertyStore, kindStore
  let propertyAPI, infoTreeAPI

  beforeEach(() => {
    ({ socketStore, configStore, quiddityStore, propertyStore, kindStore } = populateStores())

    LOG.warn = jest.fn()
    LOG.error = jest.fn()

    socket = new Socket()
    socketStore.setActiveSocket(socket)
  })

  describe('constructor', () => {
    beforeEach(() => {
      propertyStore.handleSocketChange = jest.fn()
      propertyStore.handleUpdatedQuiddities = jest.fn()
    })

    it('should fail if the ConfigStore is not given', () => {
      expect(() => new PropertyStore(socketStore)).toThrow()
    })

    it('should fail if the QuiddityStore is not given', () => {
      expect(() => new PropertyStore(socketStore, configStore)).toThrow()
    })

    it('should be correctly instantiated', () => {
      expect(propertyStore).toBeDefined()
    })

    it('should react to the SocketStore\'s active socket change', () => {
      socketStore.activeSocket = new Socket()
      expect(propertyStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })

    it('should react to Matrix IDs changes', () => {
      kindStore.addKind(QUIDDITY_KIND)
      quiddityStore.addQuiddity(QUIDDITY)

      expect(
        propertyStore.handleUpdatedQuiddities
      ).toHaveBeenCalledWith(quiddityStore.userQuiddityIds)
    })
  })

  describe('makePropertyModels', () => {
    const PROPERTY_LENGTH = Object.keys(SDI_INPUT_QUIDDITY.infoTree.property).length
    let makeSpy

    beforeEach(() => {
      makeSpy = jest.spyOn(propertyStore, 'makePropertyModel')
    })

    it('should make a property model for each property from a Quiddity tree', () => {
      propertyStore.makePropertyModels(SDI_INPUT_QUIDDITY.id, SDI_INPUT_QUIDDITY)
      expect(makeSpy).toHaveBeenCalledTimes(PROPERTY_LENGTH)
    })

    it('should ignore parser failures', () => {
      const quiddity = Quiddity.fromJSON(SDI_INPUT_QUIDDITY)
      quiddity.infoTree.property.push({ id: 'I am a failure' })
      expect(propertyStore.makePropertyModels(quiddity.id, quiddity).length).toEqual(PROPERTY_LENGTH)
      expect(makeSpy).toHaveBeenCalledTimes(PROPERTY_LENGTH + 1)
    })

    it('should log a warning if the quiddity has no property', () => {
      const quiddity = Quiddity.fromJSON({ ...SDI_INPUT_QUIDDITY, infoTree: { property: null } })
      propertyStore.makePropertyModels(quiddity.id, quiddity)
      expect(LOG.warn).toHaveBeenCalled()
    })
  })

  describe('makePropertyModel', () => {
    let unitSpy

    beforeEach(() => {
      unitSpy = jest.spyOn(propertyStore, 'getPropertyUnit')
    })

    it('should make a property model from its JSON representation', () => {
      const json = SDI_INPUT_QUIDDITY.infoTree.property[0]
      const model = Property.fromJSON(json)
      expect(propertyStore.makePropertyModel(SDI_INPUT_QUIDDITY.id, json)).toEqual(model)
    })

    it('should log an error if the parser failed', () => {
      expect(propertyStore.makePropertyModel(SDI_INPUT_QUIDDITY.id, { id: 'I am a failure' })).toEqual(null)
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should complete the JSON model with the unit of the property', () => {
      const json = SDI_INPUT_QUIDDITY.infoTree.property[0]
      propertyStore.makePropertyModel(SDI_INPUT_QUIDDITY.id, json)
      expect(unitSpy).toHaveBeenCalled()
    })
  })

  describe('handleUpdatedQuiddities', () => {
    const ALL_QUIDDITY_IDS = [`${QUIDDITY.id}1`, `${QUIDDITY.id}2`]
    let removeSpy

    beforeEach(() => {
      propertyStore.initializeProperties = jest.fn()
      removeSpy = jest.spyOn(propertyStore, 'removeQuiddityEntries')
    })

    it('should initialize all properties of each new quiddity', async () => {
      await propertyStore.handleUpdatedQuiddities(ALL_QUIDDITY_IDS)
      expect(propertyStore.initializeProperties).toHaveBeenCalledTimes(ALL_QUIDDITY_IDS.length)
    })

    it('should be called with an empty array by default', async () => {
      await propertyStore.handleUpdatedQuiddities()
      expect(propertyStore.initializeProperties).toHaveBeenCalledTimes(0)
    })

    it('should remove all properties of each dead quiddity', async () => {
      propertyStore.addProperty(QUIDDITY.id, PROPERTY)
      await propertyStore.handleUpdatedQuiddities()
      expect(removeSpy).toHaveBeenCalledWith(QUIDDITY.id)
    })
  })

  describe('updatePropertyValue', () => {
    let updateSpy

    beforeEach(() => {
      propertyStore.fetchPropertyValue = jest.fn().mockResolvedValue(PROPERTY_VALUE)
      updateSpy = jest.spyOn(propertyStore, 'setPropertyValue')
    })

    it('should update each new properties by fetching its current value', async () => {
      await propertyStore.updatePropertyValue(QUIDDITY.id, PROPERTY)
      expect(propertyStore.fetchPropertyValue).toHaveBeenCalledWith(QUIDDITY.id, PROPERTY)
      expect(updateSpy).toHaveBeenCalledWith(QUIDDITY.id, PROPERTY.id, PROPERTY_VALUE)
    })

    it('should not fetch current value if it is provided', async () => {
      await propertyStore.updatePropertyValue(QUIDDITY.id, PROPERTY, PROPERTY_VALUE)
      expect(propertyStore.fetchPropertyValue).not.toHaveBeenCalled()
      expect(updateSpy).toHaveBeenCalledWith(QUIDDITY.id, PROPERTY.id, PROPERTY_VALUE)
    })

    it('should update a property if its value has changed', async () => {
      propertyStore.addProperty(QUIDDITY.id, NEW_PROPERTY)
      updateSpy.mockClear()
      await propertyStore.updatePropertyValue(QUIDDITY.id, PROPERTY, PROPERTY_VALUE_ALT)
      expect(updateSpy).toHaveBeenCalledWith(QUIDDITY.id, PROPERTY.id, PROPERTY_VALUE_ALT)
    })

    it('should not update property with the same value', async () => {
      propertyStore.addProperty(QUIDDITY.id, NEW_PROPERTY)
      updateSpy.mockClear()
      await propertyStore.updatePropertyValue(QUIDDITY.id, PROPERTY, PROPERTY_VALUE)
      expect(updateSpy).not.toHaveBeenCalled()
    })
  })

  describe('fetchPropertyValue', () => {
    beforeEach(() => {
      ({ propertyAPI } = socketStore.APIs)
    })

    it('should fetch the current model of a property', async () => {
      propertyAPI.get = jest.fn().mockResolvedValue(NEW_PROPERTY)
      expect(await propertyStore.fetchPropertyValue(QUIDDITY.id, PROPERTY)).toEqual(NEW_PROPERTY.value)
    })

    it('should not fetch the current model of a property with type group', async () => {
      propertyAPI.get = jest.fn()
      await propertyStore.fetchPropertyValue(QUIDDITY.id, GROUP_PROPERTY)
      expect(propertyAPI.get).not.toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if the fetch has failed', async () => {
      propertyAPI.get = jest.fn().mockRejectedValue(false)
      expect(await propertyStore.fetchPropertyValue(QUIDDITY.id, PROPERTY)).toEqual(null)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('handleSocketChange', () => {
    beforeEach(() => {
      ({ propertyAPI, infoTreeAPI } = socketStore.APIs)
      propertyStore.handleGraftedProperty = jest.fn()
      propertyStore.handleUpdatedProperty = jest.fn()
    })

    it('should define the APIs if there is a new socket', () => {
      const onGraftSpy = jest.spyOn(infoTreeAPI, 'onGrafted')
      const onUpdateSpy = jest.spyOn(propertyAPI, 'onUpdated')

      propertyStore.handleSocketChange(socket)

      expect(onGraftSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
      expect(onUpdateSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function)
      )
    })

    it('should handle a grafted property tree when the API is triggered', () => {
      propertyStore.properties.set(QUIDDITY.id, new Map([[PROPERTY.id, PROPERTY]]))
      socket.onEmit('info_tree_grafted', QUIDDITY.id, `property.${PROPERTY.id}`, true)
      expect(propertyStore.handleGraftedProperty).toHaveBeenCalledWith(QUIDDITY.id, `property.${PROPERTY.id}`, true)
    })

    it('should not handle a grafted tree when the grafted object is not a property', () => {
      socket.onEmit('info_tree_grafted', QUIDDITY.id, `${PROPERTY.id}`, true)
      expect(propertyStore.handleGraftedProperty).not.toHaveBeenCalled()
    })

    it('should handle an updated property when the API is triggered', () => {
      jest.spyOn(quiddityStore, 'userQuiddityIds', 'get').mockReturnValue([QUIDDITY.id])
      socket.onEmit('property_updated', QUIDDITY.id, `${PROPERTY.id}`, true, `${PROPERTY.id}`)
      expect(propertyStore.handleUpdatedProperty).toHaveBeenCalledWith(QUIDDITY.id, `${PROPERTY.id}`, true)
    })

    it('should not handle an updated property when the quiddity is not in the matrix', () => {
      socket.onEmit('property_updated', QUIDDITY.id, `${PROPERTY.id}`, true)
      expect(propertyStore.handleUpdatedProperty).not.toHaveBeenCalled()
    })
  })

  describe('initializeProperties', () => {
    beforeEach(() => {
      propertyStore.fetchPropertyValue = jest.fn().mockResolvedValue(PROPERTY_VALUE)
      propertyStore.updatePropertyValue = jest.fn().mockResolvedValue(true)
      propertyStore.handleUpdatedQuiddities = jest.fn()
      quiddityStore.addQuiddity(QUIDDITY)
    })

    it('should add all properties from a quiddity', async () => {
      await propertyStore.initializeProperties(QUIDDITY.id)

      expect(propertyStore.properties.get(QUIDDITY.id).toJSON()).toEqual(Array.from(new Map([
        [PROPERTY_JSON.id, Property.fromJSON(PROPERTY_JSON)],
        [PROPERTY_JSON_ALT.id, Property.fromJSON(PROPERTY_JSON_ALT)]
      ])))
    })

    it('should update all added properties', async () => {
      await propertyStore.initializeProperties(QUIDDITY.id)
      expect(propertyStore.updatePropertyValue).toHaveBeenNthCalledWith(1, QUIDDITY.id, Property.fromJSON(PROPERTY_JSON))
      expect(propertyStore.updatePropertyValue).toHaveBeenNthCalledWith(2, QUIDDITY.id, Property.fromJSON(PROPERTY_JSON_ALT))
    })

    it('should not initialize a quiddity if it was already initialized', async () => {
      propertyStore.addProperty(QUIDDITY.id, Property.fromJSON(PROPERTY_JSON))
      await propertyStore.initializeProperties(QUIDDITY.id)
      expect(propertyStore.updatePropertyValue).not.toHaveBeenCalled()
    })

    it('should log an error if the initialization has failed', async () => {
      propertyStore.updatePropertyValue = jest.fn().mockRejectedValue(false)
      await propertyStore.initializeProperties(QUIDDITY.id)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('isStartable', () => {
    it('should return true if the quiddity has the property `started`', () => {
      propertyStore.addProperty(QUIDDITY.id, Property.fromJSON({ ...PROPERTY_JSON, id: 'started' }))
      expect(propertyStore.isStartable(QUIDDITY.id)).toEqual(true)
    })

    it('should return false if the quiddity hasn\'t the property `started`', () => {
      expect(propertyStore.isStartable(QUIDDITY.id)).toEqual(false)
    })
  })

  describe('isHidden', () => {
    let sdiInput, videoOutput, testPattern, webcam

    beforeEach(() => {
      sdiInput = Quiddity.fromJSON(SDI_INPUT_QUIDDITY)
      videoOutput = Quiddity.fromJSON(VIDEO_OUTPUT_QUIDDITY)
      const sdiProperties = Property.fromQuiddity(sdiInput)

      for (const property of sdiProperties) {
        propertyStore.addProperty(sdiInput.id, property)
      }

      testPattern = Quiddity.fromJSON(TEST_PATTERN_QUIDDITY)
      const patternProperties = Property.fromQuiddity(testPattern)

      for (const property of patternProperties) {
        propertyStore.addProperty(testPattern.id, property)
      }

      webcam = Quiddity.fromJSON(WEBCAM_QUIDDITY)
      const webcamProperties = Property.fromQuiddity(webcam)

      for (const property of webcamProperties) {
        propertyStore.addProperty(webcam.id, property)
      }
    })

    it('should show width and height properties for sdi inputs if an associated property resolution is custom', () => {
      propertyStore.setPropertyValue(sdiInput.id, 'Capture/resolution', 6)
      expect(propertyStore.isHidden(sdiInput.id, 'Capture/width')).toEqual(false)
      expect(propertyStore.isHidden(sdiInput.id, 'Capture/height')).toEqual(false)
    })

    it('should hide width and height properties for sdi inputs if an associated property resolution is not custom', () => {
      propertyStore.setPropertyValue(sdiInput.id, 'Capture/resolution', 5)
      expect(propertyStore.isHidden(sdiInput.id, 'Capture/width')).toEqual(true)
      expect(propertyStore.isHidden(sdiInput.id, 'Capture/height')).toEqual(true)
    })

    it('should show width and height properties for test pattern videos if an associated property resolution is custom', () => {
      propertyStore.setPropertyValue(testPattern.id, 'Generator/resolution', 6)
      expect(propertyStore.isHidden(testPattern.id, 'Generator/width')).toEqual(false)
      expect(propertyStore.isHidden(testPattern.id, 'Generator/height')).toEqual(false)
    })

    it('should hide width and height properties for test pattern videos if an associated property resolution is not custom', () => {
      propertyStore.setPropertyValue(testPattern.id, 'Generator/resolution', 5)
      expect(propertyStore.isHidden(testPattern.id, 'Generator/width')).toEqual(true)
      expect(propertyStore.isHidden(testPattern.id, 'Generator/height')).toEqual(true)
    })

    it('should show width and height properties for webcam videos if an associated property resolution is custom', () => {
      propertyStore.setPropertyValue(webcam.id, 'WEBCAM/resolution', 6)
      expect(propertyStore.isHidden(webcam.id, 'WEBCAM/width')).toEqual(false)
      expect(propertyStore.isHidden(webcam.id, 'WEBCAM/height')).toEqual(false)
    })

    it('should hide width and height properties for webcam videos if an associated property resolution is not custom', () => {
      propertyStore.setPropertyValue(webcam.id, 'WEBCAM/resolution', 5)
      expect(propertyStore.isHidden(webcam.id, 'WEBCAM/width')).toEqual(true)
      expect(propertyStore.isHidden(webcam.id, 'WEBCAM/height')).toEqual(true)
    })

    it('should not hide width and height properties of a video output quiddity', () => {
      expect(propertyStore.isHidden(videoOutput.id, 'Window/width')).toEqual(false)
      expect(propertyStore.isHidden(videoOutput.id, 'Window/height')).toEqual(false)
    })
  })

  describe('isStarted', () => {
    it('should return true if the quiddity has the property `started` set on true', () => {
      propertyStore.addProperty(QUIDDITY.id, Property.fromJSON({ ...PROPERTY_JSON, id: 'started', value: true }))
      expect(propertyStore.isStarted(QUIDDITY.id)).toEqual(true)
    })

    it('should return false if the quiddity hasn\'t the property `started`', () => {
      expect(propertyStore.isStarted(QUIDDITY.id)).toEqual(false)
    })

    it('should return false if the quiddity has the property `started` set on false', () => {
      propertyStore.addProperty(QUIDDITY.id, Property.fromJSON({ ...PROPERTY_JSON, id: 'started', value: false }))
      expect(propertyStore.isStarted(QUIDDITY.id)).toEqual(false)
    })
  })

  describe('isEnabled', () => {
    it('should not enable a read-only property', () => {
      const property = Property.fromJSON({ ...PROPERTY_JSON, id: 'read-only', writable: false })
      propertyStore.addProperty(QUIDDITY.id, property)
      propertyStore.setPropertyStatus(QUIDDITY.id, property.id, true)
      expect(propertyStore.isEnabled(QUIDDITY.id, property.id)).toEqual(false)
    })

    it('should not enable a property with a falsy status', () => {
      const property = Property.fromJSON({ ...PROPERTY_JSON, id: 'falsy-status' })
      propertyStore.addProperty(QUIDDITY.id, property)
      propertyStore.setPropertyStatus(QUIDDITY.id, property.id, false)
      expect(propertyStore.isEnabled(QUIDDITY.id, property.id)).toEqual(false)
    })

    it('should enable a property with a true status', () => {
      const property = Property.fromJSON({ ...PROPERTY_JSON, id: 'true-status' })
      propertyStore.setPropertyStatus(QUIDDITY.id, property.id, true)
      propertyStore.addProperty(QUIDDITY.id, property)
      expect(propertyStore.isEnabled(QUIDDITY.id, property.id)).toEqual(true)
    })
  })

  describe('getPropertyUnit', () => {
    const H264_QUIDDITY_KIND_ID = 'h264Encoder'
    const H264_QUIDDITY_ID = 'h264Encoder0'

    const ENCODER_QUIDDITY_KIND = new Kind(H264_QUIDDITY_KIND_ID, 'test', 'test')

    const H264_KIND = Kind.fromJSON({
      ...ENCODER_QUIDDITY_KIND.toJSON(),
      kindId: H264_QUIDDITY_KIND_ID
    })

    const H264_QUIDDITY = Quiddity.fromJSON({
      ...QUIDDITY.toJSON(),
      id: H264_QUIDDITY_ID,
      kindId: H264_QUIDDITY_KIND_ID
    })

    beforeEach(() => {
      kindStore.addKind(H264_KIND)
      quiddityStore.addQuiddity(H264_QUIDDITY)
    })

    it('should get the property\'s unit', () => {
      jest.spyOn(quiddityStore, 'usedKinds', 'get').mockReturnValue(new Map().set(H264_QUIDDITY_ID, H264_KIND))
      const staticUnit = PROPERTY_UNIT_MAPPING[H264_QUIDDITY_KIND_ID]['Encoder/bitrate']
      expect(propertyStore.getPropertyUnit(H264_QUIDDITY_ID, 'Encoder/bitrate')).toEqual(staticUnit)
    })

    it('should get a null value if the property has no unit', () => {
      expect(propertyStore.getPropertyUnit(QUIDDITY.id, 'Encoder/bitrate')).toEqual(null)
    })
  })

  describe('getPropertyModel', () => {
    it('should get the property\'s model', () => {
      propertyStore.addProperty(QUIDDITY.id, PROPERTY)
      expect(propertyStore.getPropertyModel(QUIDDITY.id, PROPERTY.id)).toEqual(PROPERTY)
    })

    it('should return null if no property model is associatied with the given key', () => {
      expect(propertyStore.getPropertyModel(QUIDDITY.id, PROPERTY.id)).toEqual(null)
    })
  })

  describe('applyNewPropertyValue', () => {
    beforeEach(() => {
      ({ propertyAPI } = socketStore.APIs)
    })

    it('should set the new property value', async () => {
      propertyAPI.set = jest.fn().mockResolvedValue(true)
      await propertyStore.applyNewPropertyValue(QUIDDITY.id, PROPERTY.id, PROPERTY_VALUE)
      expect(propertyAPI.set).toHaveBeenCalledWith(QUIDDITY.id, PROPERTY.id, PROPERTY_VALUE)
    })

    it('should log an error if the apply has failed', async () => {
      propertyAPI.set = jest.fn().mockRejectedValue(false)
      await propertyStore.applyNewPropertyValue(QUIDDITY.id, PROPERTY.id, PROPERTY_VALUE)
      expect(LOG.error).toHaveBeenCalled()
    })
  })
})

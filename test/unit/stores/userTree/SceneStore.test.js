/* global describe it expect jest beforeEach */

import populateStores from '@stores/populateStores.js'
import Scene, { DEFAULT_SCENE_ID } from '@models/userTree/Scene'
import InitStateEnum from '@models/common/InitStateEnum'
import { configure } from 'mobx'

import { USER_TREE_ID } from '@stores/userTree/UserTreeStore'
import SceneStore, { LOG } from '@stores/userTree/SceneStore'
import { USER_TREE_NICKNAME } from '@models/quiddity/specialQuiddities'

import { Socket } from '@fixture/socket'
import USER_TREE_REV1 from '@fixture/models/userTree.revision1.json'

configure({ safeDescriptors: false })

describe('SceneStore', () => {
  const AVAILABLE_SCENES = Scene.fromUserTree(USER_TREE_REV1)
  const DEFAULT_SCENE = AVAILABLE_SCENES[0]
  const FIRST_SCENE = AVAILABLE_SCENES[1]

  let socket, socketStore, configStore, sceneStore, nicknameStore

  beforeEach(() => {
    ({ socketStore, sceneStore, nicknameStore } = populateStores())

    LOG.warn = jest.fn()
    LOG.error = jest.fn()

    socket = new Socket()
    socketStore.setActiveSocket(socket)
  })

  describe('constructor', () => {
    beforeEach(() => {
      sceneStore.handleSocketChange = jest.fn()
    })

    it('should fail if the configStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new SceneStore(socketStore) }).toThrow()
      /* eslint-enable no-new */
    })

    it('should fail if the quiddityStore is not provided', () => {
      /* eslint-disable no-new */
      expect(() => { new SceneStore(socketStore, configStore) }).toThrow()
      /* eslint-enable no-new */
    })

    it('should be correctly instantiated', () => {
      expect(sceneStore).toBeDefined()
      expect(sceneStore.initState).toEqual(InitStateEnum.NOT_INITIALIZED)
    })

    it('should react to the SocketStore\'s active socket change', () => {
      socketStore.activeSocket = new Socket()
      expect(sceneStore.handleSocketChange).toHaveBeenCalledWith(socketStore.activeSocket)
    })
  })

  describe('initialize', () => {
    beforeEach(() => {
      sceneStore.fetchUserTree = jest.fn().mockResolvedValue(USER_TREE_REV1)
      sceneStore.populateScenesFromUserTree = jest.fn().mockReturnValue(true)
      sceneStore.fallbackUserScenesInitialization = jest.fn().mockResolvedValue(true)
    })

    it('should initialize all scenes if they exist', async () => {
      expect(await sceneStore.initialize()).toEqual(true)
      expect(sceneStore.fetchUserTree).toHaveBeenCalled()
      expect(sceneStore.populateScenesFromUserTree).toHaveBeenCalledWith(USER_TREE_REV1)
      expect(sceneStore.fallbackUserScenesInitialization).not.toHaveBeenCalled()
      expect(sceneStore.initState).toEqual(InitStateEnum.INITIALIZED)
    })

    it('should not initialize if store is already initialized', async () => {
      sceneStore.setInitState(InitStateEnum.INITIALIZED)
      sceneStore.setInitState = jest.fn()

      expect(await sceneStore.initialize()).toEqual(false)
      expect(sceneStore.fetchUserTree).not.toHaveBeenCalled()
      expect(sceneStore.populateScenesFromUserTree).not.toHaveBeenCalled()
      expect(sceneStore.fallbackUserScenesInitialization).not.toHaveBeenCalled()
      expect(sceneStore.setInitState).not.toHaveBeenCalled()
    })

    it('should initialize with default scenes if fetched json is invalid', async () => {
      sceneStore.fetchUserTree = jest.fn().mockResolvedValue({})
      sceneStore.populateScenesFromUserTree = jest.fn().mockReturnValue(false)

      expect(await sceneStore.initialize()).toEqual(true)
      expect(sceneStore.populateScenesFromUserTree).toHaveBeenCalledWith({})
      expect(sceneStore.fallbackUserScenesInitialization).toHaveBeenCalled()
      expect(sceneStore.initState).toEqual(InitStateEnum.INITIALIZED)
    })
  })

  describe('handleSocketChange', () => {
    let userTreeAPI

    beforeEach(() => {
      ({ userTreeAPI } = socketStore.APIs)

      sceneStore.handleGraftedUserTreeScenes = jest.fn()
      sceneStore.handlePrunedUserTreeScenes = jest.fn()
      sceneStore.handleNewSession = jest.fn()
      sceneStore.clear = jest.fn()

      jest.spyOn(nicknameStore, 'nicknames', 'get').mockReturnValue(new Map().set(USER_TREE_ID, USER_TREE_NICKNAME))
    })

    it('should define API callbacks if there is a new socket', () => {
      const onGraftSpy = jest.spyOn(userTreeAPI, 'onGrafted')
      const onPruneSpy = jest.spyOn(userTreeAPI, 'onPruned')

      sceneStore.handleSocketChange(socket)

      expect(sceneStore.clear).toHaveBeenCalled()

      expect(onGraftSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )

      expect(onPruneSpy).toHaveBeenCalledWith(
        expect.any(Function),
        expect.any(Function),
        expect.any(Function)
      )
    })

    it('should clear the store if the new socket is null', () => {
      sceneStore.handleSocketChange(null)
      expect(sceneStore.clear).toHaveBeenCalled()
    })

    it('should handle grafted scene tree', () => {
      socket.onEmit('user_tree_grafted', USER_TREE_ID, '.scenes.test', DEFAULT_SCENE)
      expect(sceneStore.handleGraftedUserTreeScenes).toHaveBeenCalledWith(USER_TREE_ID, '.scenes.test', DEFAULT_SCENE)
    })

    it('should handle pruned scene tree', () => {
      socket.onEmit('user_tree_pruned', USER_TREE_ID, '.scenes.test')
      expect(sceneStore.handlePrunedUserTreeScenes).toHaveBeenCalledWith('.scenes.test')
    })
  })

  describe('populateScenesFromUserTree', () => {
    beforeEach(() => {
      sceneStore.addUserScene = jest.fn()
    })

    it('should not populate scenes if user data is empty', () => {
      expect(sceneStore.populateScenesFromUserTree({})).toEqual(false)
      expect(sceneStore.addUserScene).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should not populate scenes if "scenes" key doesn\'t exists', () => {
      expect(sceneStore.populateScenesFromUserTree({ test: 'test' })).toEqual(false)
      expect(sceneStore.addUserScene).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should not populate scenes if "scenes" key is empty', () => {
      expect(sceneStore.populateScenesFromUserTree({ scenes: {} })).toEqual(false)
      expect(sceneStore.addUserScene).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })

    it('should populate scenes with user data', () => {
      expect(sceneStore.populateScenesFromUserTree(USER_TREE_REV1)).toEqual(true)
      expect(sceneStore.addUserScene).toHaveBeenCalledTimes(2)
      expect(sceneStore.addUserScene).toHaveBeenNthCalledWith(1, Scene.fromJSON(USER_TREE_REV1.scenes.defaultScene))
      expect(sceneStore.addUserScene).toHaveBeenNthCalledWith(2, Scene.fromJSON(USER_TREE_REV1.scenes.scene1))
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if scene is malformed', () => {
      expect(sceneStore.populateScenesFromUserTree({ scenes: { failure: 'failure' } })).toEqual(false)
      expect(sceneStore.addUserScene).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('clear', () => {
    beforeEach(() => {
      AVAILABLE_SCENES.forEach(scene => sceneStore.userScenes.set(scene.id, scene))
    })

    it('should clear all user scenes', () => {
      expect(sceneStore.userScenes.size).toEqual(2)
      sceneStore.clear()
      expect(sceneStore.userScenes.size).toEqual(0)
    })
  })

  describe('handleGraftedUserTreeScenes', () => {
    const GRAFTED_PATH = `.scenes.${DEFAULT_SCENE_ID}`
    const PROPERTY_PATH = `.scenes.${DEFAULT_SCENE_ID}.active`

    let updateSpy

    beforeEach(() => {
      updateSpy = jest.spyOn(sceneStore, 'updateUserScene')
    })

    it('should add a new scene on grafted tree', async () => {
      expect(sceneStore.userScenes.size).toEqual(0)
      await sceneStore.handleGraftedUserTreeScenes(USER_TREE_ID, GRAFTED_PATH, DEFAULT_SCENE)
      expect(sceneStore.userScenes.get(DEFAULT_SCENE_ID)).toEqual(AVAILABLE_SCENES[0])
      expect(sceneStore.selectedSceneId).toEqual(DEFAULT_SCENE_ID)
    })

    it('should update scene on grafted property', async () => {
      sceneStore.userScenes.set(DEFAULT_SCENE_ID, DEFAULT_SCENE)
      expect(sceneStore.activeScene).not.toBeDefined()
      await sceneStore.handleGraftedUserTreeScenes(USER_TREE_ID, PROPERTY_PATH, true)
      expect(updateSpy).toHaveBeenCalledWith(DEFAULT_SCENE_ID, 'active', true)
      expect(sceneStore.activeScene.id).toEqual(DEFAULT_SCENE_ID)
    })

    it('should log an error if the handle have failed', async () => {
      await sceneStore.handleGraftedUserTreeScenes(USER_TREE_ID, GRAFTED_PATH, {})
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('handlePrunedUserTreeScenes', () => {
    const PRUNED_PATH = `.scenes.${FIRST_SCENE.id}`

    beforeEach(() => {
      sceneStore.userScenes.set(DEFAULT_SCENE_ID, DEFAULT_SCENE)
      sceneStore.userScenes.set(FIRST_SCENE.id, FIRST_SCENE)
    })

    it('should remove scene on pruned tree', () => {
      expect(sceneStore.userScenes.size).toEqual(2)
      sceneStore.handlePrunedUserTreeScenes(PRUNED_PATH)
      expect(sceneStore.userScenes.size).toEqual(1)
    })

    it('should select default scene if the pruned scene was selected', () => {
      sceneStore.setSelectedScene(FIRST_SCENE.id)
      sceneStore.handlePrunedUserTreeScenes(PRUNED_PATH)
      expect(sceneStore.selectedSceneId).toEqual(DEFAULT_SCENE_ID)
    })
  })

  describe('computeSceneIndex', () => {
    beforeEach(() => {
      sceneStore.userScenes.set(DEFAULT_SCENE_ID, DEFAULT_SCENE)
      sceneStore.userScenes.set(FIRST_SCENE.id, FIRST_SCENE)
    })

    it('should return a new index from all added scenes', () => {
      expect(sceneStore.computeSceneIndex()).toEqual(2)
    })
  })

  describe('makeSceneModel', () => {
    beforeEach(() => {
      sceneStore.userScenes.set(FIRST_SCENE.id, FIRST_SCENE)
      sceneStore.setSelectedScene(FIRST_SCENE.id)
    })

    it('should generate a scene with the connections of the selected scene', () => {
      const newScene = sceneStore.makeSceneModel()
      expect(newScene.id).toEqual('scene2')
      expect(newScene.connections).toEqual(FIRST_SCENE.connections)
    })
  })

  describe('applySceneCreation', () => {
    beforeEach(() => {
      sceneStore.applyUserTreeCreation = jest.fn().mockResolvedValue(true)
      sceneStore.userScenes.set(DEFAULT_SCENE_ID, DEFAULT_SCENE)
      sceneStore.userScenes.set(FIRST_SCENE.id, FIRST_SCENE)
    })

    it('should generate a new scene and request its creation', async () => {
      await sceneStore.applySceneCreation()
      expect(sceneStore.applyUserTreeCreation).toHaveBeenCalledWith('.scenes.scene2', expect.any(Object))
    })

    it('should log an error if the creation has failed', async () => {
      sceneStore.applyUserTreeCreation = jest.fn().mockRejectedValue(false)
      await sceneStore.applySceneCreation()
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applySceneRemoval', () => {
    beforeEach(() => {
      sceneStore.applyUserTreeRemoval = jest.fn().mockResolvedValue(true)
      sceneStore.userScenes.set(FIRST_SCENE.id, FIRST_SCENE)
    })

    it('should remove the scene', async () => {
      await sceneStore.applySceneRemoval(FIRST_SCENE.id)
      expect(sceneStore.applyUserTreeRemoval).toHaveBeenCalledWith(FIRST_SCENE.branch)
    })

    it('should log an error if the removal has failed', async () => {
      sceneStore.applyUserTreeRemoval = jest.fn().mockRejectedValue(false)
      await sceneStore.applySceneRemoval(FIRST_SCENE.id)
      expect(LOG.error).toHaveBeenCalled()
    })
  })

  describe('applyActiveSceneSwitch', () => {
    const SCENE_TOGGLE = true

    beforeEach(() => {
      sceneStore.applyUserTreeCreation = jest.fn().mockResolvedValue(true)
      sceneStore.userScenes.set(DEFAULT_SCENE_ID, DEFAULT_SCENE)
      sceneStore.userScenes.set(FIRST_SCENE.id, FIRST_SCENE)
    })

    it('should apply toggle on the scene', async () => {
      await sceneStore.applyActiveSceneSwitch(FIRST_SCENE.id, SCENE_TOGGLE)
      expect(sceneStore.applyUserTreeCreation).toHaveBeenCalledTimes(1)
      expect(sceneStore.applyUserTreeCreation).toHaveBeenCalledWith(`${FIRST_SCENE.branch}.active`, SCENE_TOGGLE)
    })

    it('should disactivate previous active scene', async () => {
      sceneStore.updateUserScene(DEFAULT_SCENE_ID, 'active', SCENE_TOGGLE)
      await sceneStore.applyActiveSceneSwitch(FIRST_SCENE.id, SCENE_TOGGLE)
      expect(sceneStore.applyUserTreeCreation).toHaveBeenCalledTimes(2)
      expect(sceneStore.applyUserTreeCreation).toHaveBeenNthCalledWith(1, `${DEFAULT_SCENE.branch}.active`, !SCENE_TOGGLE)
      expect(sceneStore.applyUserTreeCreation).toHaveBeenNthCalledWith(2, `${FIRST_SCENE.branch}.active`, SCENE_TOGGLE)
    })

    it('should log an error if the switch has failed', async () => {
      sceneStore.applyUserTreeCreation = jest.fn().mockRejectedValue(false)
      await sceneStore.applyActiveSceneSwitch(FIRST_SCENE.id)
      expect(LOG.error).toHaveBeenCalled()
    })
  })
})

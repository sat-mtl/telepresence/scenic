/* global describe it jest expect beforeEach */

import populateStores from '~src/stores/populateStores'
import { deinitializeStores, initializeStores, initializeSocket } from '@stores/initializeStores'

describe('initializeStores', () => {
  let stores
  let drawerStore, quiddityStore, configStore
  let quiddityMenuStore, kindStore, socketStore, sceneStore, userTreeStore, lockStore

  beforeEach(() => {
    stores = populateStores()

    ;({
      drawerStore,
      quiddityStore,
      configStore,
      quiddityMenuStore,
      sceneStore,
      kindStore,
      socketStore,
      userTreeStore,
      lockStore
    } = stores)

    quiddityStore.initialize = jest.fn()
    kindStore.initialize = jest.fn()
    quiddityMenuStore.addMenuBinding = jest.fn()
  })

  it('should not initialize the rest of the app if the configStore is not initialized', async () => {
    configStore.initialize = jest.fn().mockResolvedValueOnce(false)
    await initializeStores(stores)

    expect(quiddityStore.initialize).not.toHaveBeenCalled()
    expect(kindStore.initialize).not.toHaveBeenCalled()
  })

  it('should initialize the kindStore if the configStore is initialized', async () => {
    configStore.initialize = jest.fn().mockResolvedValue(true)
    await initializeStores(stores)

    expect(kindStore.initialize).toHaveBeenCalled()
  })

  it('should initialize the quiddityStore if the configStore and the kindStore are initialized', async () => {
    kindStore.initialize = jest.fn().mockResolvedValue(true)
    await initializeStores(stores)

    expect(quiddityStore.initialize).toHaveBeenCalled()
  })

  it('should set menu bindings if the configStore, the kindStore, and the quiddityStore are initialized', async () => {
    kindStore.initialize = jest.fn().mockResolvedValue(true)
    configStore.initialize = jest.fn().mockResolvedValue(true)
    quiddityStore.initialize = jest.fn().mockResolvedValue(true)
    await initializeStores(stores)

    expect(quiddityMenuStore.addMenuBinding).toHaveBeenCalled()
  })

  describe('deinitializeStores', () => {
    it('should clear all 4 stores when called', () => {
      sceneStore.clear = jest.fn()
      quiddityStore.clear = jest.fn()
      configStore.clear = jest.fn()
      drawerStore.clearActiveDrawer = jest.fn()
      userTreeStore.clear = jest.fn()
      lockStore.clear = jest.fn()
      deinitializeStores(stores)

      expect(sceneStore.clear).toHaveBeenCalled()
      expect(quiddityStore.clear).toHaveBeenCalled()
      expect(configStore.clear).toHaveBeenCalled()
      expect(drawerStore.clearActiveDrawer).toHaveBeenCalled()
      expect(userTreeStore.clear).toHaveBeenCalled()
      expect(lockStore.clear).toHaveBeenCalled()
    })
  })

  describe('initializeSocket', () => {
    it('should call onConnection, onDisconnection and applyConnection when invoked', () => {
      socketStore.onConnection = jest.fn()
      socketStore.onDisconnection = jest.fn()
      socketStore.applyConnection = jest.fn()
      initializeSocket(stores)

      expect(socketStore.onConnection).toHaveBeenCalled()
      expect(socketStore.onDisconnection).toHaveBeenCalled()
      expect(socketStore.applyConnection).toHaveBeenCalled()
    })
  })
})

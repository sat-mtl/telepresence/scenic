/* global describe it expect jest beforeEach */

import { Socket } from '@fixture/socket'
import { configure } from 'mobx'
import MaxLimitStore, { LOG, restrictedKinds } from '@stores/shmdata/MaxLimitStore'

import populateStores from '@stores/populateStores.js'
import { videoOutput, xr18Output } from '@fixture/allQuiddities'
import { getKind } from '@fixture/allQuiddityKinds'

configure({ safeDescriptors: false })

describe('MaxLimitStore', () => {
  let socketStore, maxLimitStore, quiddityStore, specStore, kindStore

  beforeEach(() => {
    ({ socketStore, maxLimitStore, quiddityStore, specStore, kindStore } = populateStores())

    LOG.error = jest.fn()
    LOG.warn = jest.fn()

    socketStore.setActiveSocket(new Socket())
  })

  describe('constructor', () => {
    it('should fail if the quiddityStore is not given', () => {
      expect(() => new MaxLimitStore()).toThrow()
    })

    it('should fail if the specStore is not given', () => {
      expect(() => new MaxLimitStore(quiddityStore)).toThrow()
    })

    it('should not fail if all required stores are given', () => {
      expect(() => new MaxLimitStore(quiddityStore, specStore)).not.toThrow()
    })

    it('should be well instantiated', () => {
      expect(maxLimitStore).toBeDefined()
      expect(maxLimitStore.maxReaderLimits.size).toEqual(0)
    })
  })

  describe('maxReaderLimits', () => {
    beforeEach(() => {
      quiddityStore.addQuiddity(videoOutput)
      kindStore.addKind(getKind(videoOutput))
      quiddityStore.addQuiddity(xr18Output)
      kindStore.addKind(getKind(xr18Output))
    })

    it('should add the max reader limits of the video output quiddity with a value equal to the size its follower specs', () => {
      specStore.addFollowerSpec(videoOutput.id, videoOutput.connectionSpecs.follower[0])
      expect(maxLimitStore.maxReaderLimits).toEqual(new Map().set(videoOutput.id, 1))
      specStore.addFollowerSpec(videoOutput.id, videoOutput.connectionSpecs.follower[0])
      expect(maxLimitStore.maxReaderLimits).toEqual(new Map().set(videoOutput.id, 2))
      specStore.addFollowerSpec(videoOutput.id, videoOutput.connectionSpecs.follower[0])
      expect(maxLimitStore.maxReaderLimits).toEqual(new Map().set(videoOutput.id, specStore.followerSpecs.get(videoOutput.id).size))
    })

    it('should add the default max reader limits of of a quiddity with a restricted kind', () => {
      specStore.addFollowerSpec(xr18Output.id, xr18Output.connectionSpecs.follower[0])
      specStore.addFollowerSpec(xr18Output.id, xr18Output.connectionSpecs.follower[0])

      expect(maxLimitStore.maxReaderLimits).not.toEqual(new Map().set(xr18Output.id, specStore.followerSpecs.get(xr18Output.id).size))
      expect(maxLimitStore.maxReaderLimits).toEqual(new Map().set(xr18Output.id, restrictedKinds.get(xr18Output.kindId)))
    })
  })
})

/* global describe it expect beforeEach */

import { Socket, mockSocketCallback } from '@fixture/api/socket.mock'

import SessionAPI from '@api/switcher/SessionAPI'

describe('SessionAPI', () => {
  const SESSION_ERROR = new Error()

  let api, socket, mockEmitSocketCallback

  beforeEach(() => {
    socket = new Socket()
    api = new SessionAPI(socket)
    mockEmitSocketCallback = mockSocketCallback.bind(null, api.socket, 'emit')
  })

  describe('constructor', () => {
    it('should SessionAPI to be defined', () => {
      expect(api).toBeDefined()
      expect(api.socket).toStrictEqual(socket)
    })
  })

  describe('reset', () => {
    const execResetSession = async () => {
      return api.reset()
    }

    it('should emit `session_reset`', async () => {
      await execResetSession()

      expect(api.socket.emit).toHaveBeenCalledWith(
        'session_reset',
        expect.any(Function)
      )
    })

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitSocketCallback(null, true)
      expect(await execResetSession()).toEqual(true)
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitSocketCallback(SESSION_ERROR)
      await expect(execResetSession()).rejects.toEqual(SESSION_ERROR)
    })
  })

  describe('clear', () => {
    const execClearSession = async () => {
      return api.clear()
    }

    it('should emit `session_clear`', async () => {
      await execClearSession()

      expect(api.socket.emit).toHaveBeenCalledWith(
        'session_clear',
        expect.any(Function)
      )
    })

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitSocketCallback(null, 'test')
      expect(await execClearSession()).toEqual('test')
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitSocketCallback(SESSION_ERROR)
      await expect(execClearSession()).rejects.toEqual(SESSION_ERROR)
    })

    it('should not resolve the request when the callback contains nothing', async () => {
      mockEmitSocketCallback(null, null)
      expect(await execClearSession()).toEqual(null)
    })
  })

  describe('list', () => {
    const execListSession = async () => {
      return api.list()
    }

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitSocketCallback(null, [])
      expect(await execListSession()).toEqual([])
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitSocketCallback(SESSION_ERROR)
      await expect(execListSession()).rejects.toEqual(SESSION_ERROR)
    })
  })

  describe('save_as', () => {
    const execSaveAsSession = async (sessionName) => {
      return api.saveAs(sessionName)
    }

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitSocketCallback(null, 'test')
      expect(await execSaveAsSession('test')).toEqual('test')
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitSocketCallback(SESSION_ERROR)
      await expect(execSaveAsSession('test')).rejects.toEqual(SESSION_ERROR)
    })
  })

  describe('remove', () => {
    const execRemoveSession = async (sessionName) => {
      return api.remove(sessionName)
    }

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitSocketCallback(null, true)
      expect(await execRemoveSession('test')).toEqual(true)
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitSocketCallback(SESSION_ERROR)
      await expect(execRemoveSession('test')).rejects.toEqual(SESSION_ERROR)
    })
  })

  describe('read', () => {
    const execReadSession = async (sessionName) => {
      return api.read(sessionName)
    }

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitSocketCallback(null, true)
      expect(await execReadSession('test')).toEqual(true)
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitSocketCallback(SESSION_ERROR)
      await expect(execReadSession('test')).rejects.toEqual(SESSION_ERROR)
    })
  })
})

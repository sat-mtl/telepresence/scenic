/* global jest describe it expect beforeEach */

import { Socket, mockSocketCallback } from '@fixture/api/socket.mock'

import InfoTreeAPI from '@api/tree/InfoTreeAPI'

describe('InfoTreeAPI', () => {
  const QUIDDITY_TEST = 'test'
  const TREE_PATH_TEST = 'test'
  const INFO_TREE_DATA_TEST = 'test'
  const TEST_VALUE = '"test"'
  const INFO_TREE_DATA_ERROR = new Error()

  let api, socket, mockEmitSocketCallback

  beforeEach(() => {
    socket = new Socket()
    api = new InfoTreeAPI(socket)
    mockEmitSocketCallback = mockSocketCallback.bind(null, api.socket, 'emit')
  })

  describe('constructor', () => {
    it('should InfoTreeAPI to be defined', () => {
      expect(api).toBeDefined()
      expect(api.socket).toStrictEqual(socket)
    })
  })

  describe('get', () => {
    const execInfoTreeData = async (quiddityID, path) => {
      return api.get(quiddityID, path)
    }

    it('should emit `info_tree_get`', async () => {
      mockEmitSocketCallback(null, null)
      await execInfoTreeData(QUIDDITY_TEST, TREE_PATH_TEST)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'info_tree_get',
        QUIDDITY_TEST,
        TREE_PATH_TEST,
        expect.any(Function)
      )
    })

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitSocketCallback(null, INFO_TREE_DATA_TEST)
      expect(await execInfoTreeData()).toEqual(INFO_TREE_DATA_TEST)
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitSocketCallback(INFO_TREE_DATA_ERROR)
      await expect(execInfoTreeData()).rejects.toEqual(INFO_TREE_DATA_ERROR)
    })

    it('should resolve the request when the callback contains nothing', async () => {
      mockEmitSocketCallback(null, null)
      await expect(execInfoTreeData()).resolves.toStrictEqual({})
    })
  })

  describe('onGrafted', () => {
    let onGraftAction

    beforeEach(() => {
      onGraftAction = jest.fn()
    })

    const execOnGrafted = (quiddityId, path) => {
      api.onGrafted(onGraftAction, quiddityId, path)
      socket.onEmit('info_tree_grafted', QUIDDITY_TEST, TREE_PATH_TEST, TEST_VALUE)
    }

    it('should handle `info_tree_grafted` by calling a defined onGraftAction function', () => {
      execOnGrafted(QUIDDITY_TEST, TREE_PATH_TEST)

      expect(api.socket.on).toHaveBeenCalledWith(
        'info_tree_grafted',
        expect.any(Function)
      )
    })

    it('should call onGraftAction function when `quiddityId` and `path` is matching', () => {
      execOnGrafted(QUIDDITY_TEST, TREE_PATH_TEST)
      expect(onGraftAction).toHaveBeenCalledWith(QUIDDITY_TEST, TREE_PATH_TEST, JSON.parse(TEST_VALUE))
    })

    it('shouldn\'t call onGraftAction function when `quiddityId` and/or `path` isn\'t matching', () => {
      execOnGrafted('I am a failure')
      expect(onGraftAction).not.toHaveBeenCalled()
    })
  })

  describe('onPruned', () => {
    let onPruneAction

    beforeEach(() => {
      onPruneAction = jest.fn()
    })

    const execOnPruned = (quiddityId, path) => {
      api.onPruned(onPruneAction, quiddityId, path)
      socket.onEmit('info_tree_pruned', QUIDDITY_TEST, TREE_PATH_TEST)
    }

    it('should handle `info_tree_pruned` by calling a defined onPruneAction function', () => {
      execOnPruned(QUIDDITY_TEST, TREE_PATH_TEST)

      expect(api.socket.on).toHaveBeenCalledWith(
        'info_tree_pruned',
        expect.any(Function)
      )
    })

    it('should call onPruneAction function when `quiddityId` and `path` is matching', () => {
      execOnPruned(QUIDDITY_TEST, TREE_PATH_TEST)
      expect(onPruneAction).toHaveBeenCalledWith(QUIDDITY_TEST, TREE_PATH_TEST)
    })

    it('shouldn\'t call onPruneAction function when `quiddityId` and/or `path` isn\'t matching', () => {
      execOnPruned('I am a failure')
      expect(onPruneAction).not.toHaveBeenCalled()
    })
  })
})

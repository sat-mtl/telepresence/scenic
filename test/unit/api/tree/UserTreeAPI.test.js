/* global jest describe it expect beforeEach */

import { Socket, mockSocketCallback } from '@fixture/api/socket.mock'

import UserTreeAPI from '@api/tree/UserTreeAPI'

describe('UserTreeAPI', () => {
  const QUIDDITY_TEST = 'test'
  const BRANCH_PATH_TEST = 'test'
  const USER_DATA_TEST = 'test'
  const TEST_VALUE = 'test'
  const USER_DATA_ERROR = new Error()

  let api, socket, mockEmitSocketCallback

  beforeEach(() => {
    socket = new Socket()
    api = new UserTreeAPI(socket)
    mockEmitSocketCallback = mockSocketCallback.bind(null, api.socket, 'emit')
  })

  describe('constructor', () => {
    it('should expect UserTreeAPI to be defined', () => {
      expect(api).toBeDefined()
      expect(api.socket).toStrictEqual(socket)
    })
  })

  describe('get', () => {
    const execGetUserData = async (quiddityID, path) => {
      return api.get(quiddityID, path)
    }

    it('should emit `user_tree_get`', async () => {
      mockEmitSocketCallback(null, USER_DATA_TEST)
      await execGetUserData(QUIDDITY_TEST, BRANCH_PATH_TEST)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'user_tree_get',
        QUIDDITY_TEST,
        BRANCH_PATH_TEST,
        expect.any(Function)
      )
    })

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitSocketCallback(null, USER_DATA_TEST)
      expect(await execGetUserData()).toEqual(USER_DATA_TEST)
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitSocketCallback(USER_DATA_ERROR)
      await expect(execGetUserData()).rejects.toEqual(USER_DATA_ERROR)
    })

    it('should resolve the request when the callback contains nothing', async () => {
      mockEmitSocketCallback(null, null)
      await expect(execGetUserData()).resolves.toStrictEqual({})
    })
  })

  describe('graft', () => {
    const execGraftUserData = async (quiddityID, branchPath, data) => {
      return api.graft(quiddityID, branchPath, data)
    }

    it('should emit `user_tree_graft`', async () => {
      mockEmitSocketCallback(null, true)
      await execGraftUserData(QUIDDITY_TEST, BRANCH_PATH_TEST, USER_DATA_TEST)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'user_tree_graft',
        QUIDDITY_TEST,
        BRANCH_PATH_TEST,
        USER_DATA_TEST,
        expect.any(Function)
      )
    })

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitSocketCallback(null, true)
      expect(await execGraftUserData()).toEqual(true)
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitSocketCallback(USER_DATA_ERROR)
      await expect(execGraftUserData()).rejects.toEqual(USER_DATA_ERROR)
    })

    it('should resolve the request when the callback contains nothing', async () => {
      mockEmitSocketCallback(null, null)
      expect(await execGraftUserData()).toEqual(null)
    })
  })

  describe('prune', () => {
    const execPruneUserData = async (quiddityID, branchPath) => {
      return api.prune(quiddityID, branchPath)
    }

    it('should emit `user_tree_prune`', async () => {
      mockEmitSocketCallback(null, true)
      await execPruneUserData(QUIDDITY_TEST, BRANCH_PATH_TEST)

      expect(api.socket.emit).toHaveBeenCalledWith(
        'user_tree_prune',
        QUIDDITY_TEST,
        BRANCH_PATH_TEST,
        expect.any(Function)
      )
    })

    it('should resolve the request when a success callback is emitted', async () => {
      mockEmitSocketCallback(null, true)
      expect(await execPruneUserData()).toEqual(true)
    })

    it('should reject the request when an error callback is emitted', async () => {
      mockEmitSocketCallback(USER_DATA_ERROR)
      await expect(execPruneUserData()).rejects.toEqual(USER_DATA_ERROR)
    })

    it('should resolve the request when the callback contains nothing', async () => {
      mockEmitSocketCallback(null, null)
      expect(await execPruneUserData()).toEqual(null)
    })
  })

  describe('onGrafted', () => {
    let onGraftAction

    beforeEach(() => {
      onGraftAction = jest.fn()
    })

    const execOnGrafted = (quiddityId, path) => {
      api.onGrafted(onGraftAction, quiddityId, path)
      socket.onEmit('user_tree_grafted', QUIDDITY_TEST, BRANCH_PATH_TEST, TEST_VALUE)
    }

    it('should handle `user_tree_grafted` by calling a defined onGraftAction function', () => {
      execOnGrafted(QUIDDITY_TEST, BRANCH_PATH_TEST)

      expect(api.socket.on).toHaveBeenCalledWith(
        'user_tree_grafted',
        expect.any(Function)
      )
    })

    it('should call onGraftAction function when `quiddityId` and `path` is matching', () => {
      execOnGrafted(QUIDDITY_TEST, BRANCH_PATH_TEST)
      expect(onGraftAction).toHaveBeenCalledWith(QUIDDITY_TEST, BRANCH_PATH_TEST, TEST_VALUE)
    })

    it('shouldn\'t call onGraftAction function when `quiddityId` and/or `path` isn\'t matching', () => {
      execOnGrafted('I am a failure')
      expect(onGraftAction).not.toHaveBeenCalled()
    })
  })

  describe('onPruned', () => {
    let onPruneAction

    beforeEach(() => {
      onPruneAction = jest.fn()
    })

    const execOnPruned = (quiddityId, path) => {
      api.onPruned(onPruneAction, quiddityId, path)
      socket.onEmit('user_tree_pruned', QUIDDITY_TEST, BRANCH_PATH_TEST)
    }

    it('should handle `user_tree_pruned` by calling a defined onPruneAction function', () => {
      execOnPruned(QUIDDITY_TEST, BRANCH_PATH_TEST)

      expect(api.socket.on).toHaveBeenCalledWith(
        'user_tree_pruned',
        expect.any(Function)
      )
    })

    it('should call onPruneAction function when `quiddityId` and `path` is matching', () => {
      execOnPruned(QUIDDITY_TEST, BRANCH_PATH_TEST)
      expect(onPruneAction).toHaveBeenCalledWith(QUIDDITY_TEST, BRANCH_PATH_TEST)
    })

    it('shouldn\'t call onPruneAction function when `quiddityId` and/or `path` isn\'t matching', () => {
      execOnPruned('I am a failure')
      expect(onPruneAction).not.toHaveBeenCalled()
    })
  })
})

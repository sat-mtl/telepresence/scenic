/* global describe it expect jest beforeEach */

import hash from 'hash-it'
import NdiStream from '@models/quiddity/NdiStream'

jest.mock('hash-it')

describe('NdiStream', () => {
  let defaultNdi, parsedNdi

  const DEFAULT_HASH = 'test'
  const NDI_ID = `ndi_${DEFAULT_HASH}`
  const NDI_NAME = 'test'
  const NDI_ENTRY = 'TEST (test)'
  const NDI_HOST = 'TEST'
  const SWITCHER_PORT = '8000'

  beforeEach(() => {
    hash.mockReturnValue(DEFAULT_HASH)
    defaultNdi = new NdiStream(NDI_ID, NDI_NAME, NDI_ENTRY, NDI_HOST, SWITCHER_PORT)
    parsedNdi = NdiStream.fromNdiEntry(NDI_ENTRY, SWITCHER_PORT)
  })

  describe('constructor', () => {
    it('should define a default NDI Stream', () => {
      expect(defaultNdi.id).toEqual(NDI_ID)
      expect(defaultNdi.name).toEqual(NDI_NAME)
      expect(defaultNdi.label).toEqual(NDI_ENTRY)
      expect(defaultNdi.host).toEqual(NDI_HOST)
      expect(defaultNdi.swPort).toEqual(SWITCHER_PORT)
    })

    it('should define a default NDI Stream with a default switcher port', () => {
      const ndi = new NdiStream(NDI_ID, NDI_NAME, NDI_ENTRY, NDI_HOST)
      expect(ndi.swPort).toEqual(SWITCHER_PORT)
    })
  })

  describe('toOption', () => {
    it('should return a selectable option', () => {
      expect(defaultNdi.toOption()).toEqual({
        value: NDI_NAME,
        label: NDI_ENTRY
      })
    })
  })

  describe('fromOutput', () => {
    it('should returns every stream define in the output', () => {
      expect(NdiStream.fromOutput(`
        ${NDI_ENTRY}
        ${NDI_ENTRY}
      `)).toEqual([defaultNdi, defaultNdi])
    })
  })

  describe('fromNdiEntry', () => {
    it('should define a default NDI Stream', () => {
      expect(parsedNdi.id).toEqual(NDI_ID)
      expect(parsedNdi.name).toEqual(NDI_NAME)
      expect(parsedNdi.label).toEqual(NDI_ENTRY)
      expect(parsedNdi.host).toEqual(NDI_HOST)
      expect(parsedNdi.swPort).toEqual(SWITCHER_PORT)
    })

    it('should throw an error if the entry is not well defined', () => {
      expect(() => NdiStream.fromNdiEntry('TEST test')).toThrow()
    })
  })

  describe('hash', () => {
    it('should return the original hash', () => {
      expect(parsedNdi.hash).toEqual(DEFAULT_HASH)
    })
  })

  describe('directory', () => {
    it('should return a directory name based on the hash', () => {
      expect(parsedNdi.directory).toEqual(`/tmp/scenic/${NDI_ID}`)
    })
  })

  describe('commandLine', () => {
    it('should return a command line for ndi2shmdata', () => {
      expect(parsedNdi.commandLine).toEqual([
        `ndi2shmdata -n '${parsedNdi.label}'`,
        `-v ${parsedNdi.directory}/ndi${parsedNdi.hash}_video`,
        `-a ${parsedNdi.directory}/ndi${parsedNdi.hash}_audio`
      ].join(' '))
    })
  })

  describe('properties', () => {
    it('should return the NdiInput initial properties', () => {
      expect(parsedNdi.properties).toEqual({
        'Executor/command_line': parsedNdi.commandLine,
        'Watcher/directory': parsedNdi.directory
      })
    })
  })
})

/* global jest beforeEach describe it expect */

import Capabilities, { LOG } from '@models/shmdata/Capabilities'
import CapabilityGroupEnum from '@models/shmdata/CapabilityGroupEnum'

const ARRAY_CAPS = [
  'video/x-raw',
  'width=(int)800',
  'height=(int)600',
  'framerate=(fraction)25/1',
  'pixel-aspect-ratio=(fraction)1/1',
  'format=(string)I420'
]

const ALT_CAPS = [
  'video/x-raw',
  'framerate=25/1',
  'pixel-aspect-ratio=1/1',
  'format=I420'
]

const STRING_CAPS = ARRAY_CAPS.join(', ')
const STRING_CAPS_NO_SPACES = ARRAY_CAPS.join(',')

describe('Capabilities', () => {
  describe('fromArray', () => {
    it('should build the caps model from an array', () => {
      expect(
        Capabilities.fromArray(ARRAY_CAPS).toArray()
      ).toEqual(ARRAY_CAPS)
    })

    it('should be compatible with caps without types', () => {
      expect(
        Capabilities.fromArray(ALT_CAPS).toArray()
      ).toEqual(ALT_CAPS)
    })
  })

  describe('getInt', () => {
    it('should get the width property of the caps', () => {
      expect(
        Capabilities.fromArray(ARRAY_CAPS).getInt('width')
      ).toEqual(800)
    })

    it('should return 0 if the caps has no width property', () => {
      expect(
        Capabilities.fromArray(ALT_CAPS).getInt('width')
      ).toEqual(NaN)
    })

    it('should return 0 if the caps is not an integer', () => {
      expect(
        Capabilities.fromArray(ARRAY_CAPS).getInt('framerate')
      ).toEqual(NaN)
    })
  })

  describe('width & height', () => {
    it('should return the width and the height of the caps', () => {
      const caps = Capabilities.fromArray(ARRAY_CAPS)
      expect(caps.width).toEqual(800)
      expect(caps.height).toEqual(600)
    })

    it('should return 0 if the caps have no width and height properties', () => {
      const caps = Capabilities.fromArray(ALT_CAPS)
      expect(caps.width).toEqual(0)
      expect(caps.height).toEqual(0)
    })
  })

  describe('groups', () => {
    let caps

    beforeEach(() => {
      caps = Capabilities.fromArray(ARRAY_CAPS)
    })

    it('should create a dimension group if there are both width and height properties', () => {
      expect(caps.groups.has(CapabilityGroupEnum.DIMENSIONS)).toEqual(true)
    })

    it('should not create a dimension group if there is not width property', () => {
      caps.properties.delete('width')
      expect(caps.groups.has(CapabilityGroupEnum.DIMENSIONS)).toEqual(false)
    })

    it('should not create a dimension group if there is not height property', () => {
      caps.properties.delete('height')
      expect(caps.groups.has(CapabilityGroupEnum.DIMENSIONS)).toEqual(false)
    })

    it('should group all un-grouped properties in the others group', () => {
      expect(caps.groups.get(CapabilityGroupEnum.OTHERS).size).toEqual(3)
    })

    it('should not create the other group if there is no un-grouped property', () => {
      caps.properties.delete('format')
      caps.properties.delete('framerate')
      caps.properties.delete('pixel-aspect-ratio')
      expect(caps.groups.has(CapabilityGroupEnum.OTHERS)).toEqual(false)
    })
  })

  describe('fromString', () => {
    it('should build the caps model from a string', () => {
      expect(
        Capabilities.fromString(STRING_CAPS).toString()
      ).toEqual(STRING_CAPS)
      expect(
        Capabilities.fromString(STRING_CAPS_NO_SPACES).toString()
      ).toEqual(STRING_CAPS)
    })
  })

  describe('fromCaps', () => {
    beforeEach(() => {
      Capabilities.fromArray = jest.fn()
      Capabilities.fromString = jest.fn()
      LOG.error = jest.fn()
    })

    it('should infer a string caps', () => {
      Capabilities.fromCaps(STRING_CAPS)
      expect(Capabilities.fromString).toHaveBeenCalled()
      expect(Capabilities.fromArray).not.toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should infer an array of caps', () => {
      Capabilities.fromCaps(ARRAY_CAPS)
      expect(Capabilities.fromString).not.toHaveBeenCalled()
      expect(Capabilities.fromArray).toHaveBeenCalled()
      expect(LOG.error).not.toHaveBeenCalled()
    })

    it('should log an error if the caps are not a string or an array', () => {
      Capabilities.fromCaps({})
      expect(Capabilities.fromString).not.toHaveBeenCalled()
      expect(Capabilities.fromArray).not.toHaveBeenCalled()
      expect(LOG.error).toHaveBeenCalled()
    })
  })
})

/* global describe it expect beforeEach */

import MenuCollection from '@models/menus/MenuCollection'
import MenuItem from '@models/menus/MenuItem'

describe('MenuCollection', () => {
  const testToken = 'test'
  const testToken2 = 'test'

  const makeMenuCollection = () => {
    return MenuCollection.fromJSON({
      items: [
        { name: testToken, quiddity: testToken },
        new MenuItem(testToken2, testToken2),
        { name: testToken },
        new MenuCollection(testToken2)
      ],
      hidden: true,
      name: testToken
    })
  }

  describe('fromJSON', () => {
    it('should instantiate custom menu group with default values', () => {
      const group = MenuCollection.fromJSON({ name: testToken })

      // test default values
      expect(group.hidden).toEqual(false)
      expect(group.items).toEqual([])

      // test defined values
      expect(group.name).toEqual(testToken)
    })

    it('should instantiate custom menu group with values of different types', () => {
      const items = [
        new MenuItem(testToken, testToken),
        new MenuItem(testToken2, testToken2),
        new MenuCollection(testToken),
        new MenuCollection(testToken2)
      ]

      const group = makeMenuCollection()

      // test defined values
      expect(group.items).toEqual(items)
      expect(group.hidden).toEqual(true)
      expect(group.name).toEqual(testToken)
    })

    it('should provide all ids of its grouped items', () => {
      const group = makeMenuCollection()
      expect(group.ids).toEqual([testToken, testToken2])
    })

    it('should instantiate custom menu group with aliased values', () => {
      const items = [new MenuItem(testToken, testToken), new MenuItem(testToken2, testToken2)]
      const group = MenuCollection.fromJSON({ name: testToken, items: items })
      const group2 = MenuCollection.fromJSON({ name: testToken, subMenus: items })

      expect(group).toEqual(group2)
    })

    it('should fail when custom menu has invalid items', () => {
      expect(() => {
        MenuCollection.fromJSON({
          name: 'test',
          items: 'test',
          categories: [testToken, testToken],
          exclusive: true,
          hidden: true
        })
      }).toThrowError()
    })

    it('should fail when custom menu item has no name', () => {
      expect(() => {
        MenuCollection.fromJSON({
          items: [],
          categories: [testToken, testToken],
          exclusive: true,
          hidden: true
        })
      }).toThrowError()
    })

    it('should fail when the items are not parsable', () => {
      expect(() => {
        MenuCollection.fromJSON({
          name: 'test',
          items: [{ test: testToken }],
          categories: [testToken, testToken],
          exclusive: true,
          hidden: true
        })
      }).toThrowError()
    })
  })

  describe('addQuiddityKind', () => {
    const categoryTest = 'test'
    const quiddityTest = 'test'

    const groupTest = new MenuCollection(testToken, [
      new MenuCollection(categoryTest, [
        new MenuItem(quiddityTest, quiddityTest, [categoryTest])
      ])
    ])

    let group

    beforeEach(() => { group = new MenuCollection(testToken) })

    it('should not add a quiddity kind without `category`', () => {
      group.addQuiddityKind({ name: quiddityTest })
      expect(group.items).toEqual([])
    })

    it('should add a new collection and an item from a quiddity kind', () => {
      group.addQuiddityKind({ name: quiddityTest, category: categoryTest })
      expect(group).toEqual(groupTest)
    })

    it('should not add a new category if still exists in the item', () => {
      group.items.push(new MenuCollection(categoryTest))
      group.addQuiddityKind({ name: quiddityTest, category: categoryTest })
      expect(group).toEqual(groupTest)
    })
  })

  describe('updateItem', () => {
    let group, jsonConfig

    beforeEach(() => {
      jsonConfig = {
        items: [
          { name: 'test1', quiddity: 'test1' },
          { name: 'test2', quiddity: 'test2' }
        ],
        hidden: false,
        name: testToken
      }

      group = MenuCollection.fromJSON(jsonConfig)
    })

    it('should update the created property of the correponding item', () => {
      const created = true
      group.updateItem('test1', { created })
      const Item = group.items.find(item => item.name === 'test1')
      expect(Item.created).toEqual(created)
    })

    it('should not update the created property if the item id doesn\'t matches', () => {
      const created = true
      group.updateItem('dumb', { created })

      group.items.forEach(item => {
        expect(item.created).toEqual(false)
      })
    })
  })

  describe('show', () => {
    const jsonGroup = {
      items: [
        {
          categories: [],
          name: testToken,
          quiddity: testToken,
          hidden: false,
          created: false,
          exclusive: false
        }
      ],
      hidden: false,
      name: testToken
    }

    it('should show the object only if `hidden` and `exclusive` are false', () => {
      const group = MenuCollection.fromJSON(jsonGroup)

      // WARNING: I ignore why, but this test reset some cache and make the test pass
      expect(group.items.filter(m => m.showable).length).toEqual(1)
      expect(group.show()).toEqual(jsonGroup)
    })

    it('should show only showable items', () => {
      const jsonGroup2 = {
        items: [
          { name: testToken, quiddity: testToken },
          { name: testToken, quiddity: testToken, hidden: true }
        ],
        hidden: false,
        name: testToken
      }

      const group = MenuCollection.fromJSON(jsonGroup2)

      expect(group.items.filter(m => m.showable).length).toEqual(1)
      expect(group.show()).toEqual(jsonGroup)
    })

    it('should show nothing if all items are not showable', () => {
      const jsonGroup2 = {
        items: [
          { name: testToken, quiddity: testToken, hidden: true },
          { name: testToken, quiddity: testToken, hidden: true }
        ],
        hidden: false,
        name: testToken
      }

      const group = MenuCollection.fromJSON(jsonGroup2)

      // WARNING: I ignore why, but this test reset some cache and make the test pass
      expect(group.items.filter(m => m.showable).length).toEqual(0)
      expect(group.show()).toEqual(null)
    })

    it('should show nothing if it is hidden', () => {
      const jsonGroup2 = {
        items: [
          { name: testToken, quiddity: testToken },
          { name: testToken, quiddity: testToken, hidden: true }
        ],
        hidden: true,
        name: testToken
      }

      const group = MenuCollection.fromJSON(jsonGroup2)

      expect(group.items.filter(m => m.showable).length).toEqual(1)
      expect(group.show()).toEqual(null)
    })
  })

  describe('showable', () => {
    it('should filter all hidden custom menus', () => {
      const group = MenuCollection.fromJSON({
        items: [{ name: testToken, quiddity: testToken }],
        hidden: true,
        name: testToken
      })

      expect(group.showable).toEqual(false)
    })

    it('should filter all groups which have no items', () => {
      const group = MenuCollection.fromJSON({
        items: [],
        name: testToken
      })

      expect(group.showable).toEqual(false)
    })

    it('should filter all groups which have all items filtered', () => {
      const group = MenuCollection.fromJSON({
        items: [
          { name: testToken, quiddity: testToken },
          { name: testToken, quiddity: testToken }
        ],
        name: testToken
      })

      expect(group.items.length).toEqual(2)
      expect(group.showable).toEqual(true)

      group.items = group.items.map(item => {
        item.hidden = true
        return item
      })

      group.items.forEach(item => {
        expect(item instanceof MenuItem).toEqual(true)
        expect(item.showable).toEqual(false)
      })

      expect(group.showable).toEqual(false)
    })
  })
})

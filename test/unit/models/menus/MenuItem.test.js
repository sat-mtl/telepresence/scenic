/* global describe it expect beforeEach */

import MenuItem from '@models/menus/MenuItem'

describe('MenuItem', () => {
  const testToken = 'test'

  describe('constructor', () => {
    it('should instantiate custom menu item with default values', () => {
      const item = MenuItem.fromJSON({
        name: testToken,
        quiddity: testToken
      })

      // test default values
      expect(item.categories).toEqual([])
      expect(item.hidden).toEqual(false)
      expect(item.exclusive).toEqual(false)

      // test defined values
      expect(item.name).toEqual(testToken)
      expect(item.quiddity).toEqual(testToken)
    })

    it('should provide an id from its quiddity attribute', () => {
      const item = new MenuItem(testToken, testToken)
      expect(item.id).toEqual(testToken)
    })

    it('should instantiate custom menu item with every values', () => {
      const item = MenuItem.fromJSON({
        categories: [testToken, testToken],
        exclusive: true,
        hidden: true,
        name: testToken,
        quiddity: testToken
      })

      // test defined values
      expect(item.categories).toEqual([testToken, testToken])
      expect(item.hidden).toEqual(true)
      expect(item.exclusive).toEqual(true)
      expect(item.name).toEqual(testToken)
      expect(item.quiddity).toEqual(testToken)
    })

    it('should fail when custom menu is not well formated', () => {
      expect(() => {
        MenuItem.fromJSON({ // eslint-disable-line no-new
          categories: [testToken, testToken],
          exclusive: testToken,
          hidden: true
        })
      }).toThrowError()
    })
  })

  describe('updateItem', () => {
    let Item

    beforeEach(() => {
      Item = MenuItem.fromJSON({
        categories: [testToken, testToken],
        exclusive: false,
        created: false,
        hidden: false,
        name: testToken,
        quiddity: testToken
      })
    })

    it('should update the created property of the corresponding item', () => {
      const created = true
      Item.updateItem(testToken, { created })
      expect(Item.created).toEqual(created)
    })

    it('should not update the created token if the item id doesn\'t matches', () => {
      const created = true
      Item.updateItem('dumb', { created })
      expect(Item.created).not.toEqual(created)
    })
  })

  describe('show', () => {
    let jsonConfig, Item

    beforeEach(() => {
      jsonConfig = {
        categories: [testToken, testToken],
        exclusive: false,
        created: false,
        hidden: false,
        name: testToken,
        quiddity: testToken
      }

      Item = MenuItem.fromJSON(jsonConfig)
    })

    it('should show the item only if its properties `hidden` and `exclusive` are false', () => {
      const menuClone = Item.show()
      expect(menuClone).toEqual(jsonConfig)
    })

    it('shouldn\'t show the menu when it is not showable', () => {
      Item.hidden = true
      expect(Item.show()).toEqual(null)
    })
  })

  describe('showable', () => {
    it('should be able to show all hidden custom menus', () => {
      const Item = MenuItem.fromJSON({
        hidden: true,
        name: testToken,
        quiddity: testToken
      })

      expect(Item.showable).toEqual(false)
    })

    it('should filter all exclusive and created custom menus', () => {
      const Item = MenuItem.fromJSON({
        exclusive: true,
        name: testToken,
        quiddity: testToken
      })

      expect(Item.showable).toEqual(true)
      Item.created = true
      expect(Item.showable).toEqual(false)
    })

    it('should not filter all other configurations', () => {
      const Item = MenuItem.fromJSON({
        name: testToken,
        quiddity: testToken
      })

      expect(Item.showable).toEqual(true)
      Item.created = true
      expect(Item.showable).toEqual(true)
    })
  })
})

/* global describe it expect */

import Connection from '@models/userTree/Connection'
import connection0 from '@fixture/connection0.sample.json'
import { SIP_ID } from '@models/quiddity/specialQuiddities'

const SHMDATA_SUFFIX = 'video'
const SOURCE_ID = '1'
const DESTINATION_ID = '1'

describe('Connection', () => {
  const CONNECTION = new Connection(SOURCE_ID, DESTINATION_ID, SHMDATA_SUFFIX, null, null, 0)

  describe('constructor', () => {
    it('should throw an error if it is constructed without sourceId', () => {
      expect(() => { new Connection() }).toThrow() // eslint-disable-line no-new
    })

    it('should throw an error if it is constructed without destinationId', () => {
      expect(() => { new Connection(SOURCE_ID) }).toThrow() // eslint-disable-line no-new
    })

    it('should throw an error if it is constructed with a sip destination and a contact id of type different than string', () => {
      expect(() => { new Connection(SOURCE_ID, SIP_ID, 1) }).toThrow() // eslint-disable-line no-new
    })
  })

  describe('fromJSON', () => {
    it('should create a Connection from a valid JSON', () => {
      expect(Connection.fromJSON(connection0)).toEqual(CONNECTION)
    })

    it('should throw an error from an invalid JSON', () => {
      expect(() => { Connection.fromJSON({}) }).toThrow()
    })
  })

  describe('toJSON', () => {
    it('should return an identical JSON', () => {
      expect(Connection.fromJSON(connection0).toJSON()).toEqual(connection0)
    })
  })
})

/* global describe it expect */

import StatusEnum, { fromBuddyStatus, fromSendStatus, fromRecvStatus } from '@models/common/StatusEnum'
import { SendStatusEnum, BuddyStatusEnum, RecvStatusEnum } from '@models/sip/SipEnums'

describe('StatusEnum', () => {
  describe('fromBuddyStatus', () => {
    it('should return an active status from an online buddy', () => {
      expect(fromBuddyStatus(BuddyStatusEnum.ONLINE)).toEqual(StatusEnum.ACTIVE)
    })

    it('should return an inactive status from offline buddy', () => {
      expect(fromBuddyStatus(BuddyStatusEnum.OFFLINE)).toEqual(StatusEnum.INACTIVE)
    })

    it('should return a warning status from an away buddy', () => {
      expect(fromBuddyStatus(BuddyStatusEnum.AWAY)).toEqual(StatusEnum.BUSY)
    })

    it('should return a warning status from a busy buddy', () => {
      expect(fromBuddyStatus(BuddyStatusEnum.BUSY)).toEqual(StatusEnum.BUSY)
    })

    it('should return an error status from an unknown buddy', () => {
      expect(fromBuddyStatus(BuddyStatusEnum.UNKNOWN)).toEqual(StatusEnum.DANGER)
    })

    it('should return an error status for all the other cases', () => {
      expect(fromBuddyStatus(undefined)).toEqual(StatusEnum.DANGER)
    })
  })

  describe('fromRecvStatus', () => {
    it('should return an active status from a receiving recv status', () => {
      expect(fromRecvStatus(RecvStatusEnum.RECEIVING)).toEqual(StatusEnum.ACTIVE)
    })

    it('should return an inactive status from disconnected recv status', () => {
      expect(fromRecvStatus(RecvStatusEnum.DISCONNECTED)).toEqual(StatusEnum.INACTIVE)
    })

    it('should return a busy status from a connecting recv status', () => {
      expect(fromRecvStatus(RecvStatusEnum.CONNECTING)).toEqual(StatusEnum.BUSY)
    })

    it('should return an error status from an unknown buddy', () => {
      expect(fromRecvStatus(RecvStatusEnum.UNKNOWN)).toEqual(StatusEnum.DANGER)
    })

    it('should return an error status for all the other cases', () => {
      expect(fromRecvStatus(undefined)).toEqual(StatusEnum.DANGER)
    })
  })

  describe('fromSendStatus', () => {
    it('should return an active status from a calling sending status', () => {
      expect(fromSendStatus(SendStatusEnum.CALLING)).toEqual(StatusEnum.ACTIVE)
    })

    it('should return an inactive status from disconnected sending status', () => {
      expect(fromSendStatus(SendStatusEnum.DISCONNECTED)).toEqual(StatusEnum.INACTIVE)
    })

    it('should return a warning status from a connecting sending status', () => {
      expect(fromSendStatus(SendStatusEnum.CONNECTING)).toEqual(StatusEnum.BUSY)
    })

    it('should return an error status from an unknown buddy', () => {
      expect(fromSendStatus(SendStatusEnum.UNKNOWN)).toEqual(StatusEnum.DANGER)
    })

    it('should return an error status for all the other cases', () => {
      expect(fromSendStatus(undefined)).toEqual(StatusEnum.DANGER)
    })
  })
})

/* global describe it expect */

import StatusEnum from '@models/common/StatusEnum'
import Notification from '@models/common/Notification'

describe('Notification', () => {
  const MESSAGE = 'message'
  const DESCRIPTION = 'description'
  const STATUS = StatusEnum.DANGER
  const DURATION = 1000

  const JSON = {
    message: MESSAGE,
    description: DESCRIPTION,
    status: STATUS,
    duration: DURATION
  }

  const DEFAULT = {
    message: MESSAGE,
    description: null,
    status: StatusEnum.FOCUS,
    duration: 5000
  }

  describe('constructor', () => {
    it('should fail if no message is given', () => {
      expect(() => new Notification()).toThrow('message')
    })

    it('should set all the optional parameters by default', () => {
      expect(new Notification(MESSAGE, undefined, undefined, undefined)).toMatchObject(DEFAULT)
    })

    it('should set all the parameters set by the user', () => {
      expect(new Notification(MESSAGE, DESCRIPTION, STATUS, DURATION)).toMatchObject(JSON)
    })

    it('should always generate unique IDs', () => {
      expect((new Notification(MESSAGE)).id).not.toEqual((new Notification(MESSAGE)).id)
    })
  })

  describe('isPermanent', () => {
    it('should not be permanent by default', () => {
      const notification = new Notification(MESSAGE)
      expect(notification.isPermanent).toEqual(false)
    })

    it('should be permanent if the duration is NaN', () => {
      const notification = new Notification(MESSAGE, undefined, undefined, NaN)
      expect(notification.isPermanent).toEqual(true)
    })
  })

  describe('fromString', () => {
    it('should create a simple notification', () => {
      expect(Notification.fromString(MESSAGE)).toBeInstanceOf(Notification)
    })
  })

  describe('toJSON', () => {
    it('should return a JSON object', () => {
      const notification = new Notification(MESSAGE, DESCRIPTION, STATUS, DURATION)
      expect(notification.toJSON()).toMatchObject(JSON)
    })
  })
})

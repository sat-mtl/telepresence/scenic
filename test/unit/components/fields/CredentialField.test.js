/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'

import { AppStoresContext } from '@components/App'
import populateStores from '@stores/populateStores.js'

import StatusEnum from '@models/common/StatusEnum'
import CredentialField from '@components/fields/CredentialField'
import { Inputs } from '@sat-mtl/ui-components'

import '@utils/i18n'

const { InputText, Field } = Inputs

const TEST_KEY = 'test'
const TEST_VALUE = 'test'

describe('<CredentialField />', () => {
  let helpMessageStore

  beforeEach(() => {
    ({ helpMessageStore } = populateStores())
  })

  function renderCredentialField (props) {
    return mount(
      <AppStoresContext.Provider value={{ helpMessageStore }}>
        <CredentialField
          key={TEST_KEY}
          {...props}
        />
      </AppStoresContext.Provider>
    )
  }

  it('should render the credential value from the state', () => {
    const $field = renderCredentialField({ value: TEST_VALUE })
    expect($field.find(InputText).prop('value')).toEqual(TEST_VALUE)
  })

  it('should set an empty string if the credential is null', () => {
    const $empty = renderCredentialField({ value: undefined })
    expect($empty.find(InputText).prop('value')).toEqual('')
  })

  it('should render a danger status when the credential is not valid', () => {
    const $withError = renderCredentialField({ error: TEST_VALUE })
    expect($withError.find(Field).prop('status')).toEqual(StatusEnum.DANGER)
  })

  const getInput = $field => $field.find('input')

  describe('callbacks', () => {
    let callbackSpy

    beforeEach(() => {
      callbackSpy = jest.fn()
    })

    describe.each([
      ['onChange', 'change', {}],
      ['onCheck', 'blur', {}],
      ['onCheck', 'keypress', { key: 'Enter' }]
    ])('%s', (callbackName, eventName, eventData) => {
      it(`should be called when the ${eventName} event is triggered on a text input`, () => {
        const $field = renderCredentialField({ [callbackName]: callbackSpy })
        getInput($field).simulate(eventName, { ...eventData, target: { value: TEST_VALUE } })
        expect(callbackSpy).toHaveBeenCalledWith(TEST_VALUE)
      })

      it(`should be called with defaults when the ${eventName} event is triggered on a text input without value`, () => {
        const $field = renderCredentialField({ [callbackName]: callbackSpy })
        getInput($field).simulate(eventName, { ...eventData })
        expect(callbackSpy).toHaveBeenCalledWith('')
      })

      it(`should called when the ${eventName} event is triggered on a password input`, () => {
        const $field = renderCredentialField({ isPassword: true, [callbackName]: callbackSpy })
        getInput($field).simulate(eventName, { ...eventData, target: { value: TEST_VALUE } })
        expect(callbackSpy).toHaveBeenCalledWith(TEST_VALUE)
      })

      it(`should be called with defaults when the ${eventName} event is triggered on a password input without value`, () => {
        const $field = renderCredentialField({ [callbackName]: callbackSpy })
        getInput($field).simulate(eventName, { ...eventData })
        expect(callbackSpy).toHaveBeenCalledWith('')
      })
    })
  })
})

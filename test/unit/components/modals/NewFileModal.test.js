/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'

import NewFileModal from '@components/modals/NewFileModal'
import { CancelButton, ConfirmButton } from '@components/common/Buttons'
import StatusEnum from '@models/common/StatusEnum'

import { Inputs } from '@sat-mtl/ui-components'

import '@utils/i18n'

const { Field } = Inputs
const FILE_ID = 'test'

describe('<NewFileModal />', () => {
  let confirmMock, cancelMock

  beforeEach(() => {
    confirmMock = jest.fn()
    cancelMock = jest.fn()
  })

  function mountModal (isRenaming = false) {
    return mount(
      <NewFileModal
        visible
        onConfirm={confirmMock}
        onCancel={cancelMock}
      />
    )
  }

  const getCancelButton = $modal => $modal.update().find(CancelButton)
  const getConfirmButton = $modal => $modal.update().find(ConfirmButton)
  const getField = $modal => $modal.update().find(Field)

  describe('<CancelButton/>', () => {
    it('should cancel the modal', () => {
      getCancelButton(mountModal()).simulate('click')
      expect(cancelMock).toHaveBeenCalled()
    })
  })

  describe('<ConfirmButton />', () => {
    it('should confirm when the inputted file name is undefined', () => {
      getConfirmButton(mountModal()).simulate('click')
      expect(confirmMock).not.toHaveBeenCalled()
    })

    it('should confirm and clean the modal', () => {
      const $modal = mountModal()
      $modal.setProps({ selectedFileName: FILE_ID })
      getConfirmButton($modal).simulate('click')
      expect(confirmMock).toHaveBeenCalledWith(FILE_ID)
    })

    it('should encourage the user to create a new file', () => {
      const $button = getConfirmButton(mountModal())
      expect($button.prop('children')).toEqual('Create')
      expect($button.prop('status')).toEqual('primary')
    })

    it('should warn the user when he is overwriting a file', () => {
      const $modal = mountModal()
      $modal.setProps({ fileNameList: [FILE_ID], selectedFileName: FILE_ID })
      expect(getConfirmButton($modal).prop('children')).toEqual('Overwrite')
      expect(getConfirmButton($modal).prop('status')).toEqual('danger')
    })
  })

  describe('<Field />', () => {
    it('should have a focus status when the field is empty', () => {
      expect(getField(mountModal()).prop('status')).toEqual(StatusEnum.FOCUS)
    })

    it('should have a focus status when the field is filled', () => {
      const $modal = mountModal()
      $modal.setProps({ selectedFileName: FILE_ID })
      expect(getField($modal).prop('status')).toEqual(StatusEnum.FOCUS)
    })

    it('should have a danger status when the new file has the same name as the saved files', () => {
      const $modal = mountModal()
      $modal.setProps({ fileNameList: [FILE_ID], selectedFileName: FILE_ID })
      expect(getField($modal).prop('status')).toEqual(StatusEnum.DANGER)
    })
  })
})

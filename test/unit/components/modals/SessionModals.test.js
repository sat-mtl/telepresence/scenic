/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'

import { CancelButton, ConfirmButton } from '@components/common/Buttons'

import populateStores from '@stores/populateStores.js'

import '@utils/i18n'
import {
  OpenSessionModal,
  OPEN_SESSION_MODAL_ID,
  ResetSessionModal,
  RESET_SESSION_MODAL_ID,
  TrashCurrentSessionModal,
  TRASH_CURRENT_SESSION_MODAL_ID
} from '@components/modals/SessionModals'

describe('<SessionModals />', () => {
  let modalStore
  let confirmMock, cancelMock

  beforeEach(() => {
    ({ modalStore } = populateStores())

    confirmMock = jest.fn()
    cancelMock = jest.fn()
  })

  function mountOpenSessionModal () {
    return mount(
      <OpenSessionModal
        visible
        onConfirm={confirmMock}
        onCancel={cancelMock}
      />
    )
  }

  function mountResetSessionModal () {
    return mount(
      <ResetSessionModal
        visible
        onConfirm={confirmMock}
        onCancel={cancelMock}
      />
    )
  }

  function mountTrashCurrentSessionModal () {
    return mount(
      <TrashCurrentSessionModal
        visible
        onConfirm={confirmMock}
        onCancel={cancelMock}
      />
    )
  }

  const getCancelButton = $modal => $modal.update().find(CancelButton)

  describe('<OpenSessionModal />', () => {
    it('should set the visible prop to true when an open session modal is requested', () => {
      const $modal = mountOpenSessionModal()
      modalStore.setActiveModal(OPEN_SESSION_MODAL_ID)
      expect($modal.exists()).toBe(true)
      expect($modal.prop('visible')).toEqual(true)
    })

    it('should render a Cancel button and a Confirm button', () => {
      const $wrapper = mountOpenSessionModal()
      const $cancelButton = $wrapper.find(CancelButton)
      const $confirmButton = $wrapper.find(ConfirmButton)
      expect($cancelButton.length).toEqual(1)
      expect($confirmButton.length).toEqual(1)
    })

    it('should cancel the modal when the cancel button is clicked', () => {
      getCancelButton(mountOpenSessionModal()).simulate('click')
      expect(cancelMock).toHaveBeenCalled()
    })

    it('should trigger the onConfirm function when clicked', () => {
      mountOpenSessionModal().simulate('click')
      expect(confirmMock).not.toHaveBeenCalled()
    })
  })

  describe('<OpenSessionModal />', () => {
    it('should set the visible prop to true when an open session modal is requested', () => {
      const $modal = mountResetSessionModal()
      modalStore.setActiveModal(RESET_SESSION_MODAL_ID)
      expect($modal.exists()).toBe(true)
      expect($modal.prop('visible')).toEqual(true)
    })

    it('should render a Cancel button and a Confirm button', () => {
      const $wrapper = mountResetSessionModal()
      const $cancelButton = $wrapper.find(CancelButton)
      const $confirmButton = $wrapper.find(ConfirmButton)
      expect($cancelButton.length).toEqual(1)
      expect($confirmButton.length).toEqual(1)
    })

    it('should cancel the modal when the cancel button is clicked', () => {
      getCancelButton(mountResetSessionModal()).simulate('click')
      expect(cancelMock).toHaveBeenCalled()
    })

    it('should trigger the onConfirm function when clicked', () => {
      mountResetSessionModal().simulate('click')
      expect(confirmMock).not.toHaveBeenCalled()
    })
  })

  describe('<TrashCurrentSessionModal />', () => {
    it('should set the visible prop to true when an open session modal is requested', () => {
      const $modal = mountTrashCurrentSessionModal()
      modalStore.setActiveModal(TRASH_CURRENT_SESSION_MODAL_ID)
      expect($modal.exists()).toBe(true)
      expect($modal.prop('visible')).toEqual(true)
    })

    it('should render a Cancel button and a Confirm button', () => {
      const $wrapper = mountTrashCurrentSessionModal()
      const $cancelButton = $wrapper.find(CancelButton)
      const $confirmButton = $wrapper.find(ConfirmButton)
      expect($cancelButton.length).toEqual(1)
      expect($confirmButton.length).toEqual(1)
    })

    it('should cancel the modal when the cancel button is clicked', () => {
      getCancelButton(mountTrashCurrentSessionModal()).simulate('click')
      expect(cancelMock).toHaveBeenCalled()
    })

    it('should trigger the onConfirm function when clicked', () => {
      mountTrashCurrentSessionModal().simulate('click')
      expect(confirmMock).not.toHaveBeenCalled()
    })
  })
})

/* global describe it expect beforeEach */

import React from 'react'
import { mount } from 'enzyme'

import populateStores from '@stores/populateStores.js'
import { AppStoresContext } from '@components/App'
import Matrix, { MatrixLayout, EmptySourceColumnArea } from '@components/matrix/Matrix'

describe('<Matrix />', () => {
  let stores

  beforeEach(() => {
    stores = populateStores()
  })

  function mountMatrix () {
    return mount(
      <AppStoresContext.Provider value={stores}>
        <Matrix stores={stores} />
      </AppStoresContext.Provider>
    )
  }

  describe('<MatrixLayout />', () => {
    it('should be instantiated once by the Matrix', () => {
      expect(mountMatrix().find(MatrixLayout).length).toEqual(1)
    })
  })

  describe('<EmptySourceColumnArea />', () => {
    it('should render an EmptySourceColumnArea when no source is instantiated', () => {
      expect(mountMatrix().find(EmptySourceColumnArea).length).toEqual(1)
    })
  })
})

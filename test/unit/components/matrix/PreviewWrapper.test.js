/* global describe beforeEach it expect jest */

import React from 'react'
import { mount } from 'enzyme'

import PreviewWrapper from '@components/matrix/PreviewWrapper'
import { AppStoresContext } from '@components/App'

import populateStores from '@stores/populateStores'
import { Feedback } from '@sat-mtl/ui-components'

import PreviewStore from '@stores/timelapse/PreviewStore'

const { Preview } = Feedback

describe('<PreviewWrapper />', () => {
  let shmdataStore, previewStore

  beforeEach(() => {
    ({ previewStore, shmdataStore } = populateStores())
  })

  const mountPreview = () => {
    return mount(
      <AppStoresContext.Provider value={{ previewStore, shmdataStore }}>
        <PreviewWrapper />
      </AppStoresContext.Provider>

    )
  }

  describe('render', () => {
    it('should not render a  preview by default', () => {
      expect(mountPreview().find(Preview).length).toEqual(0)
    })

    it('should render a preview if the preview url is set', () => {
      previewStore.url = 'test'
      expect(mountPreview().find(Preview).length).toEqual(1)
    })
  })

  describe('<PreviewWrapper />', () => {
    it('should not have shmdata or quiddity data attributes when an entry is not created', () => {
      const $preview = mountPreview().find('#PreviewWrapper')
      expect($preview.find('data-shmdata').exists()).toEqual(false)
      expect($preview.find('data-quiddity').exists()).toEqual(false)
    })

    it('should have a shmdata and a quiddity data attributes when an entry is created', () => {
      jest.spyOn(PreviewStore.prototype, 'previewShmdataPath', 'get').mockReturnValue('test-shmdataPath')
      jest.spyOn(PreviewStore.prototype, 'previewQuiddityId', 'get').mockReturnValue('test-quiddityId')
      const $preview = mountPreview().find('#PreviewWrapper')
      expect($preview.prop('data-shmdata')).toEqual(previewStore.previewShmdataPath)
      expect($preview.prop('data-quiddity')).toEqual(previewStore.previewQuiddityId)
    })
  })
})

/* global describe it expect beforeEach */
import React from 'react'
import { mount } from 'enzyme'

import SceneControlPanel from '@components/panels/SceneControlPanel'
import Scene, { DEFAULT_SCENE_ID } from '@models/userTree/Scene'
import populateStores from '@stores/populateStores.js'

import { AppStoresContext } from '@components/App'
import { Inputs } from '@sat-mtl/ui-components'
const { InputText } = Inputs

describe('<SceneControlPanel />', () => {
  const DEFAULT_SCENE = new Scene(DEFAULT_SCENE_ID, undefined, true)
  const SCENE_ACTIVE = new Scene('test', undefined, true)
  let sceneStore, helpMessageStore

  beforeEach(() => {
    ({ sceneStore, helpMessageStore } = populateStores())
  })

  function mountPanel () {
    return mount(
      <AppStoresContext.Provider value={{ sceneStore, helpMessageStore }}>
        <SceneControlPanel />
      </AppStoresContext.Provider>
    )
  }

  describe('renderSceneInput', () => {
    beforeEach(() => {
      sceneStore.addUserScene(DEFAULT_SCENE)
      sceneStore.addUserScene(SCENE_ACTIVE)
    })

    it('should render an InputText if a scene is selected', () => {
      sceneStore.setSelectedScene(SCENE_ACTIVE.id)
      expect(mountPanel().find(InputText).length).toEqual(1)
    })
  })

  describe('getSceneName', () => {
    beforeEach(() => {
      sceneStore.addUserScene(SCENE_ACTIVE)
    })

    it('should render the selected scene name if none is inputted', () => {
      sceneStore.setSelectedScene(SCENE_ACTIVE.id)
      const $input = mountPanel().find(InputText)
      expect($input.prop('value')).toEqual(SCENE_ACTIVE.name)
    })
  })
})

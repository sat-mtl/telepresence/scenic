/* global describe it expect jest beforeEach */
import React from 'react'
import { mount } from 'enzyme'

import populateStores from '@stores/populateStores.js'
import FilterPanel from '@components/panels/FilterPanel'

import { AppStoresContext } from '@components/App'

import { Common } from '@sat-mtl/ui-components'
const { Tag, Menu: { Item } } = Common

describe('<FilterPanel />', () => {
  let filterStore

  const FILTERS = ['filter0', 'filter1']

  const CATEGORIES = new Map([
    ['quid0', 'category0'],
    ['quid1', 'category1']
  ])

  beforeEach(() => {
    ({ filterStore } = populateStores())
    filterStore.applyQuiddityFilter = jest.fn().mockReturnValue(true)
  })

  function mountFilterPanel () {
    return mount(
      <AppStoresContext.Provider value={{ filterStore }}>
        <FilterPanel />
      </AppStoresContext.Provider>
    )
  }

  function setupFilters (filters = FILTERS) {
    filters.forEach(f => filterStore.addCategoryFilter(f))
  }

  function setupCategories (categories = CATEGORIES) {
    filterStore.setOrderedCategories(categories)
  }

  describe('<MenuItem />', () => {
    beforeEach(() => setupCategories(
      new Map([
        ['quid0', 'category0']
      ])
    ))

    it('should add a filter when it was not added', () => {
      const $panel = mountFilterPanel()
      const $item = $panel.find(Item)

      expect(filterStore.categoryFilters.size).toEqual(0)
      $item.simulate('click')
      expect(filterStore.categoryFilters.size).toEqual(1)
    })

    it('should delete a filter when it was added', () => {
      const $panel = mountFilterPanel()
      const $item = $panel.find(Item)

      filterStore.addCategoryFilter(
        Array.from(CATEGORIES.values())[0]
      )

      expect(filterStore.categoryFilters.size).toEqual(1)
      $item.simulate('click')
      expect(filterStore.categoryFilters.size).toEqual(0)
    })
  })

  describe('<FilterTag />', () => {
    beforeEach(() => setupFilters(['filter0']))

    it('should delete the filter category if clicked', () => {
      const $panel = mountFilterPanel()
      const $tag = $panel.find(Tag)

      expect(filterStore.categoryFilters.size).toEqual(1)
      $tag.simulate('click')
      expect(filterStore.categoryFilters.size).toEqual(0)
    })
  })

  describe('<ClearLink />', () => {
    beforeEach(() => setupFilters())

    it('should clear the filter categories if clicked', () => {
      const $panel = mountFilterPanel()
      const $link = $panel.find('a')

      expect(filterStore.categoryFilters.size).toEqual(FILTERS.length)
      $link.simulate('click')
      expect(filterStore.categoryFilters.size).toEqual(0)
    })
  })

  describe('<Tags />', () => {
    beforeEach(() => setupFilters())

    it('should renders all category filters as tags', () => {
      const $panel = mountFilterPanel()
      expect($panel.find(Tag).length).toEqual(FILTERS.length)
    })
  })

  describe('<FilterMenu />', () => {
    beforeEach(() => setupCategories())

    it('should render all the user categories as menu items', () => {
      const $panel = mountFilterPanel()
      expect($panel.find(Item).length).toEqual(CATEGORIES.size)
    })
  })
})

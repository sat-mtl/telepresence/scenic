/* global jest describe beforeEach it expect */

import React from 'react'
import { mount } from 'enzyme'
import Mousetrap from 'mousetrap'
import { act } from 'react-dom/test-utils'

import populateStores from '@stores/populateStores.js'
import { AppStoresContext } from '@components/App'
import StatusEnum from '@models/common/StatusEnum'
import Kind from '@models/quiddity/Kind'

import QuiddityCreationDrawer, {
  QUIDDITY_CREATION_DRAWER_ID,
  QuiddityKindInput,
  QuiddityNicknameInput,
  QuiddityInput
} from '@components/drawers/QuiddityCreationDrawer'

import { Layout, Common } from '@sat-mtl/ui-components'

import '@utils/i18n'

const { Backdrop } = Layout
const { Button } = Common

describe('<QuiddityCreationDrawer />', () => {
  let quiddityStore, drawerStore, helpMessageStore, kindStore

  beforeEach(() => {
    ({ quiddityStore, drawerStore, helpMessageStore, kindStore } = populateStores())
  })

  function mountDrawer () {
    return mount(
      <AppStoresContext.Provider
        value={{ quiddityStore, drawerStore, helpMessageStore, kindStore }}
      >
        <QuiddityCreationDrawer />
      </AppStoresContext.Provider>
    )
  }

  const getButton = ($drawer) => $drawer.update().find(Button)
  const getKindInput = ($drawer) =>
    $drawer.update().find(QuiddityKindInput).find(QuiddityInput)
  const getNicknameInput = ($drawer) =>
    $drawer.update().find(QuiddityNicknameInput).find(QuiddityInput)

  describe('useEffect', () => {
    let addDrawerSpy, removeDrawerSpy
    let toggleActiveDrawerSpy

    beforeEach(() => {
      addDrawerSpy = jest.spyOn(drawerStore, 'addDrawer')
      removeDrawerSpy = jest.spyOn(drawerStore, 'removeDrawer')
      toggleActiveDrawerSpy = jest.spyOn(drawerStore, 'toggleActiveDrawer')
    })

    it('should register the QuiddityCreationDrawer when it is mounted', () => {
      mountDrawer()
      expect(addDrawerSpy).toHaveBeenCalledWith(QUIDDITY_CREATION_DRAWER_ID)
    })

    it('should unregister the FileDeletionDrawer and the TrashCurrentSessionModal when it is unmounted', () => {
      mountDrawer().unmount()
      expect(removeDrawerSpy).toHaveBeenCalledWith(QUIDDITY_CREATION_DRAWER_ID)
    })

    it('should be toggled when the user presses ctrl+a', () => {
      const $drawer = mountDrawer()

      act(() => {
        Mousetrap.trigger('ctrl+a')
      })

      expect(toggleActiveDrawerSpy).toHaveBeenCalledWith(
        QUIDDITY_CREATION_DRAWER_ID
      )
      $drawer.unmount()
    })
  })

  describe('<Backdrop/>', () => {
    let clearActiveDrawerSpy

    beforeEach(() => {
      clearActiveDrawerSpy = jest.spyOn(drawerStore, 'clearActiveDrawer')
    })

    it('should clean the drawers when the backdrop is clicked', () => {
      mountDrawer().find(Backdrop).simulate('click')
      expect(clearActiveDrawerSpy).toHaveBeenCalled()
    })
  })

  describe('<CreateQuiddityButton />', () => {
    it('should flag a disabled when the quiddity kind is empty', () => {
      expect(getButton(mountDrawer()).prop('disabled')).toEqual(true)
    })

    it('should not flag a disabled when the quiddity kind is empty', () => {
      const $drawer = mountDrawer()
      act(() => getKindInput($drawer).prop('onChange')('test'))
      expect(getButton($drawer).prop('disabled')).toEqual(true)
    })

    it('should flag a danger type if the kind is not registered', () => {
      const $drawer = mountDrawer()
      act(() => getKindInput($drawer).prop('onChange')('test'))
      expect(getButton($drawer).prop('type')).toEqual(StatusEnum.DANGER)
    })

    it('should flag a primary type if the kind is registered', () => {
      const $drawer = mountDrawer()

      act(() => {
        kindStore.addKind(
          new Kind('test', 'test', 'test')
        )
        getKindInput($drawer).prop('onChange')('test')
      })

      expect(getButton($drawer).prop('type')).toEqual('primary')
    })
    it('should  disable the button  if the kind is not registered', () => {
      const $drawer = mountDrawer()
      act(() => getKindInput($drawer).prop('onChange')('test'))

      expect(getButton($drawer).prop('disabled')).toEqual(true)
    })
  })

  describe('<QuiddityKindInput />', () => {
    it('should flag a focus status by default', () => {
      expect(getKindInput(mountDrawer()).prop('status')).toEqual(
        StatusEnum.FOCUS
      )
    })

    it('should flag a danger status if the inputted kind is not registered', () => {
      const $drawer = mountDrawer()
      act(() => getKindInput($drawer).prop('onCheck')('test'))
      expect(getKindInput($drawer).prop('status')).toEqual(StatusEnum.DANGER)
    })
  })

  describe('<QuiddityNicknameInput />', () => {
    it('should flag a focus status by default', () => {
      expect(getNicknameInput(mountDrawer()).prop('status')).toEqual(
        StatusEnum.FOCUS
      )
    })

    it('should flag a busy status if the nickname is empty', () => {
      const $drawer = mountDrawer()
      act(() => getNicknameInput($drawer).prop('onCheck')(''))
      expect(getNicknameInput($drawer).prop('status')).toEqual(StatusEnum.BUSY)
    })
  })
})

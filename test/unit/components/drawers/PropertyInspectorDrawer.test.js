/* global jest describe beforeEach it expect */

import React from 'react'
import { act } from 'react-dom/test-utils'
import { configure } from 'mobx'
import { mount } from 'enzyme'
import populateStores from '@stores/populateStores.js'

import {
  DEFAULT_PROPERTY_GROUP_TITLE
} from '@stores/quiddity/PropertyStore'

import Property from '@models/quiddity/Property'
import StatusEnum from '@models/common/StatusEnum'
import QuiddityTagEnum from '@models/quiddity/QuiddityTagEnum'
import Kind from '@models/quiddity/Kind'

import { Feedback, Common, Inputs } from '@sat-mtl/ui-components'

import PropertyInspectorDrawer, {
  PropertyField,
  QuiddityHeader,
  NicknameInput,
  QuiddityStatus,
  QuiddityDescription,
  DeleteQuiddityButton,
  ResetPropertiesButton,
  PropertyChildren,
  PropertyGroup,
  PropertyGroups,
  PROPERTY_INSPECTOR_DRAWER_ID
} from '@components/drawers/PropertyInspectorDrawer'

import NumberField from '@components/fields/NumberField'
import StringField from '@components/fields/StringField'
import SelectionField from '@components/fields/SelectionField'
import BooleanField from '@components/fields/BooleanField'
import ColorField from '@components/fields/ColorField'

import { sdiInput, videoOutput, rtmpOutput, sipMedia } from '@fixture/allQuiddities'
import { getKind } from '@fixture/allQuiddityKinds'

import { AppStoresContext } from '@components/App'

configure({ safeDescriptors: false })

const { Drawer } = Feedback
const { InputText } = Inputs
const { Button, Menu: { Group, Item } } = Common

describe('<PropertyInspectorDrawer />', () => {
  let drawerStore
  let quiddityStore, propertyStore, nicknameStore
  let quiddityStatusStore, kindStore
  let matrixStore
  let stores

  beforeEach(() => {
    stores = populateStores()

    ;({
      drawerStore,
      quiddityStore,
      propertyStore,
      nicknameStore,
      quiddityStatusStore,
      matrixStore,
      kindStore
    } = stores)

    propertyStore.handleUpdatedQuiddities = jest.fn()
  })

  const getProperty = (quiddityId, propertyId) => {
    const properties = propertyStore.properties.get(quiddityId)
    let property

    if (propertyId) {
      property = properties.get(propertyId)
    } else {
      property = Array.from(properties.values())[0]
    }

    return property
  }

  function setupQuiddity (quiddity) {
    const kind = new Kind(quiddity.kindId, quiddity.kindId, 'category', [], 'test-description')
    kindStore.addKind(kind)
    quiddityStore.addQuiddity(quiddity)
    quiddityStore.addSelectedQuiddity(quiddity.id)

    Property.fromQuiddity(quiddity).forEach(
      prop => {
        propertyStore.addProperty(quiddity.id, prop)
      }
    )
  }

  function setupDrawer () {
    drawerStore.addDrawer(PROPERTY_INSPECTOR_DRAWER_ID)
    drawerStore.toggleActiveDrawer(PROPERTY_INSPECTOR_DRAWER_ID)
  }

  function mountDrawer () {
    return mount(
      <AppStoresContext.Provider value={stores}>
        <PropertyInspectorDrawer />
      </AppStoresContext.Provider>
    )
  }

  describe('<PropertyField />', () => {
    let sdiProperty

    beforeEach(() => {
      setupQuiddity(sdiInput)
      setupQuiddity(videoOutput)
      setupQuiddity(rtmpOutput)

      sdiProperty = getProperty(sdiInput.id)

      propertyStore.isEnabled = jest.fn().mockReturnValue(true)
      jest.spyOn(matrixStore, 'lockedQuiddities', 'get').mockReturnValue(new Set([sdiInput.id]))
      quiddityStore.destinationIds.includes = jest.fn().mockReturnValue(true)
      propertyStore.isStartableProperty = jest.fn().mockReturnValue(true)
    })

    function mountPropertyField (quiddityId, propertyId) {
      return mount(
        <AppStoresContext.Provider value={stores}>
          <PropertyField
            quiddityId={quiddityId}
            propertyId={propertyId}
          />
        </AppStoresContext.Provider>
      )
    }

    function getPropertyField () {
      return mountPropertyField(sdiInput.id, sdiProperty.id)
    }

    it('should enable toggling active button when we have a locked destination that is started', () => {
      const $activityButton = getPropertyField().find('.SwitchButton')
      expect($activityButton.prop('disabled')).toEqual(false)
    })

    it('should disable toggling active button when we have a locked destination that is not startable', () => {
      propertyStore.isStartableProperty = jest.fn().mockReturnValue(false)
      const $activityButton = getPropertyField().find('.SwitchButton')
      expect($activityButton.prop('disabled')).toEqual(true)
    })

    it('should disable toggling active button when we have a locked started quiddity that is not a destination', () => {
      jest.spyOn(quiddityStore.destinationIds, 'includes').mockReturnValue(false)
      const $activityButton = getPropertyField().find('.SwitchButton')
      expect($activityButton.prop('disabled')).toEqual(true)
    })

    it('should properly display a property that was initially true but was switched to false', () => {
      // setup the property so that it was "initially true"
      const prop = propertyStore.properties.get(sdiInput.id).get('started')
      prop.value = true
      propertyStore.properties.get(sdiInput.id).set('started', prop)
      // mock clicking on the toggle to first set the property to true
      propertyStore.values.get(sdiInput.id).set('started', true)
      // then back to false
      propertyStore.values.get(sdiInput.id).set('started', false)
      const $activityButton = getPropertyField().find('.SwitchButton')
      expect($activityButton.props().children.props.children).toEqual('OFF')
    })

    it('should render nothing if the property type is not handled', () => {
      sdiProperty.type = 'unhandled'
      expect(getPropertyField().html()).toBeFalsy()
    })

    it('should render a NumberField if the property is a number', () => {
      const $field = mountPropertyField(sdiInput.id, 'Capture/width')
      expect($field.contains(NumberField)).toEqual(true)
    })

    it('should render a SelectionField if the property is a selection', () => {
      const $field = mountPropertyField(sdiInput.id, 'Capture/framerate')
      expect($field.contains(SelectionField)).toEqual(true)
    })

    it('should render a StringField if the property is a string', () => {
      const $field = mountPropertyField(videoOutput.id, 'Window/background_image')
      expect($field.contains(StringField)).toEqual(true)
    })

    it('should render a BooleanField if the property is a boolean', () => {
      const $field = mountPropertyField(videoOutput.id, 'Window/keyb_interaction')
      expect($field.contains(BooleanField)).toEqual(true)
    })

    it('should render a ColorField if the property is a color', () => {
      const $field = mountPropertyField(videoOutput.id, 'Window/color')
      expect($field.contains(ColorField)).toEqual(true)
    })

    it('should render a password input if the destination is a RTMP', () => {
      const $field = mountPropertyField(rtmpOutput.id, 'RTMP/stream_key')
      expect($field.find(StringField).prop('isPassword')).toEqual(true)
    })
  })

  describe('<NicknameInput />', () => {
    function mountNicknameInput (quiddityId, onChange) {
      return mount(
        <AppStoresContext.Provider value={stores}>
          <NicknameInput
            quiddityId={quiddityId}
            onChange={onChange}
          />
        </AppStoresContext.Provider>
      ).find(InputText)
    }

    beforeEach(() => {
      nicknameStore.addNickname(sdiInput.id, 'nickname')
    })

    it('should call onChange when the input is blurred', () => {
      const onChangeSpy = jest.fn()
      const $input = mountNicknameInput(sdiInput.id, onChangeSpy)
      $input.find('input').simulate('blur')
      expect(onChangeSpy).toHaveBeenCalled()
    })

    it('should call onChange when the user press enter', () => {
      const onChangeSpy = jest.fn()
      const $input = mountNicknameInput(sdiInput.id, onChangeSpy)
      $input.find('input').simulate('keypress', { key: 'Enter' })
      expect(onChangeSpy).toHaveBeenCalled()
    })
  })

  describe('<QuiddityHeader />', () => {
    beforeEach(() => {
      nicknameStore.addNickname(sdiInput.id, 'nickname')
    })

    it('should render the Property Inspector title if no quiddity is selected', () => {
      const $drawer = mountDrawer()
      expect($drawer.find(Drawer).prop('header')).toEqual('Property Inspector')
      expect($drawer.find(QuiddityHeader).length).toEqual(0)
    })

    it('should render a QuiddityHeader if a quiddity is selected', () => {
      act(() => {
        setupDrawer()
        setupQuiddity(sdiInput)
      })

      expect(drawerStore.activeDrawer).toEqual(PROPERTY_INSPECTOR_DRAWER_ID)
      expect(quiddityStore.selectedQuiddity).not.toEqual(null)
      expect(mountDrawer().find(QuiddityHeader).length).toEqual(1)
    })
  })

  describe('<DeleteQuiddityButton />', () => {
    beforeEach(() => {
      setupDrawer()
      setupQuiddity(sdiInput)
      quiddityStore.applyQuiddityRemoval = jest.fn()
      drawerStore.cleanActiveDrawer = jest.fn()
    })

    function mountButton () {
      return mountDrawer().find(DeleteQuiddityButton).find(Button)
    }

    it('should delete the selected quiddity when it is clicked', () => {
      matrixStore.populateSourceCategories()
      const $button = mountButton()
      $button.simulate('click')
      expect(quiddityStore.applyQuiddityRemoval).toHaveBeenCalledWith(sdiInput.id)
    })

    it('should close the drawer when it is clicked', () => {
      matrixStore.populateSourceCategories()
      const $button = mountButton()
      $button.simulate('click')
      expect(drawerStore.cleanActiveDrawer).toHaveBeenCalledWith(PROPERTY_INSPECTOR_DRAWER_ID)
    })

    it('should be disabled if the quiddity is locked', () => {
      jest.spyOn(matrixStore, 'lockedQuiddities', 'get').mockReturnValue(new Set([sdiInput.id]))
      const $button = mountButton()
      expect($button.prop('disabled')).toEqual(true)
    })
  })

  describe('<ResetPropertiesButton />', () => {
    beforeEach(() => {
      setupDrawer()
      setupQuiddity(sdiInput)
      propertyStore.applyPropertyResetAll = jest.fn()
    })

    function mountButton () {
      return mountDrawer().find(ResetPropertiesButton).find(Button)
    }

    it('should be disabled when the quiddity is started', () => {
      propertyStore.properties.get(sdiInput.id).set('started', new Property('started', 'boolean', true))
      propertyStore.values.get(sdiInput.id).set('started', true)
      expect(mountButton().prop('disabled')).toEqual(true)
    })

    it('should not be rendered when the selected quiddity is a sip source', () => {
      quiddityStore.selectedQuiddityIds.clear()
      setupQuiddity(sipMedia)
      expect(mountDrawer().find(ResetPropertiesButton)).toHaveLength(0)
    })

    it('shouldn\'t be disabled if the quiddity isn\'t started', () => {
      propertyStore.properties.get(sdiInput.id).set('started', new Property('started', 'boolean', true))
      propertyStore.values.get(sdiInput.id).set('started', false)
      expect(mountButton().prop('disabled')).toEqual(false)
    })

    it('should handle reset button click', () => {
      const $button = mountButton()
      $button.simulate('click')
      expect(propertyStore.applyPropertyResetAll).toHaveBeenCalledWith(sdiInput.id)
    })
  })

  describe('<QuiddityStatus />', () => {
    beforeEach(() => {
      setupDrawer()
      setupQuiddity(sdiInput)
    })

    function mountStatus () {
      return mountDrawer().find(QuiddityStatus)
    }

    it('should render the inactive status by default', () => {
      quiddityStatusStore.clear()
      expect(mountStatus().text()).toEqual(StatusEnum.INACTIVE)
    })

    it('should render the status of the selected quiddity', () => {
      quiddityStatusStore.setQuiddityStatus(QuiddityTagEnum.SOURCE, sdiInput.id, StatusEnum.ACTIVE)
      expect(mountStatus().text()).toEqual(StatusEnum.ACTIVE)
    })
  })

  describe('<QuiddityDescription />', () => {
    beforeEach(() => {
      setupDrawer()
      setupQuiddity(sdiInput)
    })

    function mountDescription () {
      return mountDrawer().find(QuiddityDescription)
    }

    it('should render the default quiddity description if there are none', () => {
      kindStore.kinds.delete(getKind(sdiInput).id)
      expect(mountDescription().text()).toEqual('No description')
    })

    it('should render the quiddity description', () => {
      expect(mountDescription().text()).toEqual('test-description')
    })
  })

  describe('<PropertyChildren />', () => {
    beforeEach(() => {
      setupQuiddity(videoOutput)
    })

    function mountPropertyChildren (quiddityId, groupId) {
      return mount(
        <AppStoresContext.Provider value={stores}>
          <PropertyChildren
            quiddityId={quiddityId}
            groupId={groupId}
          />
        </AppStoresContext.Provider>
      )
    }

    it('should render an empty array if the property group has no children', () => {
      const $children = mountPropertyChildren(videoOutput.id, 'test')
      expect($children.find(Item).length).toEqual(0)
    })

    it('should render all children of a group', () => {
      const children = propertyStore.groupedProperties.get(videoOutput.id).get('Window/overlay_config')
      const $children = mountPropertyChildren(videoOutput.id, 'Window/overlay_config')
      expect($children.find(Item).length).toEqual(children.size)
    })

    it('should not render a hidden item', () => {
      propertyStore.isHidden = jest.fn().mockReturnValue(true)
      expect(mountPropertyChildren(sdiInput.id, 'common').find(Item).length).toEqual(0)
    })
  })

  describe('<PropertyGroup />', () => {
    beforeEach(() => {
      setupQuiddity(videoOutput)
    })

    function mountPropertyGroup (quiddityId, groupId) {
      return mount(
        <AppStoresContext.Provider value={stores}>
          <PropertyGroup
            quiddityId={quiddityId}
            groupId={groupId}
          />
        </AppStoresContext.Provider>
      )
    }

    it('should render a Menu.Group with its property children', () => {
      const $group = mountPropertyGroup(videoOutput.id, 'Window/overlay_config')
      expect($group.find(Group).length).toEqual(1)
      expect($group.find(PropertyChildren).length).toEqual(1)
    })

    it('should render the property group title', () => {
      const $group = mountPropertyGroup(videoOutput.id, 'Window/overlay_config')
      const property = propertyStore.properties.get(videoOutput.id).get('Window/overlay_config')
      expect($group.find(Group).prop('title')).toEqual(property.title)
    })

    it('should render the default group title if the group id is the default group id', () => {
      const $group = mountPropertyGroup(videoOutput.id, DEFAULT_PROPERTY_GROUP_TITLE)
      expect($group.find(Group).prop('title')).toEqual(DEFAULT_PROPERTY_GROUP_TITLE)
    })
  })

  describe('<PropertyGroups />', () => {
    beforeEach(() => {
      setupQuiddity(videoOutput)
    })

    function mountPropertyGroups (quiddityId, groupId) {
      return mount(
        <AppStoresContext.Provider value={stores}>
          <PropertyGroups quiddityId={quiddityId} />
        </AppStoresContext.Provider>
      )
    }

    it('should render all property groups', () => {
      propertyStore.isHidden = jest.fn().mockReturnValue(false)
      const $groups = mountPropertyGroups(videoOutput.id)
      const groups = propertyStore.groupedProperties.get(videoOutput.id)
      expect($groups.find(PropertyGroup).length).toEqual(groups.size)
    })
  })
})

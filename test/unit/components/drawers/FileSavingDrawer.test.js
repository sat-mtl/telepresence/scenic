/* global jest describe beforeEach it expect */

import React from 'react'
import { act } from 'react-dom/test-utils'
import { mount } from 'enzyme'
import populateStores from '@stores/populateStores.js'

import { AppStoresContext } from '@components/App'

import FileBridge from '@models/common/FileBridge'

import FileSavingDrawer, { FileSavingList, FILE_SAVING_DRAWER_ID } from '@components/drawers/FileSavingDrawer'
import NewFileModal, { NEW_FILE_MODAL_ID } from '@components/modals/NewFileModal'
import FileEntry from '@components/entries/FileEntry'

import { Layout } from '@sat-mtl/ui-components'

import '@utils/i18n'

const { Backdrop } = Layout

const FILE_ID = 'test.json'
const FILE_TEST = new FileBridge('test.json')

describe('<FileSavingDrawer />', () => {
  let drawerStore, sessionStore, modalStore, helpMessageStore

  beforeEach(() => {
    ({
      drawerStore,
      sessionStore,
      modalStore,
      helpMessageStore
    } = populateStores())

    sessionStore.updateSessionList = jest.fn()
  })

  function mountDrawer () {
    return mount(
      <AppStoresContext.Provider value={{ drawerStore, helpMessageStore, modalStore, sessionStore }}>
        <FileSavingDrawer />
      </AppStoresContext.Provider>
    )
  }

  describe('useEffect', () => {
    let addDrawerSpy, removeDrawerSpy
    let addModalSpy, removeModalSpy
    let updateSessionListSpy

    beforeEach(() => {
      addDrawerSpy = jest.spyOn(drawerStore, 'addDrawer')
      removeDrawerSpy = jest.spyOn(drawerStore, 'removeDrawer')
      addModalSpy = jest.spyOn(modalStore, 'addModal')
      removeModalSpy = jest.spyOn(modalStore, 'removeModal')
      updateSessionListSpy = jest.spyOn(sessionStore, 'updateSessionList')
    })

    it('should register the FileSavingDrawer and the NewFileModal when it is mounted', () => {
      mountDrawer()
      expect(addDrawerSpy).toHaveBeenCalledWith(FILE_SAVING_DRAWER_ID)
      expect(addModalSpy).toHaveBeenCalledWith(NEW_FILE_MODAL_ID)
    })

    it('should unregister the FileSavingDrawer and the NewFileModal when it is unmounted', () => {
      mountDrawer().unmount()
      expect(removeDrawerSpy).toHaveBeenCalledWith(FILE_SAVING_DRAWER_ID)
      expect(removeModalSpy).toHaveBeenCalledWith(NEW_FILE_MODAL_ID)
    })

    it('should update the file list when it is updated only when the drawer is active', () => {
      const $drawer = mountDrawer()
      expect(updateSessionListSpy).not.toHaveBeenCalled()
      act(() => drawerStore.setActiveDrawer(FILE_SAVING_DRAWER_ID))
      $drawer.update()
      expect(updateSessionListSpy).toHaveBeenCalled()
    })
  })

  describe('<Backdrop/>', () => {
    let clearActiveDrawerSpy

    beforeEach(() => {
      clearActiveDrawerSpy = jest.spyOn(drawerStore, 'clearActiveDrawer')
    })

    it('should clean the drawers when the backdrop is clicked', () => {
      mountDrawer().find(Backdrop).simulate('click')
      expect(clearActiveDrawerSpy).toHaveBeenCalled()
    })
  })

  describe('<FileSavingList/>', () => {
    beforeEach(() => {
      sessionStore.addSession(new FileBridge(FILE_ID, '/test.json'))
      expect(sessionStore.sessions.size).toEqual(1)
    })

    it('should be instanciated without selected file', () => {
      const $list = mountDrawer().find(FileSavingList)
      expect($list.prop('selectedFileId')).toEqual(null)
    })

    it('should select a file when its entry is clicked', () => {
      const $drawer = mountDrawer()
      $drawer.find(FileEntry).simulate('click')
      const $list = $drawer.find(FileSavingList)
      expect($list.prop('selectedFileId')).toEqual(FILE_ID)
    })
  })

  const getNewFileModal = $drawer => $drawer.update().find(NewFileModal)

  describe('modals', () => {
    let applySessionSavingAsSpy, updateSessionListSpy
    let clearActiveModalSpy

    beforeEach(() => {
      applySessionSavingAsSpy = jest.spyOn(sessionStore, 'applySessionSavingAs')
      updateSessionListSpy = jest.spyOn(sessionStore, 'updateSessionList')
      clearActiveModalSpy = jest.spyOn(modalStore, 'clearActiveModal')
    })

    describe('<NewFileModal />', () => {
      it('should clear the modal when it is canceled', () => {
        getNewFileModal(mountDrawer()).prop('onCancel')()
        expect(applySessionSavingAsSpy).not.toHaveBeenCalled()
        expect(clearActiveModalSpy).toHaveBeenCalled()
      })

      it('should save the current file as the selected file if it is confirmed', () => {
        getNewFileModal(mountDrawer()).prop('onConfirm')(FILE_ID)
        expect(clearActiveModalSpy).toHaveBeenCalled()
        expect(applySessionSavingAsSpy).toHaveBeenCalledWith(FILE_ID)
      })

      it('should open save the new file', () => {
        const $drawer = mountDrawer()

        act(() => {
          sessionStore.addSession(FILE_TEST)
          sessionStore.setCurrentSession(FILE_ID)
        })

        act(() => getNewFileModal($drawer).prop('onConfirm')(FILE_ID))

        expect(clearActiveModalSpy).toHaveBeenCalled()
        expect(updateSessionListSpy).toHaveBeenCalled()
        expect(applySessionSavingAsSpy).toHaveBeenCalledWith(FILE_ID)
      })
    })
  })
})

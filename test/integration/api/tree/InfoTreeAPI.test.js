/* global describe it expect beforeAll afterAll beforeEach */

import { createSocket, delay } from '@test/testTools'

import defaultBundles from '@fixture/api/config/bundles.default.json'

import QuiddityAPI from '@api/quiddity/QuiddityAPI'
import PropertyAPI from '@api/quiddity/PropertyAPI'
import InfoTreeAPI from '@api/tree/InfoTreeAPI'
import SessionAPI from '@api/switcher/SessionAPI'
import SwitcherAPI from '@api/switcher/SwitcherAPI'
import OSCsinkTree from '@fixture/api/tree/info.OSCsink.json'

const OSCsinkKind = 'OSCsink'
const videotestsrcKind = 'videotestsrc'

describe('InfoTreeAPI', () => {
  const socket = createSocket('ws://localhost', 8000)

  let infoTreeAPI, sessionAPI, quiddityAPI, propertyAPI, switcherAPI
  let OSCsink, videotestsrc

  beforeAll((done) => {
    socket.on('connect', async () => {
      switcherAPI = new SwitcherAPI(socket)
      sessionAPI = new SessionAPI(socket)
      quiddityAPI = new QuiddityAPI(socket)
      infoTreeAPI = new InfoTreeAPI(socket)
      propertyAPI = new PropertyAPI(socket)

      await switcherAPI.sendBundles(defaultBundles)
      await delay(2000)
      done()
    })

    socket.open()
  })

  beforeEach(async () => {
    await sessionAPI.reset()
    OSCsink = await quiddityAPI.create(OSCsinkKind, 'OSCtest')
    videotestsrc = await quiddityAPI.create(videotestsrcKind, 'videotest')
  })

  afterAll(() => socket.close())

  describe('get', () => {
    it('should get the info tree of a created quiddity', async () => {
      await expect(infoTreeAPI.get(OSCsink.id))
        .resolves.toEqual(OSCsinkTree)
    })
  })

  describe('onGrafted', () => {
    it('should be triggered when a quiddity\'s info tree is grafted', (done) => {
      infoTreeAPI.onGrafted(
        () => done(),
        (quidId) => quidId === videotestsrc.id
      )

      propertyAPI.set(videotestsrc.id, 'started', true)
    })
  })
})

/* global describe it expect beforeAll afterAll beforeEach */

import { createSocket, delay } from '@test/testTools'

import QuiddityAPI from '@api/quiddity/QuiddityAPI'
import UserTreeAPI from '@api/tree/UserTreeAPI'
import SessionAPI from '@api/switcher/SessionAPI'

describe('UserTreeAPI', () => {
  const socket = createSocket('ws://localhost', 8000)

  let userTreeAPI, sessionAPI, quiddityAPI
  let emptyquid

  beforeAll((done) => {
    socket.on('connect', () => {
      sessionAPI = new SessionAPI(socket)
      quiddityAPI = new QuiddityAPI(socket)
      userTreeAPI = new UserTreeAPI(socket)
      done()
    })

    socket.open()
  })

  beforeEach(async () => {
    await sessionAPI.reset()
    emptyquid = await quiddityAPI.create('emptyquid', 'emptyquid')
  })

  afterAll(() => socket.close())

  describe('get', () => {
    it('should get the empty user tree of a created quiddity', async () => {
      await expect(userTreeAPI.get(emptyquid.id)).resolves.toEqual({})
    })

    it('should get the user tree of a quiddity', async () => {
      await userTreeAPI.graft(emptyquid.id, '.test.test', 'test')
      await expect(userTreeAPI.get(emptyquid.id))
        .resolves.toEqual({ test: { test: 'test' } })
    })

    it('should query the user tree of a quiddity', async () => {
      await userTreeAPI.graft(emptyquid.id, '.test.test', 'test')
      await expect(userTreeAPI.get(emptyquid.id, 'test.test')).resolves.toEqual('test')
    })

    it('should not query the tree if the path doesn\'t exist', async () => {
      await expect(userTreeAPI.get(emptyquid.id, 'test.fail')).resolves.toEqual({})
    })
  })

  describe('graft', () => {
    it('should set a new value to the user tree', async () => {
      await expect(userTreeAPI.graft(emptyquid.id, 'connections', 'test'))
        .resolves.toEqual({ connections: 'test' })
    })
  })

  describe('prune', () => {
    it('should prune data in the user tree if the branch exists', async () => {
      await userTreeAPI.graft(emptyquid.id, 'test', 'test')
      await expect(userTreeAPI.prune(emptyquid.id, 'test'))
        .resolves.toEqual(true)
    })

    it('should not prune data in the user tree if the branch doesn\'t exist', async () => {
      await expect(userTreeAPI.prune(emptyquid.id, 'sfgfg'))
        .resolves.toEqual(false)
    })
  })

  describe('onGrafted', () => {
    beforeEach(async () => {
      // need to wait in order to avoid conflicts with the userTree.grafted event when a quiddity is created
      await delay(1000)
    })

    it('should be triggered when a quiddity\'s user tree is grafted with a simple value', (done) => {
      userTreeAPI.onGrafted(
        () => done(),
        (quidId) => quidId === emptyquid.id
      )

      userTreeAPI.graft(emptyquid.id, 'connections', 'test')
    })

    it('should be triggered when a quiddity\'s user tree is grafted with a complex value', (done) => {
      userTreeAPI.onGrafted(
        (quidId, path, value) => {
          expect(quidId).toEqual(quidId)
          expect(path).toEqual('connections')
          expect(value).toEqual({ id: 'scene1', name: 'Scene 1', active: false, connections: [] })
          done()
        }
      )

      userTreeAPI.graft(emptyquid.id, 'connections', { id: 'scene1', name: 'Scene 1', active: false, connections: [] })
    })
  })
})

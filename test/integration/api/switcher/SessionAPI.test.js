/* global jest describe it expect beforeAll afterAll */

import { createSocket } from '@test/testTools'

import SessionAPI from '@api/switcher/SessionAPI'
import QuiddityAPI from '@api/quiddity/QuiddityAPI'

import testContent from '@fixture/api/session/test.json'

jest.setTimeout(100000)

describe('SessionAPI', () => {
  const socket = createSocket('ws://localhost', 8000)

  let sessionAPI, quiddityAPI

  beforeAll((done) => {
    socket.on('connect', () => {
      quiddityAPI = new QuiddityAPI(socket)
      sessionAPI = new SessionAPI(socket)
      done()
    })

    socket.open()
  })

  afterAll(async () => {
    for (const session of (await sessionAPI.list())) {
      await sessionAPI.remove(session)
    }

    socket.close()
  })

  describe('reset', () => {
    it('should reset the current state of switcher', async () => {
      await expect(sessionAPI.reset()).resolves.toBe(true)
    })
  })

  describe('list', () => {
    it('should return an empty list if no session has been saved', async () => {
      await expect(sessionAPI.list()).resolves.toEqual([])
    })

    it('should return the list of saved sessions', async () => {
      await sessionAPI.saveAs('test1')
      await sessionAPI.saveAs('test2')
      await expect(sessionAPI.list()).resolves.toEqual(
        expect.arrayContaining(['test1.json', 'test2.json'])
      )
    })
  })

  describe('save_as', () => {
    it('should return true when the current session is saved', async () => {
      await expect(sessionAPI.saveAs('test')).resolves.toMatch('test')
    })
  })

  describe('remove', () => {
    it('should return false when the session doesn\'t exist', async () => {
      await expect(sessionAPI.remove('fail')).resolves.toEqual(false)
    })

    it('should return true when the session is removed', async () => {
      await sessionAPI.saveAs('test')
      await expect(sessionAPI.remove('test')).resolves.toEqual(true)
    })
  })

  describe('load', () => {
    it('should save and load a session by retrieving created quiddities', async () => {
      await expect(quiddityAPI.listCreated()).resolves.toHaveLength(0)
      await quiddityAPI.create('OSCsink', 'test')
      await expect(quiddityAPI.listCreated()).resolves.toHaveLength(1)
      await sessionAPI.saveAs('test3')
      await sessionAPI.reset()
      await expect(quiddityAPI.listCreated()).resolves.toHaveLength(0)
      await sessionAPI.load('test3')
      await expect(quiddityAPI.listCreated()).resolves.toHaveLength(1)
    })
  })

  describe('write', () => {
    it('should return true when content is written to a session file', async () => {
      console.log(sessionAPI)
      await expect(sessionAPI.write(JSON.stringify(testContent), 'test4')).resolves.toEqual(true)
    })
  })

  describe('read', () => {
    it('should read a session and compare its content with expected fixture', async () => {
      const content = await sessionAPI.read('test4')
      expect(content).not.toEqual(undefined)
      expect(JSON.parse(content)).toEqual(testContent)
    })
  })
})

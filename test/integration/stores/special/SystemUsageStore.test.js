/* global jest describe it expect beforeAll beforeEach afterAll */

import { createSocket, delay } from '@test/testTools'

import populateStores from '~/src/stores/populateStores'
import { initializeStores } from '~/src/stores/initializeStores'

import defaultScenic from '@assets/json/scenic.default.json'

import {
  SYSTEM_USAGE_NAME,
  SYSTEM_USAGE_KIND
} from '@models/quiddity/specialQuiddities'

const systemUsageConfig = defaultScenic.initQuiddities
  .find(q => q.nickname === SYSTEM_USAGE_NAME)

jest.setTimeout(10000)

describe('SystemUsageStore', () => {
  let stores
  let socketStore, configStore, kindStore, quiddityStore, systemUsageStore
  const socket = createSocket('ws://localhost', 8000)

  let initSpy, graftSpy, fallbackSpy

  beforeAll((done) => {
    stores = populateStores()

    ;({
      socketStore,
      configStore,
      kindStore,
      quiddityStore,
      systemUsageStore
    } = stores)

    socket.on('connect', () => {
      socketStore.setActiveSocket(socket)
      done()
    })

    socket.open()

    initSpy = jest.spyOn(systemUsageStore, 'initialize')
    graftSpy = jest.spyOn(systemUsageStore, 'handleGraftedTop')
    fallbackSpy = jest.spyOn(systemUsageStore, 'fallbackSystemUsageQuiddity')

    configStore.initialize = jest.fn().mockResolvedValue(true)
    configStore.defaultScenic.initQuiddities = [systemUsageConfig]
  })

  beforeEach(async () => {
    await socketStore.APIs.sessionAPI.clear()
  })

  afterAll(() => {
    socket.close()
  })

  describe('initialization', () => {
    beforeEach(async () => {
      await initializeStores(stores)
    })

    it('should be grafted when the initialization is finished', async () => {
      expect(configStore.initQuiddities.length).toEqual(1)
      expect(initSpy).toHaveBeenCalled()
      expect(fallbackSpy).toHaveBeenCalled()
      await delay(1000)
      expect(quiddityStore.usedKindIds.has(SYSTEM_USAGE_KIND)).toEqual(true)
      await delay(1000)
      expect(graftSpy).toHaveBeenCalled()
    })

    it('should create a hidden and protected quiddity', async () => {
      expect(quiddityStore.quiddityByNames.has(SYSTEM_USAGE_NAME)).toEqual(true)
      const quid = quiddityStore.quiddityByNames.get(SYSTEM_USAGE_NAME)
      expect(quid.isProtected).toEqual(true)
      expect(quid.isHidden).toEqual(true)
      expect(quiddityStore.userQuiddities.length).toEqual(0)
    })
  })

  describe('fallback', () => {
    beforeEach(async () => {
      await configStore.initialize()
      await kindStore.initialize(true)
    })

    it('should not be fallbacked when it is already created', async () => {
      await quiddityStore.applyQuiddityCreation(SYSTEM_USAGE_KIND, SYSTEM_USAGE_NAME)
      await delay(1000)
      await initializeStores(stores)
      await delay(1000)
      expect(fallbackSpy).not.toHaveBeenCalled()
    })
  })
})

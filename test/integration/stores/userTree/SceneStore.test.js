/* global describe it jest expect beforeAll beforeEach afterAll */

import { createSocket } from '@test/testTools'
import { when } from 'mobx'

import populateStores from '~/src/stores/populateStores'
import { initializeStores } from '~/src/stores/initializeStores'

import { USER_TREE_NICKNAME } from '@models/quiddity/specialQuiddities'

describe('SceneStore', () => {
  let socketStore, sceneStore, quiddityStore
  let stores
  const socket = createSocket('ws://localhost', 8000)

  let initSpy

  beforeAll((done) => {
    stores = populateStores()

    ;({
      socketStore,
      quiddityStore,
      sceneStore
    } = stores)

    socket.on('connect', () => {
      socketStore.setActiveSocket(socket)
      done()
    })

    socket.open()

    initSpy = jest.spyOn(sceneStore, 'initialize')
  })

  afterAll(() => {
    socket.close()
  })

  describe('initialization', () => {
    beforeEach(async () => {
      await socketStore.APIs.sessionAPI.clear()
      await initializeStores(stores)
    })

    it('should be initialized after the userTreeStore is created', async () => {
      when(
        () => quiddityStore.quiddityByNames.has(USER_TREE_NICKNAME),
        () => expect(initSpy).toHaveBeenCalled()
      )
    })
  })
})

/* global describe it expect beforeAll beforeEach afterAll */

import { createSocket, delay } from '@test/testTools'
import populateStores from '~/src/stores/populateStores'

describe('CapsStore', () => {
  let socketStore, configStore
  let quiddityStore, propertyStore, kindStore
  let shmdataStore, capsStore

  let stores

  const socket = createSocket('ws://localhost', 8000)

  beforeAll((done) => {
    stores = populateStores()

    ;({
      socketStore,
      kindStore,
      quiddityStore,
      propertyStore,
      capsStore,
      configStore,
      shmdataStore
    } = stores)

    socket.on('connect', async () => {
      socketStore.setActiveSocket(socket)
      await configStore.initialize()
      await delay(2000)
      await kindStore.initialize()
      done()
    })

    socket.open()
  })

  beforeEach(async () => {
    await socketStore.APIs.sessionAPI.clear()
  })

  afterAll(() => {
    socket.close()
  })

  describe('writerCaps', () => {
    it('should detect writer caps when a source is started', async () => {
      const quid = await quiddityStore.applyQuiddityCreation('videotestsrc', 'src')
      await delay(2000)
      await propertyStore.applyNewPropertyValue(quid.id, 'started', true)
      await delay(2000)
      expect(shmdataStore.writingShmdatas.get(quid.id).size).toEqual(1)
      expect(capsStore.writerCaps.get(quid.id).size).toEqual(1)
    })
  })

  describe('areConnectable', () => {
    it('should return true when quiddities are compatible', async () => {
      const oscSrc = await quiddityStore.applyQuiddityCreation('oscInput', 'oscSrc')
      await propertyStore.applyNewPropertyValue(oscSrc.id, 'started', true)
      const oscDst = await quiddityStore.applyQuiddityCreation('oscOutput', 'oscDst')

      await delay(2000)
      await expect(capsStore.areConnectable(oscSrc.id, oscDst.id, 'application/x-libloserialized-osc')).toEqual(true)
    })

    it('should return false when quiddities aren\'t compatible', async () => {
      const vidSrc = await quiddityStore.applyQuiddityCreation('videotestsrc', 'vidSrc')
      await propertyStore.applyNewPropertyValue(vidSrc.id, 'started', true)
      const oscDst = await quiddityStore.applyQuiddityCreation('oscOutput', 'oscDst')

      await delay(1000)
      await expect(capsStore.areConnectable(vidSrc.id, oscDst.id)).toEqual(false)
    })
  })

  describe('findCompatibleCaps', () => {
    it('should find a compatible media type between osc quiddities', async () => {
      const oscSrc = await quiddityStore.applyQuiddityCreation('oscInput', 'oscSrc')
      await propertyStore.applyNewPropertyValue(oscSrc.id, 'started', true)
      const oscDst = await quiddityStore.applyQuiddityCreation('oscOutput', 'oscDst')
      await delay(2000)
      await expect(capsStore.findCompatibleCaps(oscSrc.id, oscDst.id)).not.toBeNull()
    })
  })
})

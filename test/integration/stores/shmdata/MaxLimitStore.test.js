/* global describe it expect beforeAll beforeEach afterAll */

import { createSocket, delay } from '@test/testTools'
import populateStores from '~/src/stores/populateStores'

describe('MaxLimitStore', () => {
  let socketStore, quiddityStore, configStore, kindStore, maxLimitStore
  let stores
  const socket = createSocket('ws://localhost', 8000)

  beforeAll((done) => {
    stores = populateStores()

    ;({
      socketStore,
      kindStore,
      quiddityStore,
      configStore,
      maxLimitStore
    } = stores)

    socket.on('connect', async () => {
      socketStore.setActiveSocket(socket)
      await configStore.initialize()
      await delay(2000)
      await kindStore.initialize()
      done()
    })

    socket.open()
  })

  beforeEach(async () => {
    await socketStore.APIs.sessionAPI.clear()
  })

  afterAll(() => {
    socket.close()
  })

  it('should notify that the oscOutput requires 1 connection', async () => {
    const quid = await quiddityStore.applyQuiddityCreation('oscOutput', 'src')
    await delay(2000)
    expect(maxLimitStore.maxReaderLimits.get(quid.id)).toEqual(1)
  })

  it.todo('should notify that the rtmp requires 2 connections')
})

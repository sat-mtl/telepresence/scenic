# Integration tests

Collection of integration tests used to test the *event driven* architecture of Scenic. It mocks all [SocketIO](https://socket.io/) calls and fakes the Switcher behaviour in order to develop the Scenic controllers logic.

## How to make a new test

- [ ] Analyse how your stores should interact with the `Switcher` server as a sequence diagram.
- [ ] Mock the missing routes in the [mocked APIs](../fixture/api).
- [ ] Use the mocked [SocketIO server](https://socket.io/docs/server-api/) in order to mock all network calls. The server is specified **only** as a developement dependency.
- [ ] Use the [mobx function `when`](https://mobx.js.org/refguide/when.html) in order to check the store state and to ensure that the expected values are in place.
- [ ] Follow one of the sequence diagram written below.
- [ ] Use the [jest callback `done`](https://jestjs.io/docs/en/asynchronous#callbacks) as a parameter in the test function in order to **end** the test after a sequence of calls.
- [ ] Don't forget to disconnect the client socket after each test.
- [ ] Don't forget to close the server after all tests.

## Integration scenarios

### Scenario 1: Create two quiddities

1. Client creates quiddities `sdiInput1` and `videoMonitor`

2. Server confirms each quiddity creation

    2.a Client starts quiddities (if not started)

3. Server emits shmdata creation for each quiddity

4. Client connects quiddity `videoMonitor` to `sdiInput`'s shmdata

    4.a Server emits shmdata writer in `videoMonitor` tree

    4.b Server emits shmdata reader in `sdiInput`'s tree

```mermaid
sequenceDiagram
  autonumber
  Scenic-->>+Broker: quiddity.create(class, nickname)
  Broker->>-Switcher: create(class, nickname)
  Switcher->>+Broker: signal('on-quiddity-created')
  Broker-->>-Scenic: quiddity.created(quiddity)

  opt quiddity.properties.started === false
	  Scenic-->>+Broker: quiddity.set(quiddityId, 'started', true)
	  Broker->>-Switcher: set_property_value(quiddityId, 'started', true)
	  Switcher->>+Broker: signal('on-property-grafted?')
	  Broker-->>-Scenic: quiddity.property.changed(quiddityId, 'started', true)
  end

  opt quiddity is a source
	  Switcher->>+Broker: signal('on-tree-grafted')
	  Broker-->>-Scenic: info_tree.grafted(quiddityId, 'shmdata.writer.{path}', shmdata)
  end
```

### Scenario 2: Connect two quiddities

1. Quiddities `sdiInput1` and `videoMonitor` are created

2. The client connects the quiddity `sdiInput1` and `videoMonitor`

    2.1 The client disconnect all shmdata of `videoMonitor` (if it has the property `maxReader` set to 1)

    2.2 The method `connect` is invoked with the quiddity `videoMonitor` and a shmdata writer of `sdiInput1`

```mermaid
sequenceDiagram
  autonumber
  loop each quiddity destination and shmdata writer
    opt quiddity is a source
      Scenic->>+Broker: squiddity.method.invoke(quiddityId, 'disconnect-all', [])
      Broker-->>-Switcher: invoke(quiddityId, 'disconnect-all')
    end

	  Scenic-->>+Broker: quiddity.invoke(quiddityId, 'connect', [shmdata-path])
	  Broker->>-Switcher: invoke(quiddityId, 'connect', [shmdata-path])
	  Switcher->>+Broker: signal('on-tree-grafted')
	  Broker-->>-Scenic: info_tree.grafted(quiddityId, 'shmdata.writer.{path}', shmdata)
  end

```

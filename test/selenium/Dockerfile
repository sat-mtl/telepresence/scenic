FROM ubuntu:20.04
LABEL MAINTAINER="Scenic team <scenic-dev@sat.qc.ca>"

ENV DEBIAN_FRONTEND noninteractive

RUN \
    # Update package list
    apt-get update && \
    # Install Xvfb and ffmpeg
    apt-get install --no-install-recommends -y xvfb ffmpeg && \
    # Install Python and pipenv needed to run scenic-selenium
    apt-get install --no-install-recommends -y python3.9 pipenv && \
    # Install curl and unzip to download Chromium
    apt-get install --no-install-recommends -y curl unzip && \
    # Install shared libraries needed for chromedriver
    apt-get install --no-install-recommends -y libglib2.0-0 libnss3 && \
    # Install shared libraires needed for Chromium
    apt-get install --no-install-recommends -y libatk1.0-0 libatk-bridge2.0-0 libcups2 libxcomposite1 libxdamage1 libxrandr2 libgbm1 libxkbcommon0 libpango1.0-0 libcairo2 libasound2 libxfixes-dev && \
    # Download raw builds of Chromium and chromedriver
    #   Currently using Chromium and chromedriver version 107.0.5304.110.
    #   Follow instructions to find branch_base_commit associated with chromium version,
    #     https://www.chromium.org/getting-involved/download-chromium/#downloading-old-builds-of-chrome-chromium
    #       current_version > branch_base_commit
    #       107.0.5304.110 > 1047731
    mkdir -p /opt && \
    curl -o /opt/chrome-linux.zip "https://www.googleapis.com/download/storage/v1/b/chromium-browser-snapshots/o/Linux_x64%2F1047731%2Fchrome-linux.zip?generation=1663284576100523&alt=media" && \
    unzip /opt/chrome-linux.zip -d /opt/ && \
    rm /opt/chrome-linux.zip && \
    curl -o /opt/chromedriver_linux64.zip "https://www.googleapis.com/download/storage/v1/b/chromium-browser-snapshots/o/Linux_x64%2F1047731%2Fchromedriver_linux64.zip?generation=1663284581314213&alt=media" && \
    unzip /opt/chromedriver_linux64.zip -d /opt/ && \
    rm /opt/chromedriver_linux64.zip && \
    ln -s /opt/chrome-linux/chrome /usr/bin/chrome && \
    ln -s /opt/chromedriver_linux64/chromedriver /usr/bin/chromedriver && \
    # Clean apt cache
    apt-get clean && \
    apt-get autoclean && \
    apt-get autoremove -y && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/
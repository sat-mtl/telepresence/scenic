from selenium.common.exceptions import NoSuchElementException

def test_source_menu_elements(scenic_instance_1):
    # This tests for the selectors for the Source Menu elements
    try:
        # Assert presence of Sources menu
        scenic_instance_1.assert_element_presence('.Menu .SelectButton[data-menu="Sources"]')

        # Assert presence of Video menu and submenu items
        scenic_instance_1.assert_element_presence('.Menu .GroupTitle[data-menu="Video"]') # TODO This selector is the same as the selector for the Destinations menu item
        scenic_instance_1.assert_element_presence('.Item[data-menu="Video Input"]')
        scenic_instance_1.assert_element_presence('.Item[data-menu="Test Pattern"]')

        # Assert presence of Audio menu and submenu items
        scenic_instance_1.assert_element_presence('.Menu .GroupTitle[data-menu="Audio"]') # TODO Again, this is the same selector as the Destination menu Audio submenu sooooooo
        scenic_instance_1.assert_element_presence('.Item[data-menu="JACK Input"]')
        scenic_instance_1.assert_element_presence('.Item[data-menu="Pulse Input"]')
        scenic_instance_1.assert_element_presence('.Item[data-menu="Test Signal"]')

        # Assert presence of Control menu and submenu items
        scenic_instance_1.assert_element_presence('.Menu .GroupTitle[data-menu="Control"]') # TODO see above
        scenic_instance_1.assert_element_presence('.Item[data-menu="MIDI Input"]')
        scenic_instance_1.assert_element_presence('.Item[data-menu="OSC Input"]')

        # Assert presence of Network menu and submenu items
        scenic_instance_1.assert_element_presence('.Menu .GroupTitle[data-menu="Network"]') # TODO Ibid.
        scenic_instance_1.assert_element_presence('.Item[data-menu="NDI® Input"]')

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'

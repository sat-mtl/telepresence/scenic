from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


def test_settings_page(scenic_instance_1) -> None:
    try:
        # Find and click the Settings Page Tab
        # TODO Left Sidebar menu items do not contain custom selectors; when they do replace XPath Selectors with CSS Selectors
        scenic_instance_1.driver.find_element(By.XPATH, '//div[contains(text(), "settings")]').click()

        # Search for the Settings Page ID and assert that it is present
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '#SettingsPage'), 'The Settings Page ID did not appear'

        # Search for the expected page header and assert it's presence
        # TODO Uses Xpath for now - the page header does not have a specific class/ID
        assert scenic_instance_1.driver.find_element(
            By.XPATH, '//h1[contains(text(), "General settings")]'), 'The Settings Page Header did not appear'

        # Search for the General settings tab
        assert scenic_instance_1.driver.find_element(
            By.XPATH, '//div[contains(text(), "General")]'), 'The General settings tab is not present'

        # Search for the expected Language Switcher on the General settings tab
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '#GeneralSettingsSection .SelectField'), 'There is no language switcher on the page'

        # Search for the SIP settings Tab
        sip_settings_tab = scenic_instance_1.driver.find_element(By.XPATH, '//div[contains(text(), "SIP")]')
        assert sip_settings_tab, 'The SIP settings tab is not present'

        # Click the SIP settings Tab
        sip_settings_tab.click()

        # Assert the presence of the SIP server field
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '.InputTextField[name="sipServer"]'), 'The SIP server text field is not present'

        # Assert the presence of the SIP port filed
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '.InputTextField[name="sipPort"]'), 'The SIP port text field is not present'

        # Assert the presence of the SIP user name field
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '.InputTextField[name="sipUser"]'), 'The SIP User Name text field is not present'

        # Assert the presence of the SIP Password field
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '.InputTextField[name="sipPassword"]'), 'The SIP server text field is not present'

        # Assert the presence of the Reset button
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.ResetButton'), 'The Reset Button is not present'

        # Assert the presence of the Login Button
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '.ConfirmButton'), 'The Log In Button is not present'

        # Assert the presence of the Stun/Turn same login toggle
        stun_turn_toggle = scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SwitchButton')
        assert stun_turn_toggle, 'The Stun/Turn toggle is not present'

        # Open the STUN/TURN Settings
        stun_turn_toggle.click()

        # Assert the presence of the TURN Server field
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '.InputTextField[name="turnServer"]'), 'The TURN Server field is not present'

        # Assert the presence of the TURN User Name field
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '.InputTextField[name="turnUser"]'), 'The TURN user name field is not present'

        # Assert the presence of the TURN password field
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '.InputTextField[name="turnPassword"]'), 'The TURN password field is not present'

        # Assert the presence of the STUN server field
        assert scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '.InputTextField[name="stunServer"]'), 'The STUN Server field is not present'
        scenic_instance_1.return_to_main_page()

    except NoSuchElementException as e:
        scenic_instance_1.return_to_main_page()
        assert False, f'UI element could not be found. Error: {e.msg}'

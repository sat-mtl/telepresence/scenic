from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time
import os


def test_bilateral_sip_call(scenic_instance_1, scenic_instance_2) -> None:

    try:
        SERVER_ADDRESS = os.environ["SERVER_ADDRESS"]
        USER1_SIP_SOURCE_PORT = os.environ["USER1_SIP_SOURCE_PORT"]
        USER1_NAME = os.environ["USER1_NAME"]
        USER1_PASSWORD = os.environ["USER1_PASSWORD"]

        USER2_SIP_SOURCE_PORT = os.environ["USER2_SIP_SOURCE_PORT"]
        USER2_NAME = os.environ["USER2_NAME"]
        USER2_PASSWORD = os.environ["USER2_PASSWORD"]

        # Find window handle for scenic instance 1
        scenic_instance_1_window_handle = scenic_instance_1.driver.current_window_handle
        print("Scenic Instance 1 window handle: " + scenic_instance_1_window_handle)

        # Login to SIP on station 1
        scenic_instance_1.login_to_sip(SERVER_ADDRESS, USER1_SIP_SOURCE_PORT, USER1_NAME, USER1_PASSWORD)

        # Add User2 as a SIP contact in User1's matrix
        print("I'm gonna click the contact now")
        scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, f'.SessionContact[data-contact="{USER2_NAME}@{SERVER_ADDRESS}"] .SipContactButton').click()
        time.sleep(2)

        # Close the SIP drawer so it's not in the way later
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR,
                                              '.SipDrawersMenu').click()

        # Initialise scenic station 2 and connect to switcher
        time.sleep(3)
        scenic_instance_2_window_handle = scenic_instance_2.driver.current_window_handle
        print("Scenic Instance 2 Window Handle: " + scenic_instance_2_window_handle)

        # Login to SIP on station 2
        scenic_instance_2.login_to_sip(SERVER_ADDRESS, USER2_SIP_SOURCE_PORT, USER2_NAME, USER2_PASSWORD)

        # Add User1 as a SIP contact in User2's matrix
        print("I'm gonna click the User 1 contact name now!")
        scenic_instance_2.driver.find_element(By.CSS_SELECTOR,
                                              f'.SessionContact[data-contact="{USER1_NAME}@{SERVER_ADDRESS}"] .SipContactButton').click()
        time.sleep(2)
        # Close the SIP drawer, again, so it's not in the way later
        scenic_instance_2.driver.find_element(By.CSS_SELECTOR,
                                              '.SipDrawersMenu').click()

        # Return to User1's station
        print('I will try to switch back to Scenic 1 now')
        scenic_instance_1.driver.switch_to.window(scenic_instance_1_window_handle)
        time.sleep(2)

        # Instantiate and power on the test source on User 1's station
        scenic_instance_1.create_quiddity_from_menu('Sources', 'Audio', 'Test Signal')
        scenic_instance_1.activate_source('audioTestInput1')

        # Connect the stream to User 2
        print("Assigning source to User 2")
        scenic_instance_1.connect_to_sip_destination('audioTestInput1', 'sip', USER2_NAME)
        time.sleep(2)

        # Open the SIP Drawer
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR,
                                              '.SipDrawersMenu').click()

        # Attempt to complete the SIP call
        print("trying to find the call button!")
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SessionPartner .SipContactButton').click()

        # Switch back to User2's Station
        print('I will try to switch back to Scenic 2 now')
        scenic_instance_2.driver.switch_to.window(scenic_instance_2_window_handle)

        # Ensure that the audio test source has appeared on the matrix
        time.sleep(3)
        scenic_instance_2.assert_sip_source_presence('audioTestInput1')

        # Instantiate and power on the test source on User 2's station
        scenic_instance_2.create_quiddity_from_menu('Sources', 'Audio', 'Test Signal')
        time.sleep(2)
        scenic_instance_2.activate_source('audioTestInput2')


        # Assign it to Scenic 1
        print("Assigning source to User 1")
        scenic_instance_2.connect_to_sip_destination('audioTestInput2', 'sip', USER1_NAME)
        time.sleep(2)

        # Open the SIP drawer and Call Scenic 1
        scenic_instance_2.driver.find_element(By.CSS_SELECTOR,
                                              '.SipDrawersMenu').click()
        # Attempt to complete the SIP call
        print("trying to find the call button!")
        scenic_instance_2.driver.find_element(By.CSS_SELECTOR, '.SessionPartner .SipContactButton').click()

        # Switch to Scenic 1 Window
        print('I will try to switch back to Scenic 1 now')
        scenic_instance_1.driver.switch_to.window(scenic_instance_1_window_handle)

        # Ensure that audio test source from Scenic 2 has appeared on the matrix
        time.sleep(3)
        scenic_instance_1.assert_sip_source_presence('audioTestInput2')
        print('Bilateral SIP call successful!')

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'

from selenium import webdriver
from selenium.webdriver.common.by import By
import time


class GetAttribute():
    def test(self):
        baseUrl = "https://letskodeit.teachable.com/pages/practice"
        driver = webdriver.Chrome()
        driver.maximize_window()
        driver.implicitly_wait(10)
        driver.get(baseUrl)

        element = driver.find_element_by_id("bmwradio")
        result = element.get_attribute("value")

        print("Value of the attribute is " + result)
        time.sleep(1)
        driver.quit()


chrome = GetAttribute()
chrome.test()

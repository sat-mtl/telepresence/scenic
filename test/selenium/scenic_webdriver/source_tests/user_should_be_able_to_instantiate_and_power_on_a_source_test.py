from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.color import Color
import time

def test_quiddity_creation_and_activation(scenic_instance_1):

    # Instantiate and power on a source
    scenic_instance_1.create_quiddity_from_menu('Sources', 'Video', 'Test Pattern')
    time.sleep(2)
    scenic_instance_1.activate_source('videoTestInput1')

    # Assert that the source is active
    active_entries = scenic_instance_1.driver.find_elements(By.CSS_SELECTOR, '.EntryStatus-active')
    assert len(active_entries) > 0, 'The source that was instantiated is not active'

    # Assert the the colour of the active state bar is green
    colour_of_entry_status_indicator = Color.from_string(scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SourceHead .EntryStatus ').value_of_css_property('background-color')).hex
    assert colour_of_entry_status_indicator == '#38c566', 'The active status indicator is not green.'

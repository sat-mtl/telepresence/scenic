from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import time
import uuid

def test_TrashCurrentSessionModal (scenic_instance_1) -> None:
    TEST_FILE_NAME_1 = 'SeleniumTestFile' + str(uuid.uuid4())

    try:
        # Create a saved session
        scenic_instance_1.create_quiddity_from_menu('Sources', 'Video', 'SDI 1')
        scenic_instance_1.save_session_file(TEST_FILE_NAME_1)

        # Run the delete_session_file function to cause the TrashCurrentSessionModal to appear without deleting the file
        scenic_instance_1.delete_session_file(TEST_FILE_NAME_1, False)

        # Assert the presence of the TrashCurrentSessionModal
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#TrashCurrentSessionModal'), 'The TrashCurrentSessionModal did not appear when the user selected the current saved file'

        # Assert the presence of the cancel button and select buttons
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.CancelButton'), 'The modal appeared but did not contain a cancel button'
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton'), 'The modal appeared but did not contain a confirm button'

        # Click the confirm button and ensure that the test file was selected
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton').click()
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, f'.FileDeletionEntry-selected .FileEntry[data-entry="{TEST_FILE_NAME_1}"]')

        # Delete the test file to avoid save file pollution; but unselect the file first and open the Save Drawer so the file can be found
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR,
                                              f'.FileDeletionEntry-selected .FileEntry[data-entry="{TEST_FILE_NAME_1}"]').click()
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SaveAsSessionMenu[type="button"]').click()
        scenic_instance_1.delete_session_file(TEST_FILE_NAME_1, True)

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'



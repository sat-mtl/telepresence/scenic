from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import time

def test_RtmpModal(scenic_instance_1) -> None:

    try:
        scenic_instance_1.create_quiddity_from_menu('Destinations', 'Network', 'RTMP Streaming') #TODO doesn't work right now because network group is non-interactable

        # Assert the presence of the Modal
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#RtmpModal'), 'The RtmpModal did not appear'

        # Assert the presence of the text fields
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.UrlInput'), 'The RTMP URL field is missing or misnamed'
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.StreamKeyInput'), 'The Stream Key field is missing or misnamed'

        # Assert the presence of the Cancel button
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.CancelButton'), 'The Cancel button is missing in the RtmpModal'

        # Try to click the 'Create' button without entering a file name and ensure it does not work when the fields are empty (also asserts it's presence)
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton').click()
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#RtmpModal'), 'The RtmpModal closed after trying to click Confirm with empty fields'

        # Generate credentials and confirm the function of the Rtmp Modal
        scenic_instance_1.generate_fake_rtmp_creds()

        # Assert the presence of the RTMP divider which confirms an RTMP destination has been created
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#rtmp-destinations'), 'An RTMP destination was not created'

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import time
import json
import os
import uuid


def test_loaded_session(scenic_instance_1) -> None:
    TEST_FILE_NAME_1 = 'SeleniumTestFile' + str(uuid.uuid4())
    TEST_FILE_NAME_2 = 'SeleniumTestFile' + str(uuid.uuid4())
    PATH_TO_DOWNLOADS_FOLDER = os.environ["PATH_TO_DOWNLOADS_FOLDER"]

    try:
        scenic_instance_1.create_quiddity_from_menu('Sources', 'Video', 'SDI 1')
        scenic_instance_1.save_session_file(TEST_FILE_NAME_1)
        scenic_instance_1.download_session_file(TEST_FILE_NAME_1)

        # Parse the downloaded test file
        with open(PATH_TO_DOWNLOADS_FOLDER + TEST_FILE_NAME_1 + '.json') as previously_saved_file:
            previously_saved_file_data = json.load(previously_saved_file)
        print(previously_saved_file_data)

        # Reinitialize Scenic
        scenic_instance_1.initialize_scenic()

        # Open the saved test file and save it as a new test file
        scenic_instance_1.load_session_file(TEST_FILE_NAME_1, True)
        scenic_instance_1.save_session_file(TEST_FILE_NAME_2)

        # Download the second test file
        scenic_instance_1.download_session_file(TEST_FILE_NAME_2)
        time.sleep(2)

        # Parse the second test file
        with open(PATH_TO_DOWNLOADS_FOLDER + TEST_FILE_NAME_2 + '.json') as current_saved_file:
            current_saved_file_data = json.load(current_saved_file)
        print(current_saved_file_data)

        # Compare the test files
        assert previously_saved_file_data == current_saved_file_data, "The json files do not contain the same data."

        # Ensure the current loaded session contains the elements that it should have
        session_file_elements = scenic_instance_1.driver.find_elements(
            By.CSS_SELECTOR, '.SourceStarter[data-nickname="sdiInput11"]')
        assert session_file_elements, "The session file is empty; saving or loading did not work"

        # Remove the generated test files
        scenic_instance_1.delete_session_file(TEST_FILE_NAME_1, True)
        scenic_instance_1.delete_session_file(TEST_FILE_NAME_2, True)

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'

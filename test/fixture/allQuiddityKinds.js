import { NDI_SNIFFER_KIND_ID, SIP_KIND_ID } from '@models/quiddity/specialQuiddities'

import QUID_KINDS from '@fixture/description.classes.json'
import Kind from '@models/quiddity/Kind'

const jsonKinds = QUID_KINDS.kinds
const quiddityKinds = Object.values(jsonKinds).map(json => Kind.fromJSON(json))
const videoTestSource = quiddityKinds.find(cl => cl.id === 'videotestsrc')

const sipKind = quiddityKinds.find(cl => cl.id === SIP_KIND_ID)
const executorKind = quiddityKinds.find(cl => cl.id === 'executor')

const ndiSnifferKind = Kind.fromJSON({ ...executorKind.toJSON(), class: NDI_SNIFFER_KIND_ID })
quiddityKinds.push(ndiSnifferKind)

const testKind = new Kind('test', 'test', 'test')
quiddityKinds.push(testKind)

function getKind (quiddity) {
  return quiddityKinds.find(cl => cl.id === quiddity.kindId)
}

export default quiddityKinds

export {
  getKind,
  sipKind,
  ndiSnifferKind,
  testKind,
  videoTestSource,
  jsonKinds
}

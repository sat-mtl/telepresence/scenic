/* global jest */

export function mockComponentFunction (Component, fn) {
  const method = Component.prototype[fn]
  const mock = jest.fn(method)

  Component.prototype[fn] = mock
  return mock
}

export const mockedEvent = {
  stopPropagation: jest.fn(),
  preventDefault: jest.fn()
}

export function mockEventFunction (fn) {
  return jest.fn(() => { fn(mockedEvent) })
}

/* global jest */

export function ShmdataMock (path, name, width, height) {
  this.get = jest.fn((prop) => {
    switch (prop) {
      case 'name': return name
      case 'path': return path
      case 'capabilities': return { width: width, height: height }
      default: return null
    }
  })
}

import React, { useContext } from 'react'
import { observer } from 'mobx-react'
import classNames from 'classnames'

import { AppStoresContext } from '@components/App'

import StatusEnum from '@models/common/StatusEnum'

import { Inputs } from '@sat-mtl/ui-components'
const { Checkbox } = Inputs

/** Displays a checkbox in order to change the `started` property
 * @Selector `.QuiddityCheckbox`
 * @param {models.Quiddity} quiddity - The quiddity model
 * @returns {external:mobx-react/ObserverComponent} - The started checkbox
*/
const StartedCheckbox = observer(({ quiddity }) => {
  const { propertyStore } = useContext(AppStoresContext)
  const cn = classNames('QuiddityCheckbox', `${quiddity.id}-checkbox`)

  const handleCheckboxChange = async ({ quiddity }) => {
    const isStarted = propertyStore.isStarted(quiddity?.id)
    await propertyStore.applyNewPropertyValue(quiddity.id, 'started', !isStarted)
  }
  return (
    <div className={cn}>
      <Checkbox
        status={StatusEnum.ACTIVE}
        shape='circle'
        type='secondary'
        size='normal'
        checked={propertyStore.isStarted(quiddity.id)}
        onChange={() => handleCheckboxChange()}
      />
    </div>
  )
})

export default StartedCheckbox

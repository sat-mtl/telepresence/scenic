import React, { useContext } from 'react'
import { useTranslation } from 'react-i18next'
import { observer } from 'mobx-react'

import OrderEnum, { toOrderButtonId } from '@models/common/OrderEnum'
import { AppStoresContext } from '@components/App'
import { Common } from '@sat-mtl/ui-components'

import '@styles/panels/OrderPanel.scss'

const { Button, Icon, NavTab } = Common
const { ExtraButton } = NavTab

/**
 * Renders a button that orders a source or a destination quiddity
 * @selector `#OrderButton`
 * @memberof module:components/panels.OrderPanel
 * @param {Function} onClick - Function triggered when the button is clicked
 * @param {external:react/Component} children - A contextual content
 * @param {string} rotation - The type of rotation of the right icon
 * @param {string} direction - Direction where the order should be updated
 * @returns {external:react/Component} An order button
 */
const OrderButton = ({ direction, children, onClick, rotation, disabled }) => {
  return (
    <ExtraButton>
      <Button
        id={toOrderButtonId(direction)}
        disabled={disabled}
        className='OrderButton'
        shape='square'
        type='heavy'
        size='small'
        onClick={onClick}
      >
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            width: '24px',
            height: '24px',
            transform: `${rotation}`
          }}
        >
          <Icon type='right' />
          {children}
        </div>
      </Button>
    </ExtraButton>
  )
}

/** Renders the right order button */
const OrderButtonRight = observer(({ disabled }) => {
  const { orderStore, quiddityStore } = useContext(AppStoresContext)
  const quidId = quiddityStore.selectedQuiddity?.id
  const newOrder = orderStore.destinationOrders.get(quidId) + 1

  return (
    <OrderButton
      disabled={disabled}
      direction={OrderEnum.RIGHT}
      rotation='rotateY(0deg)'
      onClick={() => orderStore.updateDestinationOrder(quidId, newOrder, OrderEnum.RIGHT)}
    />
  )
})

/** Renders the left order button */
const OrderButtonLeft = observer(({ disabled }) => {
  const { orderStore, quiddityStore } = useContext(AppStoresContext)
  const quidId = quiddityStore.selectedQuiddity?.id
  const newOrder = orderStore.destinationOrders.get(quidId) - 1

  return (
    <OrderButton
      disabled={disabled}
      direction={OrderEnum.LEFT}
      rotation='rotateY(180deg)'
      onClick={() => orderStore.updateDestinationOrder(quidId, newOrder, OrderEnum.LEFT)}
    />
  )
})

/** Renders the upward order button */
const OrderButtonUp = observer(({ disabled }) => {
  const { orderStore, quiddityStore } = useContext(AppStoresContext)
  const quidId = quiddityStore.selectedQuiddity?.id
  const newOrder = orderStore.sourceOrders.get(quidId) - 1

  return (
    <OrderButton
      disabled={disabled}
      direction={OrderEnum.UP}
      rotation='rotate(-90deg)'
      onClick={() => orderStore.updateSourceOrder(quidId, newOrder, OrderEnum.UP)}
    />
  )
})

/** Renders the downward order button */
const OrderButtonDown = observer(({ disabled }) => {
  const { orderStore, quiddityStore } = useContext(AppStoresContext)
  const quidId = quiddityStore.selectedQuiddity?.id
  const newOrder = orderStore.sourceOrders.get(quidId) + 1

  return (
    <OrderButton
      disabled={disabled}
      direction={OrderEnum.DOWN}
      rotation='rotate(90deg)'
      onClick={() => orderStore.updateSourceOrder(quidId, newOrder, OrderEnum.DOWN)}
    />
  )
})

/**
 * Renders the control panel for the quiddity orders
 * @returns {external:react/Component} The order panel only when the user has selected a quiddity
 */
const OrderPanel = observer(() => {
  const { orderStore } = useContext(AppStoresContext)
  const { t } = useTranslation()

  const $tools = []

  if (orderStore.isLeftButtonDisplayed || orderStore.isRightButtonDisplayed) {
    $tools.push(
      <OrderButtonLeft
        key='OrderButtonLeft'
        disabled={!orderStore.isLeftButtonDisplayed}
      />,
      <OrderButtonRight
        key='OrderButtonRight'
        disabled={!orderStore.isRightButtonDisplayed}
      />
    )
  }

  if (orderStore.isUpButtonDisplayed || orderStore.isDownButtonDisplayed) {
    $tools.push(
      <OrderButtonUp
        key='OrderButtonUp'
        disabled={!orderStore.isUpButtonDisplayed}
      />,
      <OrderButtonDown
        key='OrderButtonDown'
        disabled={!orderStore.isDownButtonDisplayed}
      />
    )
  }

  return (
    <div className='OrderPanel'>
      {$tools.length !== 0 && (
        <p className='OrderPanelTitle'>
          {t('Order Panel')}
        </p>
      )}
      <div className='OrderPanelButtons'>
        {$tools}
      </div>
    </div>
  )
})

export default OrderPanel

export {
  OrderButtonLeft,
  OrderButtonRight,
  OrderButtonUp,
  OrderButtonDown
}

/**
 * StatusEnum defines a shared status for all UI components
 * @external ui-components/Common/StatusEnum
 * @see [UIComponents documentation]{@link https://sat-mtl.gitlab.io/telepresence/ui-components/}
 */

/**
 * @module models/common
 * @desc Define all common structures
 */

/**
 * @module models/shmdata
 * @desc Define all structures that modelizes shmdatas
 */

/**
 * @module models/quiddity
 * @desc Define all structures that modelize quiddities
 */

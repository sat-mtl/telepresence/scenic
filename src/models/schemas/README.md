# RFC : Scenic object models

Scenic uses JSON-based objects called `userData` to store data about scenes and connections. These objects are stored by Switcher into an `emptyquid` quiddity named `userTree`. It adds triggers called `signals` to notify Scenic when changes happen to `userData`.

## Revisions

| Revision  | Date
|-----------|-------
| rev0      | 2017
| rev1      | 12/11/2019

## Are these models documented ?

### Current status (revision0)

No. The only way to get information on the models is by looking at the Scenic source code or by starting a Scenic session and using `switcher-ctrl` in order to view the current data stored into Switcher.

```bash
switcher-ctrl -u userTree .
```

### Revision 1 proposal

Use schemas in order to have a **clear** idea of what `userData` should be. Since the current data language used by Switcher and Scenic is JSON, there is a perfect match with the [JSON Schema specification](https://json-schema.org/). This specification is simple and offers a lot of implementations in a wide range of languages :

| Language   | Implementation
|------------|---------------
| JavaScript | [Ajv](https://ajv.js.org/)
| C++        | [JSON schema validator](https://github.com/pboettch/json-schema-validator)
| Python     | [Fast JSON schema for Python](https://horejsek.github.io/python-fastjsonschema)
| Rust (lol) | [Jsl](https://github.com/json-schema-language/json-schema-language-rust)

## Scene model

### Current status (revision0) of Scene model

The current object model is the following :

```json5
{
  "id": "scene0",
  "properties": {
    "name": "Default scene",
    "active": true,
    "select": true
  },
  "connections": {
    "connection0": {
      // Referenced as connection object
    }
  }
}
```

### Problems with current Scene model

| Key           | Discussion | Proposal
|---------------|------------|------------
| `properties`  | This additional object adds an abstraction to the model : it is possible to add any property we want, of any type. But the current Scenic implementation seems static and not flexible. It would better to remove this abstraction to give a better picture of the modifications implied by a future change of the model. | Merge the `property` object into the "scene" itself.
| `select`      | Sets a flag when a scene is selected. It is a **client-only** feature and there is no good reason to store this state into Switcher. It will cause problems when using the **multi-client** feature. | Don't sync this flag in `userData`.
| `connections` | This object is a representation of all connections in a scene. Some scenes share connections but all of them are copied into every scene. It is a good idea to have a map of all of a scene's connections in its model, but there are some drawbacks to storing of all of them in every scene. | Store the connection map in another object and only add references to the scene's connections in its model.

### Proposal for Scene model schema

See [Scene model schema](./scene.schema.json)

## Connection model

### Current status (08-11-2019) of Connection model

```json5
{
  "source": "videotestsrc0",
  "destination": "videomonitor0",
  "id": "videotestsrc0videomonitor0",
  "shmdata": "writer./tmp/switcher_scenic8000_8_video",
  "type": "sink",
  "category": "video"
}
```

### Problems with current Connection model

| Key           | Discussion | Proposal
|---------------|------------|------------
| `shmdata`     | The `shmdata` is updated by the scene loading, it is risky to hardcode it into an object. | Replace this by an `active` flag.
| `type`        | This tag is mapped on some quiddity properties: it could be interesting to use this tag in order to store some information about the behaviour of certain connections, like `persistant`. | Add a meaningful enum in order to set a specific behaviour for the connection.
| `category`    | Is this property currently used? It could be interesting to use this in order to display some fancy things around the connection, but this isn't a priority. | Remove this property.

### Proposal for Connection model schema

See [Connection model schema](./connection.schema.json)

## userTree model

### Name of the model

The `userTree` contains the user data of the current Scenic user. It mainly contains the scenes as defined above. This name is acceptable, but since it only contains `scenes`, it may be more appropriate to call it the `sceneTree` in the future.

As a side note, here is a `contactsSelected` property present in the original model. What should we do with it? We need to check how it is currently used by Scenic.

```json
{
  "contactsSelected" : {
    "contacts" : [
      "qasip1@sip.scenic.sat.qc.ca"
    ]
  }
}
```

### Schema of the userTree model

The schema is based on the previous reflexion which stipulates that the connections should't be hard-defined into the scene objects.

See [userTree model schema](./userTree.schema.json)

### Other usages ?

Scenic's configuration should also be defined as a JSON-defined object inside another `emptyquid` (a `configTree` object). This would allow the dynamic change of some Scenic configurations.

## Resources

+ [Learn JSON Schema](https://json-schema.org/learn/)
+ [Understanding JSON Schema](https://json-schema.org/understanding-json-schema/index.html)
+ [General information about the site JSON Schema](https://cswr.github.io/JsonSchema/)

# Naming convention

In order to improve collaboration for Scenic App's development, we have adopted a naming convention for functions and class names in the App's code.

## Method names

### Disclaimer

The following names are **prefixes** that should be applied to every method of the **Scenic App** codebase. This convention must be respected in order for your Merge Request to be accepted. The goal of this convention is not to enforce some arbitrarily chosen rules, but rather to make the codebase clearer and to prevent developers from adding functions with unclear behaviour. All new propositions to this nomenclature should be discussed with the maintenance team and have a clear justification.

| Prefix         | Description                                                                                                                                   | Return         | isAsync? | isMobxAction?
|----------------|-----------------------------------------------------------------------------------------------------------------------------------------------|----------------|----------|---------------
| add            | Adds an element to a data structure                                                                                                           | /              | No       | Yes
| apply          | Requests a server action. Usually invokes an API call                                                                                         | Boolean?       | Yes      | No
| bind           | Associates a key to a callback, encapsulates a MobX action or encapsulates a response to a server signal. Usually invokes a **handle** method |                | No       | No
| clean          | Removes a part of a data structure following some criteria. DOESN'T deinitialize the store                                                    | /              | Yes / No | No
| clear          | Removes all data from a structure. Deinitializes the store                                                                                    | Boolean?       | No       | Yes
| compute        | Applies an algorithm on a data structure                                                                                                      | Anything       | No       | No
| export         | Exports data from the client to a specific format, independently of the server                                                                | String         | No       | No
| fallback       | Emergency method called when a requirement is not fullfilled                                                                                  | Boolean?       | Yes      | No
| fetch          | Gets data from the server                                                                                                                     | Targeted data  | Yes      | No
| filter         | Filters data structures according to some criteria                                                                                            | Filtered data  | No       | No
| find           | Finds one or several values from data structures according to some criteria. Doesn't imply any server request                                 | Targeted value | No       | No
| handle         | Handles a server signal or a client interaction. Typically used for callbacks                                                                 | /              | No       | No
| import         | Imports data structures in a specific format                                                                                                  | Imported data  | No       | No
| initialize     | Initializes the configuration of a store in order to replicate the server state                                                               | Boolean        | Yes      | No
| is **or** has  | Check the existence of a value in a data structure or checks if a value matches some criteria                                                 | Boolean        | No       | No
| make           | Safe and smart creation of a model                                                                                                            | Model          | Yes / No | No
| merge          | Merge two or more data structures together                                                                                                   | Merged data    | No       | No
| migrate        | Reserved method to convert a JSON schema from a version to another                                                                            | JSON object    | No       | No
| notify         | Notifies some events to the user                                                                                                              | /              | No       | No
| populate       | Adds values to a data structure using some criteria                                                                                           | JSON object    | Yes / No | No
| remove         | Removes a value from a data structure                                                                                                         | /              | No       | Yes
| reset          | Composite method that is sugar for a **clear** followed by an **initialize**                                                                  | /              | Yes      | Yes
| set            | Defines a value in (**or** as) a data structure                                                                                               | /              | No       | Yes
| sort           | Sorts a data structure by comparing two elements from the structure                                                                           | Boolean. Must be compatible with Array.sort | No       | No
| synchronize    | Composite method that is sugar for a **populate** followed by a **clean**                                                                     | /              | Yes / No | No
| to **or** from | Converts a model to another format (**or** vice-versa)                                                                                        | Converted data | No       | No
| toggle         | Adds an element if it is present in the data structure or delete it                                                                            | /              | No       | No
| update         | Changes a data structure according to some criteria                                                                                           | /              | Yes / No | No

## Class names

### Attribute rules

* The class names are used for components that are displayed **multiple times**
* The ID are used for **unique** components, the ID depends of the generation context.
* React component should contain their own class name such as a `className` or an `id` property of themselves

### Case rules

* All the React Component class names are formatted in **Pascal Case**.
* All the internal class names should also be formatted in **Pascal Case**.
* All the IDs should be fomatted in **Kebab Case**.

The component's class name can be set as the `id` or the `className` of the HTML element, depending on its usage. For example, the page `MatrixPage` should be selected with the CSS selector `#MatrixPage` but the form `ColorForm` should be selected with `.ColorForm`.

### Example

The component `MyButton` is used multiple times in the web app and has an ID generated from the quiddity ID and its specialization (a button). So if the attached quiddity ID is *videoOutput0*, it will be rendered as :

```html
<button className="MyButton" id="videoOutput0-button" />
```

### Nomenclature table

A table can summarize all the rules :

| *Component*: MyComponent  | Displayed **only once**    | Displayed **multiple times**                        |
|---------------------------|----------------------------|-----------------------------------------------------|
| **Has no** generated ID   | `<div id="MyComponent" />` | `<div className="MyComponent" />`                   |
| **Has** a generated ID    | *NOT ALLOWED*              | `<div className="MyComponent" id="generated-id" />` |

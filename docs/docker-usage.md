# Scenic Docker usage

Scenic front-end can be served run from a pre-built Docker binary image available from registry.gitlab.com/sat-mtl/tools/scenic/scenic.

## Setup host for scenic

### Install Docker

Install Docker and add your user to `docker` group.

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo apt install docker-compose-plugin
sudo /sbin/usermod -aG docker $USER
```

## Launch Scenic

```bash
docker run \
  --rm \
  -it \
  --name scenic \
  -p 8080:8080 \
  registry.gitlab.com/sat-mtl/tools/scenic/scenic:update-docker-image
```

Open http://localhost:8080 in your browser.

## Build and push scenic image

Using Docker qemu-arm emulation support and buildx builder allow to create multi-arch image capable of running on both arm64 and amd64.

`BUILDX_NO_DEFAULT_ATTESTATIONS=1` is a [GitLab SLSA Provenance bug](https://gitlab.com/gitlab-org/gitlab/-/issues/388865#note_1557255192) workaround for multiple manifest multi-arch images.

```bash
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx create --name mybuilder --use
docker buildx inspect --bootstrap
docker login registry.gitlab.com
BUILDX_NO_DEFAULT_ATTESTATIONS=1 docker buildx build \
  --progress plain \
  --push \
  --platform linux/amd64,linux/arm64 \
  -t registry.gitlab.com/sat-mtl/tools/scenic/scenic:<your-current-working-branch> \
  -f dockerfiles/h2o.Dockerfile \
  .
docker manifest inspect registry.gitlab.com/sat-mtl/tools/scenic/scenic:<your-current-working-branch>
```

<!-- AUTO GENERATED DOCUMENTATION: DO NOT MODIFY -->
# Glossary of CSS selectors
| Component name            | Description                                                                                              | Css selector                                                                                                  |
| ------------------------- | -------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------- |
| SceneTabBar               | <p>Renders the scene tab bar that contains all tabs of user created scenes and the default scene tab</p> | `#SceneTabBar`                                                                                                |
| Lock                      | <p>Renders a lock icon</p>                                                                               | `.Lock`                                                                                                       |
| ConnectionLock            | <p>Renders alock icon that fits with Connection boxes</p>                                                | `.ConnectionLock`                                                                                             |
| PropertyField             | <p>Renders the Field used to change a property according to its type</p>                                 | `.PropertyField[data-quiddity="quiddity id"][data-nickname="quiddity nickname"][data-property="property id"]` |
| PropertyInspectorDrawer   | <p>Drawer that displays all property of a selected quiddity</p>                                          | `#PropertyInspectorDrawer`                                                                                    |
| RowEntry                  | <p>A generic entry that should be display in a column or as a row header</p>                             | `.RowEntry[data-entry="entry title"]`                                                                         |
| DestinationHead           | <p>Displays a destination head for a destination quiddity</p>                                            | `#DestinationHead[data-quiddity="nickname"]`                                                                                            |
| PreviewWrapper            | <p>A preview windows for all video shmdatas</p>                                                          | `#PreviewWrapper[data-quiddity="quiddity id"][data-shmdata="shmdata path"]`                                   |
| SourceShmdata             | <p>Renders a shmdata box related to a source</p>                                                         | `.SourceShmdata[data-shmdata="shmdata path"]`                                                                 |
| EmptyShmdata              | <p>Renders an empty box that replaces a shmdata</p>                                                      | `.EmptyShmdata`                                                                                               |
| SourceLock                | <p>Renders a lock on the source</p>                                                                      | `.SourceLock`                                                                                                 |
| SourceStarter             | <p>Renders a checkbox that starts or stops a source</p>                                                  | `.SourceStarter[data-source="source id"][data-nickname="source nickname"]`                                    |
| EmptyStarter              | <p>Renders an empty starter that does nothing</p>                                                        | `.EmptyStarter`                                                                                               |
| SourceHead                | <p>Renders the head of the source</p>                                                                    | `.SourceHead[data-source="source id"][data-nickname="source nickname"]`                                       |
| PageIcon                  | <p>Renders the page's icon</p>                                                                           | `#PageMenu .PageIcon`                                                                                         |
| PageTitle                 | <p>Renders the page's title</p>                                                                          | `#PageMenu .PageTitle`                                                                                        |
| PageButton                | <p>Renders the menu button that triggers the page rendering</p>                                          | `#PageMenu .PageButton`                                                                                       |
| ScenicSignature           | <p>The signature of Scenic</p>                                                                           | `#PageMenu .ScenicSignature`                                                                                  |
| PageMenu                  | <p>Menu that renders all main menus of the Scenic app</p>                                                | `#PageMenu`                                                                                                   |
| PanelBar                  | <p>Renders all matrix panels</p>                                                                         | `.PanelBar`                                                                                                   |
| MenuBar                   | <p>Renders the menu bar</p>                                                                              | `.MenuBar`                                                                                                    |
| TabBar                    | <p>Renders the tab bar for the Matrix</p>                                                                | `.TabBar`                                                                                                     |
| SideBar                   | <p>Renders the side bar</p>                                                                              | `.SideBar`                                                                                                    |
| DrawerOverlay             | <p>Renders all drawers used in the Matrix</p>                                                            | `.DrawerOverlay`                                                                                              |
| NewSessionMenu            | <p>Renders the button that resets the session</p>                                                        | `#NewSessionMenu`                                                                                             |
| SaveSessionAsMenu         | <p>Renders the button that opens the FileSavingDrawer</p>                                                | `#SaveAsSessionMenu`                                                                                          |
| SaveSessionMenu           | <p>Renders the button that saves the current session</p>                                                 | `#SaveSessionMenu`                                                                                            |
| OpenSessionMenu           | <p>Renders the button that opens the FileLoadingDrawer</p>                                               | `#OpenSessionMenu`                                                                                            |
| DeleteSessionMenu         | <p>Renders the button that opens the FileDeletionDrawer</p>                                              | `#DeleteMenuButton`                                                                                           |
| PropertyInspectorMenu     | <p>Renders the button that opens the PropertyInspectorDrawer</p>                                         | `#PropertyInspectorMenu`                                                                                      |
| SipDrawersMenu            | <p>Renders the button that opens the Sip drawers</p>                                                     | `#SipDrawersMenu`                                                                                             |
| SceneActivationCheckbox   | <p>Renders scene activation checkbox</p>                                                                 | `#SceneControlPanel .Checkbox`                                                                                |
| CapabilityGroup           | <p>Renders a capability group</p>                                                                        | `.CapabilityGroup`                                                                                            |
| Capabilities              | <p>Renders all capabilities in a HTML definition list</p>                                                | `.Capabilities`                                                                                               |
| ShmdataInformationTooltip | <p>Renders the tooltip</p>                                                                               | `.ShmdataInformationTooltip[data-shmdata="shmdata id"][data-media="media type"]`                              |

## {components/bars}
| Component name | Description                                    | Css selector     |
| -------------- | ---------------------------------------------- | ---------------- |
| SettingTabBar  | <p>Navigation bar for Scenic Settings Tabs</p> | `#SettingTabBar` |

## components/common
| Component name | Description                              | Css selector      |
| -------------- | ---------------------------------------- | ----------------- |
| CancelButton   | <p>Button used to cancel an action</p>   | `.CancelButton`   |
| ResetButton    | <p>Button used to reset some actions</p> | `.ResetButton`    |
| ConfirmButton  | <p>Button used to confirm an action</p>  | `.ConfirmButton`  |
| UploadButton   | <p>Button used to upload something</p>   | `.UploadButton`   |
| DownloadButton | <p>Button used to download something</p> | `.DownloadButton` |
| MenuButton     | <p>Button used in a menu bar</p>         | `.MenuButton`     |

## components/drawers.PropertyInspectorDrawer
| Component name        | Description                                                                                                | Css selector                                 |
| --------------------- | ---------------------------------------------------------------------------------------------------------- | -------------------------------------------- |
| NicknameInput         | <p>Renders the Input used to change a quiddity's nickname</p>                                              | `.NicknameInput`                             |
| QuiddityDescription   | <p>Renders the description of the selected quiddity, this description is stored with the quiddity kind</p> | `.QuiddityDescription`                       |
| QuiddityStatus        | <p>Renders the status of the selected quiddity</p>                                                         | `.QuiddityStatus`                            |
| DeleteQuiddityButton  | <p>Renders the Delete button to delete quiddity</p>                                                        | `.DeleteQuiddityButton`                      |
| ResetPropertiesButton | <p>Renders the Reset button to reset quiddity's properties</p>                                             | `.ResetPropertiesButton`                     |
| PropertyGroup         | <p>Renders a property Group as a Menus.Group</p>                                                           | `.GroupTitle[data-menu="name of the group"]` |

## components/drawers.QuiddityCreationDrawer
| Component name        | Description                                  | Css selector             |
| --------------------- | -------------------------------------------- | ------------------------ |
| QuiddityInput         | <p>An input to set a quiddity attribute</p>  | `.QuiddityInput`         |
| QuiddityKindInput     | <p>An input to set the quiddity kind</p>     | `.QuiddityKindInput`     |
| QuiddityNicknameInput | <p>An input to set the quiddity nickname</p> | `.QuiddityNicknameInput` |

## components/drawers.SipContactDrawer
| Component name        | Description                                                              | Css selector                                  |
| --------------------- | ------------------------------------------------------------------------ | --------------------------------------------- |
| HangupAllButton       | <p>Renders a button that hangups all called contacts</p>                 | `.HangupAllButton`                            |
| LogoutButton          | <p>Renders a button that logouts the user from SIP</p>                   | `.LogoutButton`                               |
| SipContact            | <p>Renders a contact entry</p>                                           | `.SipContact`                                 |
| SipContactButton      | <p>Renders a button that triggers a contextual action with a contact</p> | `.SipContactButton`                           |
| SessionPartner        | <p>Renders a partner</p>                                                 | `.SessionPartner[data-contact="contact uri"]` |
| SessionPartnerSection | <p>Renders partners' section</p>                                         | `#SessionPartnerSection`                      |
| SessionContact        | <p>Renders a contact</p>                                                 | `.SessionContact[data-contact="contact uri"]` |
| SessionContactSection | <p>Renders the contacts section</p>                                      | `#SessionContactSection`                      |

## components/drawers
| Component name   | Description                                      | Css selector        |
| ---------------- | ------------------------------------------------ | ------------------- |
| SipContactDrawer | <p>Drawer that controls all the SIP contacts</p> | `#SipContactDrawer` |

## components/matrix
| Component name | Description                                                     | Css selector                                                                   |
| -------------- | --------------------------------------------------------------- | ------------------------------------------------------------------------------ |
| LockedBox      | <p>Displays a connection box that is locked</p>                 | `.LockedBox`                                                                   |
| ArmedBox       | <p>Displays a connection box that is armed</p>                  | `.ArmedBox`                                                                    |
| ConnectedBox   | <p>Displays a connection box that is connected</p>              | `.ConnectedBox`                                                                |
| DisabledBox    | <p>Displays a connection box that is disabled</p>               | `.DisabledBox`                                                                 |
| ConnectableBox | <p>Displays a connection box that is connectable</p>            | `.ConnectableBox`                                                              |
| ConnectionBox  | <p>Displays a connection between a Source and a Destination</p> | `.ConnectionBox[data-source="source id"][data-destination="destination id"]`   |
| ConnectionBox  | <p>Displays a connection between a Source and a Destination</p> | `.ConnectionBox[data-connection="source id_destination id"]`                   |
| ConnectionBox  | <p>Displays a connection between a Source and a Destination</p> | `.ConnectionBox[data-nickname="source nickname_destination nickname"]`         |
| ConnectionBox  | <p>Displays a connection between a Source and a Destination</p> | `.ConnectionBox[data-connection="source id_destination id_contact"]`           |
| ConnectionBox  | <p>Displays a connection between a Source and a Destination</p> | `.ConnectionBox[data-nickname="source nickname_destination nickname_contact"]` |
| MatrixLayout   | <p>Renders the layout of the matrix</p>                         | `#MatrixLayout`                                                                |

## components/menus
| Component name    | Description                                  | Css selector                                       |
| ----------------- | -------------------------------------------- | -------------------------------------------------- |
| QuiddityMenuItem  | <p>A menu item that creates a quiddity</p>   | `.MenuItem[data-menu="<name of the item>"]`        |
| QuiddityMenuGroup | <p>A group of items in the quiddity menu</p> | `.MenuGroup[data-menu="<name of the collection>"]` |

## components/modals.NdiStreamModals
| Component name   | Description                                                     | Css selector      |
| ---------------- | --------------------------------------------------------------- | ----------------- |
| NdiConfirmButton | <p>Renders the Confirm Button for the Modal</p>                 | `.ConfirmButton`  |
| NdiStreamModal   | <p>Renders the Modal used to show all NDI streams available</p> | `#NdiStreamModal` |

## components/modals
| Component name          | Description                                                | Css selector               |
| ----------------------- | ---------------------------------------------------------- | -------------------------- |
| NewContactModal         | <p>Scenic modal used to create a new contact</p>           | `#NewContactModal`         |
| NewFileModal            | <p>Scenic modal used to create a new file</p>              | `#NewFileModal`            |
| RenameNicknameModal     | <p>Scenic modal used to confirm renaming of a nickname</p> | `#RenameNicknameModal`     |
| RtmpModal               | <p>Scenic modal used to create an RTMP quiddity</p>        | `#RtmpModal`               |
| SwitcherConnectionModal | <p>Scenic modal used to connect to Switcher</p>            | `#SwitcherConnectionModal` |

## components/modals.RtmpModal
| Component name | Description                                              | Css selector      |
| -------------- | -------------------------------------------------------- | ----------------- |
| UrlInput       | <p>Renders the InputText for the RTMP URL</p>            | `.UrlInput`       |
| StreamKeyInput | <p>Renders the InputPassword for the RTMP stream key</p> | `.StreamKeyInput` |

## components/modals.SceneModals
| Component name   | Description                                                      | Css selector        |
| ---------------- | ---------------------------------------------------------------- | ------------------- |
| RenameSceneModal | <p>Renders the modal used to confirm renaming of a Scene</p>     | `#RenameSceneModal` |
| DeleteSceneModal | <p>Renders the modal used to confirm the deletion of a Scene</p> | `#DeleteSceneModal` |

## components/modals.SwitcherConnectionModal
| Component name           | Description                                     | Css selector     |
| ------------------------ | ----------------------------------------------- | ---------------- |
| UrlInput                 | <p>Renders the InputText for the host URL</p>   | `.UrlInput`      |
| PortInput                | <p>Renders the InputText for the host port</p>  | `.PortInput`     |
| SwitcherConnectionButton | <p>Renders the Connect Button for the Modal</p> | `.ConnectButton` |

## components/pages
| Component name             | Description                                                     | Css selector                  |
| -------------------------- | --------------------------------------------------------------- | ----------------------------- |
| AssistanceSection          | <p>The Assistance section</p>                                   | `#AssistanceSection`          |
| InfoSection                | <p>The Info section</p>                                         | `#InfoSection`                |
| ContributingSection        | <p>The Contributing section</p>                                 | `#ContributingSection`        |
| LicenseSubSection          | <p>The Licence section</p>                                      | `#LicenseSubSection`          |
| VersionSubSection          | <p>The Version subsection</p>                                   | `#VersionSection`             |
| ContributorsSection        | <p>The Contributors section</p>                                 | `#ContributorsSection`        |
| AuthorsSubSection          | <p>The Authors subsection</p>                                   | `#AuthorsSubSection`          |
| TestersSubSection          | <p>The Testers subsection</p>                                   | `#TestersSection`             |
| MetalabSection             | <p>The Metalab section</p>                                      | `#MetalabSection`             |
| AboutSection               | <p>The About section</p>                                        | `#AboutSection`               |
| SatLogo                    | <p>Logo of the SAT</p>                                          | `#HelpPage .SatLogo`          |
| HelpPage                   | <p>Page that displays the help resources</p>                    | `#HelpPage`                   |
| LivePage                   | <p>Page that displays all scenes</p>                            | `#LivePage`                   |
| MatrixPage                 | <p>Page that displays all the source and destination matrix</p> | `#MatrixPage`                 |
| GeneralSettingsSection     | <p>The section about the general settings</p>                   | `#GeneralSettingsSection`     |
| AdvancedSipSettingsSection | <p>Subsection that displays all advanced SIP settings</p>       | `#AdvancedSipSettingsSection` |
| RequiredSipSettingsSection | <p>Subsection that displays all required SIP settings</p>       | `#RequiredSipSettingsSection` |
| SipSettingsControlSection  | <p>Subsection that provides all global SIP actions</p>          | `#SipSettingsControlSection`  |
| SipSettingsSection         | <p>Section for all SIP settings</p>                             | `#SipSettingsSection`         |
| SettingsPage               | <p>Page that displays all the settings of the app</p>           | `#SettingsPage`               |

## components/panels.OrderPanel
| Component name | Description                                                            | Css selector   |
| -------------- | ---------------------------------------------------------------------- | -------------- |
| OrderButton    | <p>Renders a button that orders a source or a destination quiddity</p> | `#OrderButton` |

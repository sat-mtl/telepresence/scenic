# Walkthrough

So you have been cast in the middle of the scenic maze, and you want to go find the leaky pipe, fix it and get out of there without being eaten by the mythical state-consistency monster.

This is intended as your guide through this sticky situation.

This will consist of two parts :

- A general description of the program's initialization flow
- A file by file (ish) enumeration of what the different folders/files contains and what their content does.

## Glossary

Scenic/switcher has a history of using obscure and idiosyncratic terminology.

Here are some particularly egregious examples and their meaning.

- Quiddity : A quiddity is a plugin. It is a thing that can have inputs and outputs. It can generate, receive or transmit media. Basically every interaction between scenic and switcher are about or through quiddities. Some examples are the `glfwin` quiddity that creates a window that can display images/video, the `systemusage` quiddity that probes the os and updates itself with cpu usage and other system information.
- bundle : A bundle is a special quiddity that can encapsulate multiple quiddities connected together, have default parameters and hide details from the end user. Most quiddities manipulated by scenic are in fact bundles.
- kind : A kind is a type of quiddity. `glfwin` is a kind. Switcher has a certain number of quiddity kind, but it's possible to define more at runtime via bundles.
- sfid/swid : swid and sfid are the names that are given to input and output slots that a quiddity may have. They stand for **s**hmdata **f**ollower/**w**riter **id**. sfid and swid can be associated to media types and are used by switcher to determine what to plug in where, when we ask it to plug. The gist of it is that a quiddity that outputs video will necessarily have a video swid, and you can plug that swid in the sfid of a quiddity that can take video in its input.
- fallback : In the context of scenic, a method that is prefixed by fallback is often there to try to compensate for a deficiency in switcher. We have removed many of them but some remain.
- infoTree : An infoTree is a json-like (but not quite json) structure. A quiddity has 3 infoTrees. One is commonly known as infoTree (yes I know ...) and it contains various information relating to a quiddity's internal state. Another is the userTree which was used for deleted features and the last one is the connection spec tree.
- session : A session is a particular set of quiddities and quiddity states that are serialized to a json file. This mostly doesn't work right now and I don't think it was ever stable. All the parts of scenic that manage sessions will be glossed over in this document.
- scene : A scene is a set of connections between quiddities. The idea was to be able to use scenic as a kind of show controller and be able to say "for this scene I want camera 1 to be plugged in screen 1 and for another I'd like it to be plugged in screen 2.". This mostly never worked and was very buggy at the best of times. All ui elements related to scenes were disabled but we still have vestigial scene related code throughout the codebase.
- shmdata : shmdata is a protocol used to transfer multimedia stream between applications. In the context of scenic, a shmdata can be thought of a media stream. A shmdata lives somewhere on the computer that runs switcher so it has a path. It also has a caps string.
- caps : caps is a term borrowed from gstreamer. It's a string that describes a kind of media. A caps string looks like this : `video/x-raw-yuv, width=60, height=60`. This tells its uncompressed video with the `yuv` pixel format and a size of 60x60. If you ever find yourself thinking you need to mess around with caps in scenic, don't, and try to use connection specs first. There is still scenic code that uses caps but we try to phase it out.
- connection specs : They live in the connection spec tree of a quiddity. Connection specs describe the different sfids and swids associated with a quiddity and determine which quiddity can be connected with which other.
- claw : I don't think this word is in the scenic codebase but it's an important switcher concept. A quiddity's claw is an object that manages its sfids, swids and connection specs. I thought it meant **C**onnection**law** but Nico told me at some point that its just "une griffe" ...
## Program flow

The program starts in `main.js`. It imports everything it needs and initiates all the different stores.

The populateStores() function from populateStores.js initializes all the stores used by the software in a hierarchic order. There are a lot of stores and the dependencies between them are not always easy to understand or even very logical.

What you really need to know is :

- The SocketStore is at the root of everything : it manages the connection between scenic and switcher.
- The quiddityStore contains the quiddity models of all the quiddities that switcher currently manages.
- Most of the other stores are used to manage a subset of the properties that are present inside a quiddity.

After the stores are populated, they are injected into the App component.

This component is the root of the program and contains a bunch of other components that themselves contain other components.

The first thing you will see when you start scenic is the SwicherConnectionModal which connects the socket from the socketStore with a switcher.

Once the SocketStore is connected to a switcher, it's `onConnection` callback will be called and the various stores will initialize themselves. This will, among other things, call initializeInitQuiddities in the quiddityStore.

Scenic relies on some quiddities always being present when it starts up. The "initQuiddityies" are read from a configuration file and by default are

- systemUsage which is needed to display cpu and network usage
- sip which is needed for sip media transmission.
- thumbnails which generates thumbnails for all the video quiddities
- preview which behaves exactly like thumbnail but for one quiddity at a time.
- ndiSniffer which will inform scenic regarding what ndi streams are available on the network.
- emptyquid which is used to propagate and persist front end information on a switcher insance. Note that this is now mostly(?) unused and was used for the deleted scenes and order features

Once all of this is done, scenic will wait for user interaction or for backend events, periodically receiving infoTree update events from the systemUsage quiddity and displaying the result.

## Folder by folder walktrough

This is a folder by folder walkthrough of the important files in the scenic repository. You don't need to read all of this in one sitting, please use control+f.

### assets
This contains various assets used by scenic. In addition to graphic and cosmetic assets, this acutally contains configuration files that can greatly modify the behaviour of scenic
#### assets/json
These files are either defaults that will extend a user's configuration or example files that show the right format for a configuration file.

Note that switcher is responsible for reading the contents of any config files. This means switcher needs to know where these files are. You can tell it by having a `global.json` config file in your `~/.config/switcher/` folder. The default path for the scenic user configuration files is `~/.config/scenic/`
##### bundles.default.json
this file contains the default bundles that are included with scenic. Most of the init quiddity mentionned previously are defined there. You may have to change stuff there if you want to change default parameters for quiddities. An important resource for this is the [writing-bundles.md](https://gitlab.com/sat-mtl/tools/switcher/-/blob/master/doc/writing-bundles.md?ref_type=heads) file from the switcher repository.

The defaults declared there **will** be partially overridden and extended by a production configuration file when scenic is run on a station. This allows us to define bundles that are fine-tuned for particular hardware.
##### menus.default.json
This describes the quiddities in the quiddity creation menu. It gives them a name, a category and it tells scenic which quiddity kind to ask switcher for.

The defaults declared there **will** be overrided by a production version when scenic is run on a station.
##### scenic.default.json
This contains a mishmash of various configurations. It contains which quiddities should always be present when an instance of scenic runs, it can prevent quiddity properties from being displayed, ... etc.

This will also be overriden on a scenic station but I don't think there's much that is changed there.

##### contacts.example.json
As the name says, this is an example. For SIP features to work, you will need a contacts.json file on your computer. We do not distribute those, you will have to ask for it.

##### switcher.default.json
This is a sample switcher extraConfig file. If you rename this to `global.json` and move this to `~/.config/switcher/`, switcher will be able to find the files declared there.

#### assets/locales
This contains folders for every language under the sun (meaning French and English)

Each folder contains a locale.json file that contains translation keys and values.

### config
This contains the config files needed for the tooling related to scenic. With some luck you'll never have to touch those.

### dockerfiles/h2o.Dockerfile
This is the dockerfile used to build a docker image that serves scenic on localhost via h2o.

### docs
Pretty much every file in there has an intro blurb that tries to explain the file. Go read them if you're interested

### scripts
I think most of those are obsolete.

### src
Now we're talking.

#### src/components
These are all the things that use React and jsx.
##### components/App.js
This is the root component of the page. If you follow the calls inside `initializeSocket()` You will get an understanding of the startup flow of scenic.
##### components/bars/SceneTabBar.js
This is the bar for the scene tabs. Scenes were removed a little while ago so that file is not too interesting and probably mostly obsolete.
##### components/bars/settingTabBar.js
This is the tab bar that swaps the config page from general to sip configs
##### components/bars/StatusBar.js
This mainly displays CPU and network usage. As such, it is dependent on the SystemUsageStore and the systemusage quiddity.
#### components/common

##### components/common/PageWrapper.js
This is a component that wraps the different pages in scenic and displays the right one depending on which page is the active one in the PageStore. If you wanted to add another view to scenic, you could build one and add it there.
##### components/common/Buttons.js
Different button helpers.
#### components/drawers
This contains the different side drawers of scenic. The most important are PorpertyInspectionDrawer and the two SIP ones.

Things I won't cover :
The File drawers are related to session loading and saving.

##### components/drawers/PropertyInspectorDrawer.js
This is probably the most complex drawer. For obvious reasons, this depends on the PropertyStore to get information on the available quiddity properties and their values.

Since properties and their values are also available in the QuiddityStore's quiddities objects, you may be tempted to use those as a source of truth. Don't. The reasons for this will be explained in the entries for the QuiddityStore and the PropertyStore.

For now, if you need to modify this store, you need to use the PropertyStore.properties and the `PropertyStore.values` to get accurate and up-to-date information on the quiddities properties and values respectively. The values are available in the PropertyStore.properties object but they are not guaranteed to be updated.

Quiddity properties can be of many different types. They have a name and can either be at the root or under a category called a group. The started property has special treatment because, even though it acts like a regular boolean property in most regards, it has an important meaning for most quiddities and has a fair amount of dedicated code paths in `switcher`

##### components/drawers/QuiddityCreationDrawer.js
This is a pretty simple drawer and also a fairly self-contained one. Its particularity is that it can only be displayed with the ctrl-a keybinding. It can be used to instanciate a `quiddity` of any `kind`.

You probably won't need to touch this. Note the use of the Mousetrap library : it enables us to globally bind keys instead of having to bind on a particular dom element.

##### components/drawers/SipContactDrawer
This displays the sip contacts and the various buttons used to manipulate sip sessions.

Note that the add contact button is slightly broken and does not do what you would think it does : It temporarily adds a contact to switcher's SIP quiddity but this contact will not persist between calls.

##### components/drawers/SipLoginDrawer.js
This tries to tell the sip quiddity to log in with the parameters given to it. Pretty straightforward.

#### components/entries
This could probably have gone under common.

RowEntry is mainly used to have rows in the contact list.
FileEntry is used for sessions.

#### components/fields
I won't cover those individually.

These are mainly used for quiddity properties that live in the PropertyInspectorDrawer.

#### components/matrix
This is the big one.
##### components/matrix/ConnectionBox.js
**warning** this is a fairly fragile and convoluted part of the code.

This represents one of the spaces in the matrix. If you have a matrix that looks like this :
```
 |a|b|c|
1|x|x|x|
2|x|x|x|
3|x|x|x|
```
Then each x is a ConnectionBox.

The main part of the code here is the `ConnectionBox` component. It depends on a lot of stores and needs to check a lot of things to correctly function.

A ConnectionBox will always be instanciated from the Matrix component with a `sourceEntry` and a `destinationEntry` These often correspond to the media input or output of a whole quiddity but since some quiddities can have multiple inputs and outputs, the mapping is more complicated than just one entry per quiddity. See the `models/MatrixEntry` entry for more details on that.

How the `ConnectionBox` component works is that it will recreate the `Connection` model from the sourceEntry and destinationEntry it was given and run a lot of checks to see if they are connectable, if they are already connected and if they are locked or not. The connection box also looks to see if the connection is "armed" which is a status that's related to scenes and is not used anymore.

Note the presence of a `useEffect` react hook which will call a `fallbackFollowingShmdatas` function. I'm not entirely sure if we still need this but this is to make extra sure that everything is up-to-date.

##### components/matrix/DestinationHead.js

This component manages the display and events related to destination matrixEntries. On click, it will display the property inspection drawer for the quiddity associated with the entry.

##### components/matrix/DestinationSeparator.js

This manages the display of the destination separators. Destinations are separated in three(?) distinct categories :
- local destinations
- NDI destinations
- SIP destinations

##### components/matrix/Matrix.js
This is the parent Matrix component. It creates a grid of source, destination and connection box components and it organizes them with a CSS grid layout.

Sources are displayed from top to bottom in the first column and destinations are displayed left to right in the first row.

Be aware that the css grid layout is in fact a generated string of the format displayed below and that CSS, being what it is, will not throw exceptions in your face when you mess this up. It will simply display something wrong.
```
"a a a"
"b c c"
"b c c"
```

This format has a lot of rules and constraints for what is valid so be careful when you mess around with it. If you ask me, this thing should have been a \<table\>

##### components/matrix/PreviewWrapper

This is a component that is used to have a spot where to display previews. A preview is what shows up when you click on a thumbnail

It's basically just a conditionally displayed \<img\> tag that gets a base64 image data url 10 times per second from the `PreviewStore`.

##### components/matrix/ShmdataBox.js

This displays the little box that sometimes contains a thumbnail on the right side part of a source entry. It manages the calls to display the preview and the tooltip that shows media information on the shmdata associated with the entry.

##### components/matrix/Source.js

This is a component that assembles all the necessary parts of a source. It has a start button that can toggle the start button of a quiddity if needed. It contains the shmdatabox and the name of the quiddity.

It's rendered by the matrix component.

##### components/matrix/SourceSeparator

This is a separator used to categorize sources. Sources are categorized by whether they are local or they come from a distant sip contact. They are categorized by contact.

#### components/menus
This contains the different menu bars.

##### components/menus/PageMenu.js

This is the menu that lives on the left-hand side of the app, where you can change the page from config to matrix to help. It basically changes values in the pageStore to make the PageWrapper display the current page.

##### components/menus/QuiddityMenu.js

This is the menu used to create quiddities.
This is built from the menus.json configuration file.

#### components/modals
These are the various modals that exist in scenic.

One thing to note about how we do modals is that they mostly are just unconditionally dumped near the root of the app and displayed when they need to be there.

If you want to create a modal that needs a bit more context sensitivity, the way to do it is to conditionally render its component somewhere within a component that has access to the context you need (see SipContactDrawer.js for an example with the NewContactModal). The css provided by our theme should in theory take care of making it look like a modal wherever you put the component.

I'm not going to enumerate the modals except maybe to say that Rtmp is mostly unused and scene is obsolete.

#### components/pages
These are whole pages. They are all in the PagesWrapper component. The LivePage was used to switch between scenes in contexts where thumbnails were taking too much CPU (streaming 20 5fps images through data url transmitted by websockets displayed in components that constantly rerender is not super efficient). Since scenes are out, this page is osbsolete.

#### components/panels
These are mostly containers for buttons.

##### components/panels/FileControlPanel.js
This is the small menu with the garbage can on the right-hand side. This contains the buttons used to manage sessions.

##### components/panels/FilterPanel.js
Filters never worked in a way that satisfied anyone and were mostly unused. They were disabled. This used to contain the filter menu.

##### components/panels/MatrixMenuPanel.js
This is the small menu on the left with the button that opens the PropertyInspectorDrawer and the SIP contact list.

##### components/panels/NotificationOverlay.js
This is used to display notifications caused by `switcher` events. You need one copy of this component on every page on which you want to display notifications.

##### components/panels/OrderPanel.js
This contains the buttons that manage quiddity order. We disabled them because it was buggy and not really necessary once source categorization was implemented.

##### components/panels/SceneControlPanel.js
This is disabled since we disabled scenes.

#### components/quiddity
why ?

##### components/quiddity/NicknameLabel.js
This is used in the source components to display a quiddity's nickname

##### components/quiddity/StartedCheckbox.js
As far as I can tell, this isn't used ...

#### components/tooltips
These display various tooltips as a cursor is put over an element

##### components/tooltips/LockedDestinationTooltip.js
I think this one is broken, I've never seen it in action. It's supposed to tell you that a destination is locked. I'll talk more about locking in the lockstore part.

##### components/tooltips/ShmdataInformationTooltip.js
This one is displayed when you hover over where a thumbnail should go in a source.

##### components/wrappers
I don't know why pagewrapper is not in this...

##### components/wrappers/HelpWrapper.js
This component is used to wrap every component that displays a help message in the bar on the bottom left of the interface.

### main.html
HTML boilerplate in which the React stuff is injected into.
the `#scenic` div is the important one

### main.js
root script that renders the app in the `#scenic` div

### models
These are object-oriented models of `switcher` and `scenic` states.

Most of these are pretty straightforward and don't do a lot of work.

##### models/common/ConfigEnum
This describes an enum for the 4 different config files. Since some of the config files are actually switcher config files, it also defines a helper method to determine which config file is which from a file path.

##### models/common/FileBridge.js
This model can take a js blob object and expose its interesting properties (path, name, content ...).

This is used in the session loading and saving feature.

##### models/common/InitStateEnum.js
This enumerates initialization states. It's an important indicator of a store's state. A lot of stores do some work on initialization and need to be de-initialized if a switcher is disconnected from scenic.

##### models/common/Notification.js
This is the model that drives the NotificationOverlay component. It defines all the attributes you could want in a notification (message, duration, ...)

##### models/common/OrderEnum.js
This defines values to be used with the quiddity reordering system. This system is disabled right now.

##### models/common/PageEnum.js
This defines values used by the pageStore to tell the PageWrapper component which Page component to display.

##### models/common/InitStateEnum.js
This is used to encapsulate links, possibly associated with icons

##### models/common/SettingEnum.js
This enumerates the different configuration pages (general configurations and sip configurations)

##### models/common/StatusEnum.js
This defines a ui status enum and some helper methods to convert all kind of switcher/scenic related statuses to their display equivalents.

##### models/common/UnitEnum.js
This defines kbps and bps types and conversion from them to numbers and vice versa.
This is used in the propertyStore to hardcode special cases of quiddity properties that exposes a bitrate in bps or in kbps

#### models/matrix
These define models that are used in the matrix store or components.

##### models/matrix/MatrixCategoryEnum
These are used for destination entries categorization.

##### models/matrix/MatrixEntry
A MatrixEntry is somewhat complicated.

It represents either a column header or a row header of the matrix, AKA an assignable media source or destination. If the mapping of media -> quiddity was 1 to 1, a matrix entry would simply be a quiddity but since a quiddity can have multiple outputs or inputs and can even be dynamically attributed inputs and outputs, a MatrixEntry is a bit more complicated.

A matrix entry is defined like this :
It can represent either part of a quiddity's I/O or a shmdata slot for a SIP contact.
It has either a quiddity or a contact ID and it can have a shmdata associated with it.

That's it. This model on its own can tell you little things like "is this an h264 encoded video ?" or "is it a source that comes from a distant SIP contact" but apart from that, distinguishing sources from destinations must be done externally with more context.

#### models/menus
These manage the quiddity menu's menu items. In particular, some bundles that we define can represent hardware outputs that it wouldn't make sense to be able to instanciate more than once. These two files manages the state of those and can be instanciated from the menu configuration files.

#### models/quiddity/kind.js
These are built from a call to `switcherio`'s getKinds. They describe basic quiddity kinds attributes.

#### models/quiddity/NdiStream.js
This model is meant to be instanciated from the parsed output of the `NDISniffer` quiddity. It represents the characteristics of an NDI media stream available on the network `switcher` has access to.

This provides scenic with the command line to pass the NDIInput quiddity in order to have it expose the right stream.

#### models/quiddity/Property.js
This models a quiddity property.

Note that this includes multiple string pattern based heuristic to determine how properties should behave. This may need some adjusting sometimes.

#### models/quiddity/Quiddity.js
This models a quiddity. This mostly encapsulates data that a quiddity has in switcher but also modelises some information that scenic adds on top of a quiddity E.G. the hidden and protected attributes.

The additionnal hidden and protected attributes are merged into an `isPrivate` attribute that is used to distinguish quiddities used by scenic to make different part of the front end work and quiddities that were instanciated by the user.

It also specifies helper methods to determine destination quiddity matrix categories.

#### models/quiddity/QuiddityTagEnum.js
Enum containing a tag that denotes source or destination.

#### models/quiddity/specialQuiddities.js
This file defines a bunch of kinds and nicknames of special quiddities like for example the systemUsage quiddity or the sip quiddity.

#### models/schemas
I won't go into each of these individually but give some context.

At some point it was decided that using json schema to validate object integrity at runtime was a good idea. This makes it so that for certain objects, like for example a Connection, if you want to change the type of an attribute like an id from int to string, you will also need to change the schema that goes with it in order for the thing to stop crashing at runtime.

In my opinion this adds very little value and should be removed. If we want more type safety we could go with typescript.

If you encounter schema validation crashes I would encourage you to try ripping out the validation.

#### models/shmdata
These are models that manage different parts of a shmdata. A surprising amount of these files actualy just serve to model different indicators of what a shmdata actually does. This is because before the advent of claws and connection specs (approximately 2022), there was no easy way to know what a quiddity could connect to or whether or not it was already connected to something.

##### models/shmdata/Capabilities.js
This model provides utility methods over switcher gst-style caps strings to be able to extract media information. This is useful since shmdata generally (within switcher usage at least) uses gst-style caps syntax to describe its media.

##### models/shmdata/CapabilityGroupEnum.js
This defines constants used to know if a part of a caps is a size.

##### models/shmdata/MediaTypeEnum.js
This defines media types constants that are used to determine which logo to represent a source shmdata in the case it can't have a thumbnail.

##### models/shmdata/Shmdata.js
This encapsulates the data available in a shmdata. Note that the fromJson static method is tailored for the shmdata representation present in the shmdata part of a quiddity's infotree.

This also defines a method to know if a shmdata comes from a SIP contact and if so which one.

##### models/shmdata/ShmdataRoleEnum.js
This defines shmdata roles. Technically, a shmdata doesn't have a role, is just data in a sysv shared memory but since our shmdata model is built from a quiddity's info tree and a quiddity can either write or read shmdatas, a shmdata object built from a quiddity can either read or write.

There are three roles here, READER, WRITER and FOLLOWER. READER and FOLLOWER should be considered synonymous even if there exist a difference for the shmdata api. Everything should theoretically be followers but I think there exist edge cases where the term reader is still used.

##### models/shmdata/Spec.js
This models a part of a connection spec and its attributes.

Note that a connection spec generally refers to a collection of individual specs. This models a part of a connection spec.

An element of a connection spec has
- a label which is a human-readable indication of what it does (e.g. "video input")
- a description
- a canDo array which contains the media part of a caps string for what it can handle
- a sfid or a swid (f for Follower, w for Writer)

##### models/shmdata/Stat.js
This represents the data rate part of a shmdata with helper method to get bit rate and bit rate in megabits per second.


#### models/sip
Since sip is going away, I'm going to gloss a little bit over these. They are not very complicated anyway.

##### models/sip/Contact.js
This represents the sip contacts and provides helper method to get their attributes.

##### models/sip/SipCredentials.js
This model represents the sip credential that a scenic user needs to enter to log in to SIP.

##### models/sip/SipEnums.js
These modelise different states of the sip quiddity with regard to data transmission from one contact to another.

#### models/systemUsage
These models are built with data from the systemusage quiddity and have helper methods that convert the values to human readeable units. This also manages the ui colors of the resources (e.g. : a cpu bar of a core pegged to 100% will be displayed in red) via the StatusEnum.

#### models/userTree/Connection.js
This is an important model since its used directly in the ConnectionBox component. It models a potential connection between two quiddities (given a sfid) or one quiddity and a contact.

This can be a connection between the writer sfid of a quiddity and either the reader sfid of a quiddity or a SIP contact.

Note that this does not modelise the swid of the reader quiddity to which the sfid of the writer quiddity is connected : the `switcher` method that we use to connect two quiddities together is `try-connect`, it takes two quiddity ids and it exhaustively tries every sfid and swid until it finds a happy combination. It then only returns us a `sfid`.

This makes it so that this model is incomplete and is probably the root cause of very obscure bugs.

Note that this model has a bunch of methods used to serialize connection information to the userTree. This was used for the scenes feature but is not used anymore.

#### models/userTree/Scene.js
This describes a scene in scenic.

A scene only makes sense in the context of the current set of quiddities that are running in the `switcher` connected to scenic. It is a collection of connections that should be activated when it becomes active.

Since scenes are a purely scenic related concept and `switcher` doesn't have knowledge of them, we needed a way to synchronize them between multiple scenic instances controlling the same `switcher`.

This and quiddity reordering is the reason for the creation of the userTree. By writing data to the userTree, scenic can shove data in the backend and another scenic instance can retrieve it at a later time to have information about what scenes exist.

Unfortunately, this feature never worked correctly. Better tools exist for show control anyway, and we can direct artists to them if need be.

### stores
The files in this folder contain the majority of the logic of scenic.

Stores are responsible for containing and managing the state of the application. Components can then observe certain attributes of a store and mobx automatically rerenders them when something important changes.

The stores are also responsible for calling the backend when a user interaction mandates it.

#### stores/common
These deal with things that are mostly related to the front end's state like which page is displayed, which modal is active or what notifications are in the notification should be displayed and which are expired

##### stores/common/ConfigStore.js
This store keeps the loaded configurations in memory. Be aware that some configuration data structures are merged with the default configurations (like the bundles) and some others are overwritten (like the menus).

##### stores/common/DrawerStore.js
This manages the display state of the different drawers in scenic.

##### stores/common/HelpMessageStore.js
This receives messages from the HelpWrapper components and the HelpSection component of the StatusBar displays that message.

##### stores/common/ModalStore.js
This manages the display of the currently active modal.

##### stores/common/NotificationStore.js
This manages a Set of notifications that either should be displayed on the NotificationOverlay or that should not be displayed anymore.

##### stores/common/PageStore.js
This manages the display of the current page.

##### stores/common/SessionStore.js
This maintains a list of the session files available to the `switcher` scenic is connected to.

Sessions are serialized sets of quiddities and their connection that can theoretically be restored. This is pretty buggy at the moment.

Note that scenic considers that the current state of the application is the "current session" and that clearing the current session will reset the state of switcher.

##### stores/common/SettingStore.js
This stores the current scenic settings.


The most interesting ones are displayThumbnails and displayEncoders.

displayThumbnails is true by default and controls whether or not the thumbnail quiddity should exist and be connected to all sources that could generate a thumbnail (right now raw video sources but there is an experimental branch for audio thumbnails).

displayEncoders is a bit more complicated. Some bundled quiddities have two writer sfids : one for raw video and another one for compressed video. Raw video cannot be patched to the sip quiddity right now because the sip quiddity does not handle compression and sending raw video over the network would overload many connections. The problem is that we cannot make thumbnails with compressed video and we cannot send compressed video to video displays.

Because of this, the bundled quiddities that represent hardware video inputs on a scenic station come with both their raw source and the encoder that compresses that raw video. By default, this displays two matrix Sources and can be confusing.

displayEncoders can be set to false and this will trigger some changes in the MatrixStores behaviour : It will consider that a MatrixEntry can have multiple shmdatas : a raw and an encoded one. That way a user can see a beautiful thumbnail and patch only one box to destinations that require encoded or raw video.

This **seems** to work but I'm not sure if it is tested often because it is not the default.

#### stores/common/ populateStores.js initializeStores.js and populateBindings.js

These three files are used at the startup of scenic. You can read the first section of this document to get a description but I think the best way to understand this is to read the code, put debug prints or breakpoints and observe it at work.

In general the order is :
populateStores -> populateBindings -> wait for the socket to be connected to a switcher -> initializeStore -> execute the creation bindings that were populated in populateBindings.

#### stores/matrix
These files deal with different aspects of the connection matrix.

##### stores/matrix/FilterStore.js
This manages quiddity filters. Filters are disabled right now because source categorization by contact made them unnecessary and they didn't work great in the first place.

##### stores/matrix/LayoutHelper.js
All of these functions are used to generate the necessary CSS strings to be able to define a grid layout that makes sense for the current sources and destinations.

##### stores/matrix/LockStore.js
This defines behaviours for locked sources and destinations.

Locks are a scenic-only concept that prevents user from doing things that could result in unwanted behaviours.

Generally, a source becomes locked when it is assigned to a network destination. When a source is locked, it is not possible to toggle its started button.

Only two kinds of destinations will be locked: SIP contacts when they are actively being called with some media and NDI destinations when they are actively broadcasting media on the network.

Note that the setLock function recursively checks sources to lock everything that could possibly be related to them. Example : if you have a video source that you then encode and then decode and then encode again and you send the input of that second encoder via an NDI output, this function should figure out that you should not be able to stop the video source and should lock all the signal chain.

##### stores/matrix/MatrixStore.js
This contains the models used to populate and update the matrix view.

This keeps lists of source and destination `MatrixEntry`s and has some computed properties that should give you sorted and categorized sources and destinations.

##### stores/quiddity
These stores handle various aspects of quiddity states.

##### stores/quiddity/EncoderStore.js
This store exists because of the `displayEncoders` setting. When `displayEncoders` is set to false, some additional logic needs to be added to know if a MatrixEntry can be connected to something else. This logic lives there.

##### stores/quiddity/KindStore.js
This is mainly used in the QuiddityCreationDrawer to ensure that the kind the user is entering is valid and can be created by switcher.

##### stores/quiddity/NicknameStore.js
This is used to change the components that needs to display quiddity nicknames.

We need this because mobx is not very good at detecting changes to values in nested objects. See the entry for QuiddityStore.js for more details.

##### stores/quiddity/OrderStore.js
This was used to handle quiddity reordering. Ordering is disabled now.

Since quiddity order is a front-end only concern and multiple scenic's can control one switcher, there needed to be a way to communicate quiddity orders from one scenic instance to another or even to handle a page refresh without losing order.

To handle this, the userTree of the emptyquid was used to send front-end data to the backend and to retrieve it.

##### stores/quiddity/PropertyStore.js
This is used to display and update components that use quiddity properties, mostly started buttons and the `PropertyInspectorDrawer`.

Very important note : Because of mobx limitations, there are **3** different objects that act as source of truth for properties.

If you need up-to-date information on what properties exist and their names for a certain quiddity, look in the `properties` map. If you want the value of a propery, look in the `values` map and if you want the status (meaning is it disabled right now ?) look at `statuses`.

Confusingly, the `properties` map has all of this information but only the names are guaranteed to be up-to-date.

Also, note that property information are also in the info tree of every quiddity in the quiddity but again, those are not guaranteed to be up-to-date.

Thanks mobx.

##### stores/quiddity/QuiddityMenuStore.js
This manages the quiddity creation menu

##### stores/quiddity/QuiddityStatusStore.js
This seems to react to changes in the quiddity store and to be used in the status prop of certain components. It could change quiddity display but I'm not sure its ever relevant.

##### stores/quiddity/QuiddityStore.js
This keeps a list of all the quiddities that are instanciated in switcher. It has a lot of computed properties that will give you quiddities that are sources or destinations.

Since quiddities are the basic building blocks in switcher, this store is a pretty good indication of the state of the software. At runtime, you can run `toJS(stores.quiddityStore.quiddities)` in the console and have a good idea of what exists at this time.

Initially, this store was never guaranteed to be a source of truth for anything else than the existence of a quiddity but some work has been done to try to make the quiddities object up to date with the actual state of switcher.

Note that this work is not completely finished and I think (???) properties, user tree and maybe connection specs stil needs work.

In a perfect world, the quiddity object contained in this store could be used as a source of truth for every component that deals with anything contained by a quiddity. However, the very dynamic nature of some parts of a quiddity (notably the info tree) makes it so mobx will not react to certain changes to attributes of a quiddity object, especially attributes with nested keys that didn't exist at the time the object was declared observable to mobx.

To counteract that, it was decided to create substore for every possible parts of a quiddity, to ensure that every change to a part of a quiddity can trigger a mobx component rerender.

Initially, the sub stores reacted to their own events : if the shmdata part of a quiddity's info tree changed, only the shmdata store would receive that update and update its internal models. This made the quiddityStore tantalizing in its data content but often not up to date with the reality of the backend.

More and more, we want to move event processing to the quiddity store and have it update the substore. When this is done, the flow of events throughout scenic will be easier to understand.

#### stores/shmdata
These generally model shmdatas and their properties. This was more relevant when connection specs didn't exist. With the advent of connection specs and more robust connection logic in switcher, some of the code in these stores can probably be removed and their responsability be shifted to the backend.

##### stores/shmdata/CapsStore.js
Right now this contains a method that determines if two quiddities are connectable. There's an MR in draft that considerably reduces the amount of code in this store by using connection specs instead.

##### stores/shmdata/MaxLimitStore.js
At some point, switcher didn't really handle what happened if you tried to connect a quiddity that was already connected to something else. This is a vestige of that time.

##### stores/shmdata/ShmdataStore.js
This keeps a list of all the shmdata's mentioned in all-of-the quiddity's trees. I think this store was written with some wrong assumptions about how shmdatas work and wrong assumptions about how switcher works which makes it fragile, hard to debug and sometimes lies (there's a line that says `throw new Error('A shmdata is written by more than one quiddity')`, don't believe it, its perfectly possible and valid).

Unfortunately, this is used by the matrix stores and my attempt to refactor it was not successful because of mobx update quirks.

It works ok right now so maybe try not to touch it too much ? If you need to modify this, good luck ...

##### stores/shmdata/SpecStore.js
This keeps track of the connection specs of quiddites. It's not used much right now but as mentioned in the entry for the CapsStore, there's an MR that makes heavyer use of it.

##### stores/shmdata/StatStore.js
This keeps track of the stats attribute of the shmdata attribute of a quiddity's info tree.

This is used to update components that need to display debit information.

#### stores/sip

##### stores/sip/ContactStore.js
This maintains a list of the contacts stored in the sip quiddity's infotree. It also handles important actions like attaching shmdata to a contact (basically telling the sip quiddity that we want to connect a media and send it to someone) and calling with sip.

##### stores/sip/ContactStatusStore.js
This maintains a list of contact statuses. SIP is a voip protocol so contacts can be busy, online, offline, ...etc.

##### stores/sip/SipCredentialStore.js
This keeps the sip login parameters in memory. It also validates those fields to make sure the users are entering valid urls.

##### stores/sip/SipStore.js
This handles sip quiddity actions that do not have anything to do with contacts.

#### stores/SocketStore.js
This handles the connection to switcher and contains the socket object.

Most stores will observe changes to the socket object and try to reset themselves when it changes. This makes it so when scenic is disconnected to switcher the states of the stores are reset to defaults.

#### stores/special
These handle some edgecases that should be managed by the backend but that scenic has to provide special code for.

##### stores/special/NdiStore.js
For `switcher`, NDI quiddities do not exist. Everything to do with NDI is done via the `executor` quiddity the can execute arbitrary command lines and expose the shmdatas that can result from these commands.

This store handles all the logic needed to discover ndi streams via the NDISniffer executor quiddity and to create the right command lines for ndiinput and ndioutput quiddities.

##### stores/special/RTMPStore.js
RTMP is used to stream to sites like youtube, facebook or twitch.

In all the time I've been here, I have never seen RTMP being used. It's apparently broken with Facebook and I gather that scenic needed to make sure a video AND an audio was plugged in before we could start this quiddity or else it would not work. I guess this store handles that ?

If anywone wants to stream stuff in a scenic performance, kindly guide them towards OBS studio.

##### stores/special/SystemUsageStore.js
This reads the special values in the systemusage quiddity's infoTree and makes the information found there available so that components such as the cpu bar things can render them.

#### stores/store.js
This is a base class for the stores.

From the top of my head this is mostly used to give uniform initialization logic to every store without having to write it everywhere.

#### stores/timelapse
These handle the preview and timelapse quiddities. The thumbnail store waits for new quiddities to be created and try to connect them to the thumbnail quiddity to create thumbnails for them.

The preview store waits for someone to click on the thumbnail of a quiddity to connect that quiddity to the preview quiddity and generate a higher fps, higher resolution preview.

Both thumbnails and previews are base64 encoded images streamed directly from the backend and displayed in an \<img\> tag.

#### stores/userTree
##### stores/userTree/ConnectionStore.js
This should not be in the userTree folder. It's much too important and only has a little to do with the userTree of a quiddity.

This is responsible for trying to maintain a list of all the connected quiddities and has methods used to ask the backend to connect quiddities. It also handles connections to contacts.

Note that this store has an "armed" connection concept. This was related to the scenes and is not used anymore.

##### stores/userTree/SceneStore.js
This is not used anymore. See the entry for `Scene.js` for more details.

##### stores/userTree/UserTreeStore.js
This mainly tries to keep the userTrees of all the quiddities updated in the backend. This is only used for disabled features.

### utils
This contains misc helpers.
#### utils/cookies.js
These are user-friendly wrappers for cookie manipulation. Cookies are used for ease of debugging to save and restore sip credentials.
#### utils/debugTools.js
This is used to put scenic in debug mode to get access to the debugStores variable.

There is now a global `stores` variable that does the same thing so this is obsolete.
#### utils/errors.js
Contains the errors thrown everywhere by objects that don't have their requirements or that fail to initialize.

#### utils/fileTools.js
Utils to manage file upload and download.

#### utils/i18n.js
Tells the i18n library to use our locales files. If you want to translate scenic in spanish, you'll have to modify this.

#### utils/logger.js
This does some logger magic that I don't know anything about.

#### utils/numberTools.js
Don't you just loooove js's exhaustive standard library ?

Just some basic number manipulation helpers.

#### utils/objectTools.js
Don't you just loooove js's exhaustive standard library ? Oh, did I say that earlier ?

This imports stuff from lodash to avoid having to code cloning from scratch.

This also defines horrible functions that are used by the quiddityStore to try to keep a quiddity's info tree up to date. The functions are documented so I won't bother repeating the docstrings here.

#### utils/stringTools.js
I swear, the bit about js's standard library never gets old.

Of note here is the function escapeToSafeCss which you should use when you want to use arbitrary object names as CSS identifiers. It will strip out the illegal characters that would cause your CSS to fail.

#### utils/uiTools.js
Defines a helper to make hover less of a pain to code with react.

### test
I won't go through everything in this folder and its subfolders.

This contains the unit and integration tests and the infrastructure needed to run them.

#### test/unit
These are our unit tests.

Scenic is asynchronous and mostly works by reacting to side effects caused by events sent by the backend.

Unit testing this kind of code is very hard because the behaviour of a single function often depends on state that is distributed through multiple stores.

Our unit tests are very dependent on mocking return values of functions which makes them very brittle. Changing seemingly trivial internal details of a function can easily break a unit test.

If you are writing new unit tests, please do not imitate the style of those already existent. Only write unit tests for self-contained, preferably side effect free functions.

If your function is not self-contained and side effect free, use integration tests.

#### test/fixture
This folder contains sample objects to be used in unit tests. You can import json files directly in the tests but some helper js files can help you import an entire class of fixture object all at once (e.g. the `allQuiddities.js` file).

#### test/integration
These tests are more robust than the unit ones and tests global behaviour of the software. Apart from the configuration test that breaks all the time, if you break one of these, you have probably broken something in the software.1

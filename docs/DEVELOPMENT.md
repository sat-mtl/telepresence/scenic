# Development Guide

This guide lists the available **NPM** and **Make** scripts and commands used to simplify Scenic's development process.

To get detailed information on the inner architecture and dependencies of Scenic, please refer to the [Maintenance Guide](./MAINTENANCE.md).

## NPM Scripts

NPM is used extensively to **serve**, **build**, **test** and **document** the Scenic codebase.
[NodeJS LTS](https://nodejs.org/en/) must be installed on your system.

| Command                    | Mode        | Description
|--------------------------- |------------ |------------
| `npm install`              | \*          | Install all internal dependencies of the project
| `npm start`                | Development | Start a webpack *development* server for the Scenic webapp on [localhost:8080](http://localhost:8080)
| `npm run build`            | Production  | Build and optimize the webapp into the \`dist\` folder
| `npm run lint`             | Development | Lint the project with the [StandardJS](https://standardjs.com/) linter
| `npm run test:coverage`    | Development | Run all tests and generate a full coverage report of the code (used for CI)
| `npm run test:unit`        | Development | Run all unit tests
| `npm run test:integration` | Development | Run all integration tests
| `npm run test`             | Development | Run **all** tests (using [Jest](https://jestjs.io/)
| `npm run test:watch`       | Development | Watch the codebase and rerun the tests when changes are detected (requires the [entr](https://github.com/eradman/entr) utility)
| `npm run doc:jsdoc`        | \*          | Generate the `docs/JSDOC.md` file from the [JSDoc](https://jsdoc.app/) docstrings embedded in the JavaScript code
| `npm run doc:selectors`    | \*          | Generate the `docs/SELECTOR_GLOSSARY.md` file from the React component files
| `npm run doc:build`        | \*          | Build the whole user documentation using [MkDocs](https://www.mkdocs.org/) into the \`public\` folder
| `npm run doc:serve`        | \*          | Start an [MkDocs](https://www.mkdocs.org/) *development* server for the user documentation on [localhost:8000](http://localhost:8000)
| `npm run locales:sort`     | \*          | Sort all globalization keys in locale json files

Keep in mind that [scenic-core](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/) needs to be running in order for Scenic to do anything. The web application will be unusable as long as a Websocket connection between the UI and the Core isn't established. The UI respects the Websocket API provided by [scenic-core](https://gitlab.com/sat-mtl/tools/scenic/scenic-core/).

!!!warning
    The application build is heavy and may fail with a _JS heap out of memory_ error. You can fix this by building the distribution with `NODE_ENV=production node --max_old_space_size=8048 ./node_modules/webpack/bin/webpack.js --progress --config config/webpack.config.js`

!!!info
    All the testing commands are publishing all the logs produced by Scenic in `stdout` by default. These logs can be silented by changing the `NODE_ENV` variable. By exemple, `NODE_ENV=quiet npm test` is hidding all logs.

## Make Scripts

The project's `Makefile` provides additional utility scripts, mainly for deployment and CI/CD purposes

| Command                 | Purpose      | Description
|------------------------ |------------ |--------------
| `make build`            | Deployment   | Build the application from a clean installation
| `make bundle`           | Deployment   | Build and bundle the application into a compressed file
| `make install`          | Deployment   | Deploy the application in a local H2O server
| `make install-apache`   | Deployment   | Deploy the application in a local Apache2 HTTP server
| `make clean`            | Housekeeping | Remove all *auto-generated* folders
| `make install-mkdocs`   | Housekeeping | Install the **User Documentation** dependencies
| `make uninstall`        | Housekeeping | Uninstall everything installed by `make install`
| `make sort-locale-keys` | Housekeeping | Alphabetically sort all keys in the `locale.json` asset files

## Debugging

To debug Scenic, you should use the `npm start` to build and serve the application on [localhost:8080](http://localhost:8080). You should also use your browser's [Developer Tools](https://www.digitalocean.com/community/tutorials/how-to-use-the-javascript-developer-console) to inspect the application. Specifically, the [Browser Console](https://developer.mozilla.org/en-US/docs/Tools/Browser_Console) will output every message, error and warnings needed for debugging the application, all in real-time.

### Debug features

Some features exist to aid debugging. You can ask scenic to connect to a distant switcher with a query string with the endpoint value. This is the supported format :  `http://localhost:8080/?endpoint=<ip adress>:<port>`

The list of global stores is available in the global `stores` variable and the `toJS` function of mobX is also available in the global scope.

Also, using the URL argument `debug` will enable debug functions such as the `debugStore` method. Consequently, opening scenic with the URL `http://localhost:8000?debug=true` will allow you to do:

```js
debugStores()['shmdataStore'] // available in the development console of the browser
```

This will print a JSON object that is describing all the stored values of the store. This is a convenient way to inspect the current statement of a store at runtime. Also, we encourage to use these methods in unit tests.

## Testing

To avoid regression bugs, we try to keep Scenic's coverage high. To automate Scenic's testing while making changes to the codebase, we highly recommend the usage of the `npm run test:watch`  script, which leverages the power of the [entr](https://github.com/eradman/entr) utility. This NPM command will automatically run unit tests when changes are detected in the codebase. You can also limit which tests and which part of the codebase will be watched with the following command:

```bash
npm run test:watch -- SocketStore # Only runs tests associated with the 'SocketStore'
```
## Pushing fixes/features

In order to add a new piece of code in the project, you should follow the [contributing documentation](./CONTRIBUTING.md).

## Generating the documentation

Scenic's *user documentation* is built with [MkDocs](https://www.mkdocs.org/). To generate it, first install the Python dependencies:

```bash
pip install setuptools # must be installed first
pip install -r docs/requirements.txt
```

You should then be able to serve the whole documentation on [localhost:8000](http://localhost:8000):

```bash
npm run doc:serve # Serve the Mkdocs site on localhost:8000
firefox localhost:8000
```

Know that the [JSDoc](https://jsdoc.app/) can be generated separately from the docstrings in the JS codebase, inside the `JSDOC.md` file. The same applies to the `SELECTOR_GLOSSARY.md` file, which contains a list of automatically generated CSS selectors used for E2E testing with automated tools such as [Selenium](https://www.selenium.dev). To generate these files separately:

```bash
npm run doc:selector # Generate the SELECTOR_GLOSSARY.md file
npm run doc:jsdoc # Generate the JSDOC.md file
```

The `docs/JSDOC.md` and `docs/SELECTOR_GLOSSARY.md` files are also automatically generated by the `npm run doc:serve` and `npm run doc:build` commands.

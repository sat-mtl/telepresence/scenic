FROM registry.gitlab.com/sat-mtl/tools/switcher:deps-ubuntu2204 AS switcher-build

# Allowed types are "Debug", "Release" or "RelWithDebInfo"
ARG BUILD_TYPE="Debug"

# Allow to build this image from a fork
ARG SWITCHER_REPOSITORY="https://gitlab.com/sat-mtl/tools/switcher.git"
ARG SWITCHER_BRANCH="develop"

# Should be configured and improved according to future needs
ARG ENABLE_GPL="ON"
ARG ENABLE_WEBRTC="OFF"
ARG ENABLE_JACK="OFF"
ARG ENABLE_TIMECODE="OFF"
ARG ENABLE_VRPN="OFF"

WORKDIR "/"

# One day, this should be replaced by `apt install switcher, switcher-dev, python3-switcher, switcher-doc`
RUN git clone "${SWITCHER_REPOSITORY}" \
 && cd switcher \
 && git checkout "${SWITCHER_BRANCH}" \
 && git submodule update --init --recursive \
 && cmake -GNinja -B build \
          -DENABLE_GPL="${ENABLE_GPL}" \
          -DPLUGIN_WEBRTC="${ENABLE_WEBRTC}" \
          -DPLUGIN_JACK="${ENABLE_JACK}" \
          -DPLUGIN_TIMECODE="${ENABLE_TIMECODE}" \
          -DPLUGIN_VRPN="${ENABLE_VRPN}" \
          -DCMAKE_BUILD_TYPE="${BUILD_TYPE}" \
 && ninja -C build \
 && ninja install -C build \
 && ldconfig \
 && rm -rdf build

FROM switcher-build AS integration-deps

WORKDIR "/tmp"

ENV BASH_ENV ~/.bashrc
ENV VOLTA_HOME /root/.volta
ENV PATH $VOLTA_HOME/bin:$PATH

RUN apt update -y \
 && apt install -y -qq --no-install-recommends \
                    curl \
 && curl https://get.volta.sh | bash \
 && volta install node

FROM integration-deps AS swio-cmd

# Should be configured and improved according to future needs
ARG SWITCHER_REPOSITORY="https://gitlab.com/sat-mtl/tools/switcher.git"
ARG SWITCHER_BRANCH="develop"

WORKDIR "/"

ADD assets/json/switcher.default.json /etc/switcher/global.json

ADD assets/json/menus.default.json /root/.config/scenic/menus.json
ADD assets/json/scenic.default.json /root/.config/scenic/scenic.json
ADD assets/json/bundles.default.json /root/.config/scenic/bundles.json
ADD assets/json/contacts.example.json /root/.config/scenic/contacts.json

# TODO switcher crashes when it loads config
ENV XDG_CONFIG_HOME /home/root/.config/

USER root
EXPOSE 8000

CMD ["switcher", "--debug"]

<!-- markdownlint-disable MD033 MD034 MD041 -->

<img src="docs/img/scenic-signature-horizontal.png" onerror="this.style.display = 'none'"/>

[![pipeline status](https://gitlab.com/sat-mtl/tools/scenic/scenic/badges/develop/pipeline.svg)](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/commits/develop)
[![coverage report](https://gitlab.com/sat-mtl/tools/scenic/scenic/badges/develop/coverage.svg)](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/commits/develop)
[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)
[![Scenic version](https://img.shields.io/badge/dynamic/json?color=yellow&label=version&query=version&url=https%3A%2F%2Fgitlab.com%2Fsat-mtl%2Ftelepresence%2Fscenic%2F-%2Fraw%2Fmaster%2Fpackage.json)](https://gitlab.com/sat-mtl/tools/scenic/scenic/-/tags)

# Overview

Scenic is a **graphical user interface** made with [React](https://fr.reactjs.org/). Together with [Scenic Core](https://gitlab.com/sat-mtl/tools/scenic/scenic-core) and [Switcher](https://gitlab.com/sat-mtl/tools/switcher), it forms the backbone of the **Scenic suite**, a telepresence software stack that allows for real-time transmission of audiovisual and arbitrary data over any IP network. Telepresence systems can be used in various artistic contexts, so that two different creative spaces can communicate with each other in real-time and present a combined performance.

Scenic is designed to be used in conjunction with [Scenic Core](https://gitlab.com/sat-mtl/tools/scenic/scenic-core), a Node.JS server that communicates with Scenic using [Socket.IO](https://socket.io/).

Scenic is currently maintained by the [Société des Arts Technologiques (SAT)](http://sat.qc.ca/), a non-profit artistic entity based in Montreal, Canada.

# Usage

First, launch [Scenic Core][scenic-core] with the `scenic` command.

Second, open Scenic in your browser (Chromium is recommended).

* If you are running Scenic locally, open `localhost:8080` in your browser. (`chromium-browser http://localhost:8080`)
* If you wish to run the online version of Scenic (no installation required), open `http://scenic-app.com` in your browser.

For optimal support of touchscreens, consider launching **Chromium** with ```--touch-events```.

[scenic-guide]: https://sat-mtl.gitlab.io/tools/scenic/scenic/INSTALLATION/
[scenic-core]: https://gitlab.com/sat-mtl/tools/scenic/scenic-core/
[scenic]: https://gitlab.com/sat-mtl/tools/scenic/scenic/

# Docker deployment

Please consult [Docker usage page](docs/docker-usage.md).

# Automated cloud dev environment

Scenic developers can use [Gitpod][Gitpod] automated cloud dev environment to easily develop and test new features.

[Gitpod]: https://sat-mtl.gitlab.io/tools/scenic/scenic/GITPOD/

## Contributing

Check out our [Contributing Guide][contributing] to get started!

Additionally, take a look at Scenic's [documentation][documentation].

[contributing]: CONTRIBUTING.md
[documentation]: https://sat-mtl.gitlab.io/tools/scenic/scenic/DEVELOPMENT/

# Authors

See [here](AUTHORS.md).

# License

This project is licensed under the GNU General Public License version 3 - see the [LICENSE](LICENSE.md) file for details.

# Acknowledgments

This project was made possible by the [Société des Arts Technologiques (SAT)](http://sat.qc.ca/).
